<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_peta_sebaran extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_peta_sebaran');
		}

	public function index()
	{
		$this->report_produksi();

	}

	public function get_data()
	{
		echo json_encode($this->mdl_peta_sebaran->get_data());
	}

	public function report_produksi( $filter = '' )
	{
		$array_input = $this->input->post(NULL, TRUE);
		$data_dasar = isset($array_input['dasar_kapal'])?  $array_input['dasar_kapal'] : "0";

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'report_peta_sebaran';
		$data['submit_form'] = "reporting/report_produksi";
		
		echo Modules::run('templates/page/v_form', 'reporting', 'peta_sebaran', $labels, $add_js, $add_css, $data);

	}

}
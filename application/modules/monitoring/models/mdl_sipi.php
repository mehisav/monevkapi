<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_sipi extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        $this->load->database();
        
    }

    public function data_sipi_expired()
    {
        $this->load->database();
        $query = 'SELECT    dmm.id_kapal as "id_kapal",
                            dmm.nama_kapal as "nama_kapal",
                            dmm.gt as "gt",
                            dmm.tanda_selar as "tanda_selar",
                            dmm.sipi,
                            dmm.tanggal_sipi,
                            dmm.tanggal_akhir_sipi,
                            mkk.nama_kabupaten_kota,
                            mp.nama_propinsi
                FROM db_monev_kapi.mst_inka_mina as dmm
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON dmm.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                WHERE dmm.aktif = "YA" AND (dmm.tanggal_akhir_sipi < CURDATE() AND  dmm.sipi <> "")
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function kapal_inka_tanpa_sipi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT *
                FROM db_monev_kapi.mst_inka_mina AS mim
                WHERE mim.aktif = 'YA' AND (mim.sipi IS NULL OR mim.sipi = '')
            ";
        $run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    public function kapal_inka_sipi_expired()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT *
                FROM db_monev_kapi.mst_inka_mina AS mim
                WHERE mim.aktif = 'YA' AND (mim.tanggal_sipi < CURDATE() AND  mim.sipi <> '')

            ";
        $run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function kapal_dss($nama_kapal, $id_dss)
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $nk_array = explode(' ', $nama_kapal);
        if($id_dss == 0)
        {
            $where_str = '';
            foreach ($nk_array as $value) {
                $where_str .= ' AND dmk.nama_kapal LIKE "%'.$value.'%"';
            }
        }else
        {
            $where_str = ' AND dmk.id_kapal = "'.$id_dss.'"';
        }
        $query = 'SELECT  dmk.*, mi.no_sipi, mi.tanggal_sipi AS "tgl_sipi", mi.tanggal_akhir_sipi AS "tgl_akhir_sipi"
                FROM    mst_kapal as dmk
                LEFT JOIN mst_izin as mi ON mi.id_kapal = dmk.id_kapal AND mi.aktif = "Ya"
                WHERE dmk.aktif = "YA"'.$where_str.'
                ORDER BY mi.tanggal_akhir_sipi DESC';
        $run_query = $this->db_dss->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_database_inka_sipi($filter = '')
    {   
        $this->db_monev = $this->load->database('default', TRUE);
        if($filter=='')
        {
            $data_inka = $this->kapal_inka_tanpa_sipi();
            $nama_update = 'tanpa_sipi';
        }else
        {
            $data_inka = $this->kapal_inka_sipi_expired();
            $nama_update = 'sipi_expired';
        }    

        foreach ($data_inka as $value)
        {
            // vdump($data_inka);die();
            $data = array(
                'id_kapal' => $value->id_kapal,
                'nama_kapal' => $value->nama_kapal,
                'tanda_selar' => $value->tanda_selar,
                'id_bahan_kapal' => $value->id_bahan_kapal,
                'id_alat_tangkap' => $value->id_alat_tangkap,
                'gt' => $value->gt,
                'panjang_kapal' => $value->panjang_kapal,
                'lebar_kapal' => $value->lebar_kapal,
                'dalam_kapal' => $value->dalam_kapal,
                'no_mesin' => $value->no_mesin,
                'mesin' => $value->mesin,
                'daya' => $value->daya,
                'pelabuhan_pangkalan' => $value->pelabuhan_pangkalan,
                'gross_akte' => $value->gross_akte,
                'sipi' => $value->sipi,
                'tanggal_sipi' => $value->tanggal_sipi,
                'tanggal_akhir_sipi' => $value->tanggal_akhir_sipi,
                'id_dss' => $value->id_dss
            );
            
            $data_dss = $this->kapal_dss($value->nama_kapal,  $value->id_dss);
            if($data_dss != false){
                $data['id_bahan_kapal'] = (isset($data_dss->id_bahan_kapal)) ? $data_dss->id_bahan_kapal : $data['id_bahan_kapal'];
                $data['id_alat_tangkap'] = (isset($data_dss->id_alat_tangkap)) ? $data_dss->id_alat_tangkap : $data['id_alat_tangkap'];
                $data['nama_kapal'] = (isset($data_dss->nama_kapal)) ? $data_dss->nama_kapal : $data['nama_kapal'];
                $data['tanda_selar'] = (isset($data_dss->tanda_selar)) ? $data_dss->tanda_selar : $data['tanda_selar'];
                $data['gt'] = (isset($data_dss->gt_kapal)) ? $data_dss->gt_kapal : $data['gt'];
                $data['panjang_kapal'] = (isset($data_dss->panjang_kapal)) ? $data_dss->panjang_kapal : $data['panjang_kapal'];
                $data['lebar_kapal'] = (isset($data_dss->lebar_kapal)) ? $data_dss->lebar_kapal : $data['lebar_kapal'];
                $data['dalam_kapal'] = (isset($data_dss->dalam_kapal)) ? $data_dss->dalam_kapal : $data['dalam_kapal'];
                $data['no_mesin'] = $data_dss->no_mesin;
                $data['mesin'] = (isset($data_dss->merek_mesin)) ? $data_dss->merek_mesin : $data['mesin'];
                $data['daya'] = (isset($data_dss->daya_kapal)) ? $data_dss->daya_kapal : $data['daya'];
                $data['gross_akte'] = (isset($data_dss->nomor_gross_akte)) ? $data_dss->tempat_gross_akte.'/'.$data_dss->nomor_gross_akte : $data['gross_akte'];
                $data['sipi'] = (isset($data_dss->sipi)) ? $data_dss->sipi : $data['sipi'];
                $data['tanggal_sipi'] = (isset($data_dss->tgl_sipi)) ? $data_dss->tgl_sipi : $data['tanggal_sipi'];
                $data['tanggal_akhir_sipi'] = (isset($data_dss->tgl_akhir_sipi)) ? $data_dss->tgl_akhir_sipi : $data['tanggal_akhir_sipi'];
                $data['id_dss'] = $data_dss->id_kapal;
                
                $this->update($data);

            }
        }
        $data_update['nama'] = $nama_update;
        $this->db_monev->insert('trs_update_monitoring', $data_update);
    }
    
    public function data_update($filter)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT *
                FROM trs_update_monitoring AS tup
                WHERE tup.nama = '".$filter."'
            ";
        $run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = $temp->tanggal_update;
        }else{
            $result = '';
        }
        return $result;
    }

    public function update($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->where('id_kapal', $data['id_kapal']);
        $query = $this->db_monev->update('mst_inka_mina',$data);

        if($this->db_monev->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function data_tanpa_sipi()
    {
        $this->load->database();
        $query = 'SELECT    dmm.id_kapal as "id_kapal",
                            dmm.nama_kapal as "nama_kapal",
                            dmm.gt as "gt",
                            dmm.tanda_selar as "tanda_selar",
                            dmm.sipi,
                            dmm.tanggal_sipi,
                            dmm.tanggal_akhir_sipi,
                            mkk.nama_kabupaten_kota,
                            mp.nama_propinsi
                FROM db_monev_kapi.mst_inka_mina as dmm
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON dmm.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                WHERE dmm.aktif = "YA" AND (dmm.sipi IS NULL OR dmm.sipi = "")
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function daftar_kapal_sipi($filter)
    {
        $this->load->database();
        $a = ($filter == 'sipi_expired' )? 'AND (mim.tanggal_akhir_sipi < CURDATE() AND  mim.sipi <> "")' : 'AND (mim.sipi IS NULL OR mim.sipi = "")';
        $query = 'SELECT mp.nama_propinsi,
                            mkk.nama_kabupaten_kota,
                            mim.nama_kapal as "nama_kapal",
                            mim.tanda_selar,
                            mim.gross_akte,
                            mim.sipi,
                            mim.tanggal_akhir_sipi,
                            mpb.nama_pelabuhan,
                            mat.nama_alat_tangkap,
                            mim.gt,
                            CONCat("P=", mim.panjang_kapal, ", L=", mim.lebar_kapal, ", D=" ,mim.dalam_kapal ),
                            mim.daya,
                            mim.mesin,
                            mim.no_mesin,
                            mim.kontraktor_pembangunan,
                            mim.lokasi_pembangunan,
                            mim.tahun_pembuatan,
                            mbk.nama_bahan_kapal,
                            kmp.masalah,
                            mk.nama_kub,
                            mk.no_siup,
                            mk.tahun_pembentukan,
                            mk.sumber_anggaran,
                            mk.oleh,
                            mk.no_telp,
                            mk.alamat,
                            mk.nama_ketua,
                            mk.telp_ketua,
                            mk.nama_sekretaris,
                            mk.nama_bendahara,
                            mk.anggota,
                            tp.jml_produksi,
                            tp.jml_opreasi,
                            tp.avg_hari_operasi,
                            tp.avg_produksi,
                            tp.avg_bbm,
                            tp.avg_operasional,
                            tp.avg_pendapatan,
                            mim.keterangan
                FROM db_monev_kapi.mst_inka_mina as mim
                LEFT JOIN db_monev_kapi.mst_kub as mk ON mk.id_kub = mim.id_kub
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON mim.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                LEFT JOIN db_monev_kapi.mst_permasalahan as kmp ON kmp.id = mim.id_permasalahan
                LEFT JOIN db_master.mst_alat_tangkap as mat ON mat.id_alat_tangkap = mim.id_alat_tangkap
                LEFT JOIN db_master.mst_bahan_kapal as mbk ON mbk.id_bahan_kapal = mim.id_bahan_kapal
                LEFT JOIN (SELECT id_kapal, SUM(jml_ikan) AS jml_produksi, 
                                    COUNT(*) AS jml_opreasi,  
                                    AVG(datediff(tgl_keluar,tgl_masuk)) AS avg_hari_operasi,
                                    AVG(jml_ikan) AS avg_produksi,
                                    AVG(kebutuhan_bbm) AS avg_bbm,
                                    AVG(biaya_operasional) AS avg_operasional,
                                    AVG(nilai_pendapatan-biaya_operasional) AS avg_pendapatan
                                FROM trs_produksi 
                                GROUP BY id_kapal) tp ON tp.id_kapal = mim.id_kapal
                LEFT JOIN (SELECT mim.id_kapal, 
                                    group_concat(nama_pelabuhan separator ", ") AS nama_pelabuhan
                            FROM db_monev_kapi.mst_inka_mina as mim
                            LEFT JOIN db_master.mst_pelabuhan as mpb 
                                ON find_in_set(mpb.id_pelabuhan, mim.id_pelabuhan_pangkalan)
                            GROUP BY mim.id_kapal
                         ) mpb ON mpb.id_kapal = mim.id_kapal
                WHERE mim.aktif = "YA" '.$a.'
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}
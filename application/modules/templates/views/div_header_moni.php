<!-- mulai #kapiheader --> 
<div id="kapiheader">  
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<h1 id="kapilogo"><a href="index.php"><img src="<?php echo $logo_url;?>/logo.png" class="img-responsive" alt="KAPI | Aplikasi Monitoring Evaluasi Bantuan Kapal"></a></h1>
			</div>
			<div class="col-lg-2">
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div style="text-align: right; font-size: 0.9em; background-color: rgba(0, 128, 255, 0.3); padding: 10px; display: none;">
							&nbsp;
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- akhir #kapiheader --> 
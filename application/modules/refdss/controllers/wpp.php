<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wpp extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_wpp');
		}

	public function index()
	{
		$this->list_wpp();

	}

	public function list_wpp( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_wpp';
		$data['list_wpp'] = $this->mdl_wpp->list_wpp();
		
		echo Modules::run('templates/page/v_form', 'refdss', 'tabel_wpp', $labels, $add_js, $add_css, $data);

	}


    public function list_wpp_array()
    {

        $list_opsi = $this->mdl_wpp->list_opsi();

        return $list_opsi;
    }

}
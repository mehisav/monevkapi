<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_oprasi_kapal extends CI_Model
{
	//private $db_dss;
	private $db;

    function __construct()
    {
    $this->load->database();
    }

   public function detail_list_produksi($id)
    {
        
        $query = "SELECT `mst_dpi`.`id_wpp`, `nama_wpp`, 
                        `gt`, `nama_kapal`, 
                        `tgl_keluar`, DATEDIFF(`tgl_masuk`, `tgl_keluar`), 
                        `jml_ikan`, `nilai_pendapatan`, 
                        `nama_jenis_ikan`, `kebutuhan_bbm`, 
                        `biaya_operasional`, 
                        `jml_abk` 
                    FROM db_monev_kapi.trs_produksi as trs_produksi
                    LEFT JOIN db_monev_kapi.mst_inka_mina as mst_inka_mina 
                        ON `mst_inka_mina`.`id_kapal` = `trs_produksi`.`id_kapal` 
                    LEFT JOIN (SELECT id_produksi, group_concat(nama_jenis_ikan separator ', ') AS nama_jenis_ikan 
                                FROM db_monev_kapi.trs_produksi as trs_produksi 
                                LEFT JOIN db_master.mst_jenis_ikan as mst_jenis_ikan
                                    ON find_in_set(mst_jenis_ikan.id_jenis_ikan, trs_produksi.id_jenis_ikan) 
                                GROUP BY id_produksi) jenis_ikan 
                        ON `jenis_ikan`.`id_produksi` = `trs_produksi`.`id_produksi` 
                    LEFT JOIN db_master.mst_dpi as mst_dpi 
                        ON `mst_dpi`.`id_dpi` = `trs_produksi`.`id_dpi` 
                    LEFT JOIN db_master.mst_wpp as mst_wpp
                        ON `mst_wpp`.`id_wpp` = `mst_dpi`.`id_wpp` 
                    WHERE `trs_produksi`.`id_kapal` = '".$id."'
                ";
        $run_query = $this->db->query($query);
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = FALSE;
        }
        return $result;
    }
}
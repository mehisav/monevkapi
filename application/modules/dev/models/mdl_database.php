<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_database extends CI_Model
{
	private $db_kapi;

    function __construct()
    {
        parent::__construct();
    }

    public function jumlah_kapal_propinsi()
    {
        $query = 'SELECT dsp.nama_propinsi AS "value", 
                        COUNT(*) AS "jml_kapal"
                    FROM db_monev_kapi.mst_inka_mina AS dmk
                    LEFT JOIN db_master.mst_kabupaten_kota AS dskk ON dskk.id_kabupaten_kota = dmk.id_kab_kota
                    LEFT JOIN db_master.mst_propinsi AS dsp ON dskk.id_propinsi = dsp.id_propinsi
                    WHERE dmk.aktif="Ya" AND dmk.id_kab_kota <> 0
                    GROUP BY dsp.id_propinsi
            ';
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    
    public function get_data()
    {
        $query = "SELECT mp.id_propinsi as id_provinsi,
                        mp.nama_propinsi as provinsi,
                        count(*) as jumlah_kapal
                FROM db_monev_kapi.mst_inka_mina as mim 
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON mkk.id_kabupaten_kota = mim.id_kab_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                WHERE mp.id_propinsi IS NOT NULL AND mim.aktif = 'Ya'
                GROUP BY mp.id_propinsi
        ";
        // $query = 'SELECT id_propinsi as id_provinsi, provinsi, count(*) as jumlah_kapal FROM db_monev_kapi.mst_inka_mina mim, db_master.mst_propinsi mp where provinsi <> "null" AND mim.provinsi = mp.nama_propinsi group by provinsi;';
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function kapal_inka_lama()
    {
        $this->db_monev = $this->load->database('db_monev', TRUE);
        $query = "SELECT *
                FROM monev_kapi.mst_inka_mina AS mim
            ";
        $run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function kapal_dss($nama_kapal)
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $nk_array = explode(' ', $nama_kapal);

        $where_str = '';
        foreach ($nk_array as $value) {
            $where_str .= ' AND dmk.nama_kapal LIKE "%'.$value.'%"';
        }
        $query = 'SELECT  dmk.*, mi.no_sipi, mi.tanggal_sipi AS "tgl_sipi", mi.tanggal_akhir_sipi AS "tgl_akhir_sipi"
                FROM    mst_kapal as dmk
                LEFT JOIN mst_izin as mi ON mi.id_kapal = dmk.id_kapal 
                WHERE dmk.aktif = "YA"'.$where_str;
        $run_query = $this->db_dss->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_database_inka_kub()
    {
        $this->db_monev_baru = $this->load->database('default', TRUE);
        $data_inka = $this->kapal_inka_lama();

        foreach ($data_inka as $value)
        {
            $id_bahan_kapal = '0';
            if(strtoupper($value->bahan_kapal)=='FIBER') $id_bahan_kapal=5;
            elseif(strtoupper($value->bahan_kapal)=='KAYU') $id_bahan_kapal=2;
            $data = array(
                'id_kub' => $value->id_kub,
                'id_kab_kota' => $value->kab_kota,
                'nama_kapal' => $value->nama_kapal,
                'tahun_pembuatan' => $value->tahun_pembuatan,
                'tanda_selar' => $value->tanda_selar,
                'id_bahan_kapal' => $id_bahan_kapal,
                'gt' => $value->gt,
                'panjang_kapal' => $value->panjang_kapal,
                'lebar_kapal' => $value->lebar_kapal,
                'dalam_kapal' => $value->dalam_kapal,
                'no_mesin' => '',
                'mesin' => $value->mesin,
                'daya' => $value->daya,
                'pelabuhan_pangkalan' => $value->pelabuhan_pangkalan,
                'gross_akte' => $value->gross_akte,
                'siup' => $value->siup,
                'sipi' => $value->sipi,
                'tanggal_sipi' => '',
                'tanggal_akhir_sipi' => '',
                'kontraktor_pembangunan' => $value->kontraktor_pembangunan,
                'lokasi_pembangunan' => $value->lokasi_pembangunan,
                'foto_pembuatan' => $value->foto_pembuatan,
                'foto_sea_trial' => $value->foto_sea_trial,
                'foto_oprasional' => $value->foto_oprasional,
                'foto_siup1' => $value->foto_siup1,
                'foto_siup2' => $value->foto_siup2,
                'foto_bast1' => $value->foto_bast1,
                'foto_bast2' => $value->foto_bast2,
                'id_dss' => '',
                'id_pengguna_buat' => $value->id_pengguna_buat,
                'tanggal_buat' => $value->tanggal_buat,
                'id_pengguna_ubah' => $value->id_pengguna_ubah,
                'tanggal_ubah' => $value->tanggal_ubah,
                'aktif' => $value->aktif
            );
            
            $data_dss = $this->kapal_dss($value->nama_kapal);
            if($data_dss != false){
                $data['id_bahan_kapal'] = (isset($data_dss->id_bahan_kapal)) ? $data_dss->id_bahan_kapal : $data['id_bahan_kapal'];
                $data['id_alat_tangkap'] = (isset($data_dss->id_alat_tangkap)) ? $data_dss->id_alat_tangkap : $data['id_alat_tangkap'];
                $data['nama_kapal'] = (isset($data_dss->nama_kapal)) ? $data_dss->nama_kapal : $data['nama_kapal'];
                $data['tanda_selar'] = (isset($data_dss->tanda_selar)) ? $data_dss->tanda_selar : $data['tanda_selar'];
                $data['gt'] = (isset($data_dss->gt_kapal)) ? $data_dss->gt_kapal : $data['gt'];
                $data['panjang_kapal'] = (isset($data_dss->panjang_kapal)) ? $data_dss->panjang_kapal : $data['panjang_kapal'];
                $data['lebar_kapal'] = (isset($data_dss->lebar_kapal)) ? $data_dss->lebar_kapal : $data['lebar_kapal'];
                $data['dalam_kapal'] = (isset($data_dss->dalam_kapal)) ? $data_dss->dalam_kapal : $data['dalam_kapal'];
                $data['no_mesin'] = $data_dss->no_mesin;
                $data['mesin'] = (isset($data_dss->merek_mesin)) ? $data_dss->merek_mesin : $data['mesin'];
                $data['daya'] = (isset($data_dss->daya_kapal)) ? $data_dss->daya_kapal : $data['daya'];
                $data['gross_akte'] = (isset($data_dss->nomor_gross_akte)) ? $data_dss->tempat_gross_akte.'/'.$data_dss->nomor_gross_akte : $data['gross_akte'];
                $data['sipi'] = (isset($data_dss->sipi)) ? $data_dss->sipi : $data['sipi'];
                $data['tanggal_sipi'] = (isset($data_dss->tgl_sipi)) ? $data_dss->tgl_sipi : '';
                $data['tanggal_akhir_sipi'] = (isset($data_dss->tgl_akhir_sipi)) ? $data_dss->tgl_akhir_sipi : '';
                $data['lokasi_pembangunan'] = (isset($data_dss->tempat_pembangunan)) ? $data_dss->tempat_pembangunan : $data['lokasi_pembangunan'];
                $data['id_dss'] = $data_dss->id_kapal;
            }
            $this->db_monev_baru->insert('mst_inka_mina',$data);
        }
    }

}
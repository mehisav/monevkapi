<?php 
  /**
  * 
  */
  class Init_tasks
  {
    
    private $CI;
    private $url_login_integrasi = 'http://integrasi.djpt.kkp.go.id/login_baru/';
    private $url_menu_integrasi = 'http://integrasi.djpt.kkp.go.id/login_baru/portal/main';
    private $public_access = array();
    private $id_aplikasi_otoritas_kapi = "1";
    private $is_logged_in = FALSE;

    function __construct()
    {
      $this->CI =& get_instance();
      $this->CI->load->model('pengguna_kapi/mdl_pengguna_kapi');
      // $this->CI->load->config('custom_constants');
      // $this->public_access = $this->CI->config->item('public_access'); 
    }

    function check_login()
    {
      // var_dump(strpos($this->CI->session->userdata('id_aplikasi'), $this->id_aplikasi_otoritas_kapi ) !== FALSE);
      $id_app = $this->CI->session->userdata('id_aplikasi');
      $whitelist = array('127.0.0.1', '::1');
      // var_dump($id_app);
      // die;
      if( empty($id_app) && !in_array($_SERVER['REMOTE_ADDR'], $whitelist ) )
      {
        // var_dump('empty_id_app');
        // die;
        header( 'Location: '.$this->url_login_integrasi ) ;
      }
      elseif($id_app !== FALSE && strpos($id_app, $this->id_aplikasi_otoritas_kapi ) === FALSE ){
        // die;
        header( 'Location: '.$this->url_menu_integrasi ) ;
      }
    }

    function load_custom_constants()
    {
      $this->CI->load->config('custom_constants');
    }

    function ci_profiler()
    {
      //Check if in dev_mode
      $isDebugMode = $this->CI->input->get('dbg');

      if($isDebugMode === '1'){
        $this->CI->output->enable_profiler(TRUE);       
      }
    }

    function get_info_pengguna_kapi()
    {
      
      $id_pengguna = $this->CI->session->userdata('id_pengguna');
      $id_pengguna = 269;
      // vdump($this->CI->session->all_userdata('id_pengguna'), true);

      $arr_info_pengguna_kapi = $this->CI->mdl_pengguna_kapi->info_pengguna_kapi($id_pengguna);

      $userdata_pengguna = array();
      
      if($arr_info_pengguna_kapi){
        foreach ($arr_info_pengguna_kapi as $key => $value) {
          $userdata_pengguna[$key] = $value;
        }
        $this->CI->session->set_userdata($userdata_pengguna);
      }



    }

    
  }
?>
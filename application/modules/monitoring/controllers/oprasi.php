<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oprasi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_oprasi');
		}

	public function index()
	{
		$this->oprasi();

	}

	public function oprasi( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'oprasi_kapal';
		$data['jumlah_kapal']= $this->mdl_oprasi->jumlah_kapal();
		$data['jumlah_sipi']= $this->mdl_oprasi->jumlah_kapal_sipi();
		$data['jumlah_belum_sipi']= $this->mdl_oprasi->jumlah_kapal_tidak_sipi();
		$data['aktif_berhenti']= $this->mdl_oprasi->aktif_berhenti();
		$data['kapal_aktif']= $this->mdl_oprasi->kapal_aktif();
		$data['aktif_sipi']= $this->mdl_oprasi->aktif_sipi();
		$data['habis_sipi']= $this->mdl_oprasi->habis_sipi();
		$data['izin_dinas']= $this->mdl_oprasi->izin_dinas();
		$data['total_lapor']= $this->mdl_oprasi->total_lapor();
		$data['sudah_lapor']= $this->mdl_oprasi->sudah_lapor();
		$data['berhenti_oprasional']= $this->mdl_oprasi->berhenti_oprasional();
		$data['rusak_mesin']= $this->mdl_oprasi->rusak_mesin();
		$data['rusak_body']= $this->mdl_oprasi->rusak_body();
		$data['kebakaran']= $this->mdl_oprasi->kebakaran();
		$data['cuaca_buruk']= $this->mdl_oprasi->cuaca_buruk();
		$data['merugi']= $this->mdl_oprasi->merugi();
		$data['tidak_sipi']= $this->mdl_oprasi->tidak_sipi();
		$data['kurang_api']= $this->mdl_oprasi->kurang_api();
		$data['kurang_body']= $this->mdl_oprasi->kurang_body();
		$data['tidak_biaya']= $this->mdl_oprasi->tidak_biaya();
		$data['kendala_pemasaran']= $this->mdl_oprasi->kendala_pemasaran();
		$data['kendala_rumpon']= $this->mdl_oprasi->kendala_rumpon();
		$data['kendala_bbm']= $this->mdl_oprasi->kendala_bbm();
		$data['kendala_vms']= $this->mdl_oprasi->tidak_biaya();
		$data['belum_oprasi']= $this->mdl_oprasi->belum_oprasi();
		
		echo Modules::run('templates/page/v_form', 'monitoring', 'data_kapal_oprasi', $labels, $add_js, $add_css, $data);

	}

    public function info_kapal( $filter = '' ){
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter == 'jumlah_kapal' ){
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal('');
			$data['title'] = 'MONITORING TOTAL KAPAL';
			$data['filter'] = 'jumlah_kapal';

		}else if ($filter == 'jumlah_sipi' )
		{
			$where = 'AND sipi <> ""';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL MEMILIKI SIPI';
			$data['filter'] = 'jumlah_sipi';

		}else if ($filter == 'jumlah_belum_sipi' )
		{
			$where = 'AND sipi = ""';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM MEMILIKI SIPI';
			$data['filter'] = 'jumlah_belum_sipi';

		}else if ($filter == 'aktif_berhenti' )
		{
			$where = 'AND (id_permasalahan = 1 OR id_permasalahan = 2 OR id_permasalahan = 3 OR id_permasalahan = 12 OR id_permasalahan = 4 OR id_permasalahan = 0)';
			$data['title'] = 'MONITORING KAPAL OPRASIONAL DAN BERHENTI OPRASIONAL';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['filter'] = 'aktif_berhenti';

		}else if ($filter == 'kapal_aktif' )
		{
			$where = 'AND id_permasalahan = 0';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL OPRASIONAL';
			$data['filter'] = 'kapal_aktif';

		}else if ($filter == 'aktif_sipi' )
		{
			$where = 'AND tanggal_akhir_sipi > CURDATE()
                    AND (sipi <> "" 
                    OR LOCATE("izin", sipi) = 0
                    OR LOCATE("daerah", sipi) = 0)';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL AKTIF SIPI';
			$data['filter'] = 'aktif_sipi';

		}else if ($filter == 'habis_sipi' )
		{
			$where = 'AND tanggal_akhir_sipi < CURDATE()
                AND (sipi <> "" 
                    OR LOCATE("izin", sipi) = 0
                    OR LOCATE("daerah", sipi) = 0)';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL SIPI EXPIRED';
			$data['filter'] = 'habis_sipi';

		}else if ($filter == 'izin_dinas' )
		{
			$where = 'AND( LOCATE("izin", sipi) <> 0
                    OR LOCATE("daerah", sipi) <> 0)';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL IZIN DAERAH';
			$data['filter'] = 'izin_dinas';
	
		}else if ($filter == 'sudah_lapor' )
		{
			$where = 'AND tp.jumlah_lapor > 0';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL SUDAH LAPOR';
			$data['filter'] = 'sudah_lapor';

		}else if ($filter == 'belum_lapor' )
		{
			$where = 'AND tp.jumlah_lapor IS NULL';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM LAPOR';
			$data['filter'] = 'belum_lapor';

		}else if ($filter == 'berhenti_orpasional' )
		{
			$where = 'AND (id_permasalahan = 1 
                        OR id_permasalahan = 2
                        OR id_permasalahan = 3
                        OR id_permasalahan = 4
                        OR id_permasalahan = 12
                    )';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BERHENTI OPRASIONAL';
			$data['filter'] = 'berhenti_orpasional';

		}else if ($filter == 'rusak_mesin' )
		{
			$where = 'AND id_permasalahan = 1';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL RUSAK MESIN';
			$data['filter'] = 'rusak_mesin';

		}else if ($filter == 'rusak_body' )
		{
			$where = 'AND id_permasalahan = 2';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL RUSAK BODY';
			$data['filter'] = 'rusak_body';

		}else if ($filter == 'kebakaran' )
		{
			$where = 'AND id_permasalahan = 3';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL KEBAKARAN';
			$data['filter'] = 'kebakaran';

		}else if ($filter == 'cuaca_buruk' )
		{
			$where = 'AND id_permasalahan = 4';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BERHENTI OPRASIONAL KARENA CUACA BURUK';
			$data['filter'] = 'cuaca_buruk';

		}else if ($filter == 'merugi' )
		{
			$where = 'AND id_permasalahan = 12';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BERHENTI OPRASIONAL KARENA MERUGI';
			$data['filter'] = 'merugi';

		}else if ($filter == 'belum_oprasi' )
		{
			$where = 'AND tp.jumlah_lapor IS NULL';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASI';
			$data['filter'] = 'belum_oprasi';

		}else if ($filter == 'tidak_biaya' )
		{
			$where = 'AND id_permasalahan = 5';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASIONAL TIDAK ADA BIAYA';
			$data['filter'] = 'tidak_biaya';

		}else if ($filter == 'kurang_body' )
		{
			$where = 'AND id_permasalahan = 6';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASIONAL BODY TIDAK SESUAI SPESIFIKASI KUB';
			$data['filter'] = 'kurang_body';

		}else if ($filter == 'kurang_api' )
		{
			$where = 'AND id_permasalahan = 7';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASIONAL BODY TIDAK SESUAI SPESIFIKASI KUB';
			$data['filter'] = 'kurang_api';

		}else if ($filter == 'kendala_oprasional' )
		{
			$where = 'AND (id_permasalahan = 8 OR id_permasalahan = 13 OR id_permasalahan = 11 OR id_permasalahan = 8)';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA OPRASIONAL';
			$data['filter'] = 'kendala_oprasional';

		}else if ($filter == 'kendala_rumpon' )
		{
			$where = 'AND id_permasalahan = 7';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA RUMPON';
			$data['filter'] = 'kendala_rumpon';

		}else if ($filter == 'kendala_bbm' )
		{
			$where = 'AND id_permasalahan = 13';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA BBM';
			$data['filter'] = 'kendala_bbm';

		}else if ($filter == 'kendala_vms' )
		{
			$where = 'AND id_permasalahan = 11';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA VMS';
			$data['filter'] = 'kendala_vms';

		}else if ($filter == 'kendala_pemasaran' )
		{
			$where = 'AND id_permasalahan = 8';
			$data['data_kapal'] = $this->mdl_oprasi->data_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA PEMASARAN';
			$data['filter'] = 'kendala_pemasaran';

		}else
		{
			$data['data_kapal'] = 0;
		}
		// $data['submit_form'] = "monitoring/oprasi/tabel_kapal_inka";
		$labels = 'tabel_jumlah_kapal';
		$template = 'templates/page/v_form';
		$modules = 'monitoring';
		$views = 'detail_kapal_info';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
    }

    public function export_kapal( $filter = '' ){
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter == 'jumlah_kapal' ){
			$data_kapal = $this->mdl_oprasi->daftar_kapal('');
			$data['title'] = 'MONITORING TOTAL KAPAL';

		}else if ($filter == 'jumlah_sipi' )
		{
			$where = 'AND mim.sipi <> ""';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL MEMILIKI SIPI';
			$data['filter'] = 'jumlah_sipi';

		}else if ($filter == 'jumlah_belum_sipi' )
		{
			$where = 'AND mim.sipi = ""';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM MEMILIKI SIPI';
			$data['filter'] = 'jumlah_belum_sipi';

		}else if ($filter == 'aktif_berhenti' )
		{
			$where = 'AND (id_permasalahan = 1 OR id_permasalahan = 2 OR id_permasalahan = 3 OR id_permasalahan = 12 OR id_permasalahan = 4 OR id_permasalahan = 0)';
			$data['title'] = 'MONITORING KAPAL OPRASIONAL DAN BERHENTI OPRASIONAL';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['filter'] = 'aktif_berhenti';

		}else if ($filter == 'kapal_aktif' )
		{
			$where = 'AND id_permasalahan = 0';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL OPRASIONAL';
			$data['filter'] = 'kapal_aktif';

		}else if ($filter == 'aktif_sipi' )
		{
			$where = 'AND mim.tanggal_akhir_sipi > CURDATE()
                    AND (mim.sipi <> "" 
                    OR LOCATE("izin", mim.sipi) = 0
                    OR LOCATE("daerah", mim.sipi) = 0)';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL AKTIF SIPI';
			$data['filter'] = 'aktif_sipi';

		}else if ($filter == 'habis_sipi' )
		{
			$where = 'AND mim.tanggal_akhir_sipi < CURDATE()
                AND (mim.sipi <> "" 
                    OR LOCATE("izin", mim.sipi) = 0
                    OR LOCATE("daerah", mim.sipi) = 0)';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL SIPI EXPIRED';
			$data['filter'] = 'habis_sipi';

		}else if ($filter == 'izin_dinas' )
		{
			$where = 'AND( LOCATE("izin", mim.sipi) <> 0
                    OR LOCATE("daerah", mim.sipi) <> 0)';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL IZIN DAERAH';
			$data['filter'] = 'izin_dinas';
	
		}else if ($filter == 'sudah_lapor' )
		{
			$where = 'AND tp.jml_opreasi > 0';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL SUDAH LAPOR';
			$data['filter'] = 'sudah_lapor';

		}else if ($filter == 'belum_lapor' )
		{
			$where = 'AND tp.jml_opreasi IS NULL';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			// vdump($data);
			// echo "string";
			$data['title'] = 'MONITORING KAPAL BELUM LAPOR';
			$data['filter'] = 'belum_lapor';

		}else if ($filter == 'berhenti_orpasional' )
		{
			$where = 'AND (id_permasalahan = 1 
                        OR id_permasalahan = 2
                        OR id_permasalahan = 3
                        OR id_permasalahan = 4
                        OR id_permasalahan = 12
                    )';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BERHENTI OPRASIONAL';
			$data['filter'] = 'berhenti_orpasional';

		}else if ($filter == 'rusak_mesin' )
		{
			$where = 'AND id_permasalahan = 1';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL RUSAK MESIN';
			$data['filter'] = 'rusak_mesin';

		}else if ($filter == 'rusak_body' )
		{
			$where = 'AND id_permasalahan = 2';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL RUSAK BODY';
			$data['filter'] = 'rusak_body';

		}else if ($filter == 'kebakaran' )
		{
			$where = 'AND id_permasalahan = 3';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL KEBAKARAN';
			$data['filter'] = 'kebakaran';

		}else if ($filter == 'cuaca_buruk' )
		{
			$where = 'AND id_permasalahan = 4';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BERHENTI OPRASIONAL KARENA CUACA BURUK';
			$data['filter'] = 'cuaca_buruk';

		}else if ($filter == 'merugi' )
		{
			$where = 'AND id_permasalahan = 12';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BERHENTI OPRASIONAL KARENA MERUGI';
			$data['filter'] = 'merugi';

		}else if ($filter == 'belum_oprasi' )
		{
			$where = 'AND tp.jml_opreasi IS NULL';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASI';
			$data['filter'] = 'belum_oprasi';

		}else if ($filter == 'tidak_biaya' )
		{
			$where = 'AND id_permasalahan = 5';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASIONAL TIDAK ADA BIAYA';
			$data['filter'] = 'tidak_biaya';

		}else if ($filter == 'kurang_body' )
		{
			$where = 'AND id_permasalahan = 6';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASIONAL BODY TIDAK SESUAI SPESIFIKASI KUB';
			$data['filter'] = 'kurang_body';

		}else if ($filter == 'kurang_api' )
		{
			$where = 'AND id_permasalahan = 7';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL BELUM OPRASIONAL BODY TIDAK SESUAI SPESIFIKASI KUB';
			$data['filter'] = 'kurang_api';

		}else if ($filter == 'kendala_oprasional' )
		{
			$where = 'AND (id_permasalahan = 8 OR id_permasalahan = 13 OR id_permasalahan = 11 OR id_permasalahan = 8)';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA OPRASIONAL';
			$data['filter'] = 'kendala_oprasional';

		}else if ($filter == 'kendala_rumpon' )
		{
			$where = 'AND id_permasalahan = 7';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA RUMPON';
			$data['filter'] = 'kendala_rumpon';

		}else if ($filter == 'kendala_bbm' )
		{
			$where = 'AND id_permasalahan = 13';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA BBM';
			$data['filter'] = 'kendala_bbm';

		}else if ($filter == 'kendala_vms' )
		{
			$where = 'AND id_permasalahan = 11';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA VMS';
			$data['filter'] = 'kendala_vms';

		}else if ($filter == 'kendala_pemasaran' )
		{
			$where = 'AND id_permasalahan = 8';
			$data_kapal = $this->mdl_oprasi->daftar_kapal($where);
			$data['title'] = 'MONITORING KAPAL KENDALA PEMASARAN';
			$data['filter'] = 'kendala_pemasaran';

		}else
		{
			$data_kapal = false;
		}
		
		//load our new PHPExcel library
		$this->load->library('excel');
		$template = FCPATH.'assets\kapi\format\format_kapal.xlsx';
		// $template = dirname(__FILE__).'/'.$_GET['kode'].'.xlsx';
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSetting = array('memoryCacheSize' => '100MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSetting);

		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($template);
		$objWorksheet = $objPHPExcel->getSheet(0);

		/*content*/
		// $data_kapal = $this->mdl_siup->daftar_kapal_siup();
		$column = Array(
					"A","B","C","D","E","F","G","H","I","J","K","L","M",
					"N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
					"AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM",
					"AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
					"BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM",
					"BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ",
					"CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM",
					"CN","CO","CP","CQ","CR","CS","CT","CU","CV","CW","CX","CY","CZ",
					"DA","DB","DC","DD","DE","DF","DG","DH","DI","DJ","DK","DL","DM",
					"DN","DO","DP","DQ","DR","DS","DT","DU","DV","DW","DX","DY","DZ",
					"EA","EB","EC","ED","EE","EF","EG","EH","EI","EJ","EK","EL","EM",
					"EN","EO","EP","EQ","ER","ES","ET","EU","EV","EW","EX","EY","EZ",
					"FA","FB","FC","FD","FE","FF","FG","FH","FI","FJ","FK","FL","FM",
					"FN","FO","FP","FQ","FR","FS","FT","FU","FV","FW","FX","FY","FZ",
					"GA","GB","GC","GD","GE","GF","GG","GH","GI","GJ","GK","GL","GM",
					"GN","GO","GP","GQ","GR","GS","GT","GU","GV","GW","GX","GY","GZ",
					"HA","HB","HC","HD","HE","HF","HG","HH","HI","HJ","HK","HL","HM",
					"HN","HO","HP","HQ","HR","HS","HT","HU","HV","HW","HX","HY","HZ",
					"IA","IB","IC","ID","IE","IF","IG","IH","II","IJ","IK","IL","IM",
					"IN","IO","IP","IQ","IR","IS","IT","IU","IV","IW","IX","IY","IZ"
					);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['title']);
		$rowNumber = 6;

		foreach ($data_kapal as $item)
		{
			$col=1;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber, ($rowNumber-5));
			// vdump($item);
			foreach($item as $key => $cell) {
				// $objPHPExcel->getActiveSheet()->setCellValue($column[$col].$rowNumber, "A");
				$objPHPExcel->getActiveSheet()->setCellValue($column[$col].$rowNumber, $cell);
				$col++;
			}

			$rowNumber++;
		}
		/*End Content*/
		// die();
		//Border
		$styleArray = array(
		    'borders' => array(
		      'allborders' => array(
		          'style' => PHPExcel_Style_Border::BORDER_THIN
		      )
		    )
		);
		$objPHPExcel->getActiveSheet()->getStyle('A6:AM'.($rowNumber-1))->applyFromArray($styleArray);
		// Save as an Excel BIFF (xls) file 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename=" Data Master Kapal - '.date("d-m-Y h:i").'.xlsx"'); 
		header('Cache-Control: max-age=0'); 

		$objWriter->save('php://output'); 
    }
}
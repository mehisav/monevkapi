<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_produksi extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        parent::__construct();
        
    }

    public function list_produksi()
    {
        $this->load->database();
        $query = 'SELECT tp.id_produksi,
                            mim.nama_kapal,
                            mw.nama_wpp,
                            md.nama_dpi,
                            mim.gt,
                            tp.tgl_keluar,
                            tp.tgl_masuk,
                            mpbk.nama_pelabuhan as nama_pelabuhan_keluar,
                            mpbm.nama_pelabuhan as nama_pelabuhan_masuk,
                            tp.jml_ikan,
                            tp.nilai_pendapatan,
                            mji.id_jenis_ikan,
                            tp.kebutuhan_bbm,
                            tp.biaya_operasional,
                            tp.nama_nahkoda,
                            tp.jml_abk
                    FROM db_monev_kapi.trs_produksi as tp
                    INNER JOIN db_monev_kapi.mst_inka_mina as mim ON mim.id_kapal = tp.id_kapal
                    LEFT JOIN db_master.mst_pelabuhan as mpbk ON mpbk.id_pelabuhan = tp.id_pelabuhan_keluar
                    LEFT JOIN db_master.mst_pelabuhan as mpbm ON mpbm.id_pelabuhan = tp.id_pelabuhan_masuk
                    LEFT JOIN db_master.mst_dpi as md ON md.id_dpi = tp.id_dpi
                    LEFT JOIN db_master.mst_wpp as mw ON mw.id_wpp = md.id_wpp
                    LEFT JOIN (SELECT tp.id_produksi, 
                                        group_concat(nama_jenis_ikan separator ", ") AS id_jenis_ikan
                                FROM db_monev_kapi.trs_produksi as tp
                                LEFT JOIN db_master.mst_jenis_ikan as mji 
                                    ON find_in_set(mji.id_jenis_ikan, tp.id_jenis_ikan)
                                GROUP BY tp.id_produksi) mji
                            ON mji.id_produksi = tp.id_produksi
                    WHERE tp.aktif = "Ya" 
                    ORDER BY tgl_masuk DESC';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_produksi($id)
    {
        $this->load->database();
        $query = 'SELECT tp.id_produksi,
                            mim.id_kapal,
                            mim.nama_kapal,
                            mw.nama_wpp,
                            md.id_dpi,
                            md.nama_dpi,
                            mim.gt,
                            tp.tgl_keluar,
                            tp.tgl_masuk,
                            mpbk.nama_pelabuhan as nama_pelabuhan_keluar,
                            tp.id_pelabuhan_keluar,
                            mpbm.nama_pelabuhan as nama_pelabuhan_masuk,
                            tp.id_pelabuhan_masuk,
                            tp.jml_ikan,
                            tp.nilai_pendapatan,
                            tp.id_jenis_ikan,
                            tp.kebutuhan_bbm,
                            tp.biaya_operasional,
                            tp.nama_nahkoda,
                            tp.jml_abk
                    FROM db_monev_kapi.trs_produksi as tp
                    INNER JOIN db_monev_kapi.mst_inka_mina as mim ON mim.id_kapal = tp.id_kapal
                    LEFT JOIN db_master.mst_pelabuhan as mpbk ON mpbk.id_pelabuhan = tp.id_pelabuhan_keluar
                    LEFT JOIN db_master.mst_pelabuhan as mpbm ON mpbk.id_pelabuhan = tp.id_pelabuhan_masuk
                    LEFT JOIN db_master.mst_dpi as md ON md.id_dpi = tp.id_dpi
                    LEFT JOIN db_master.mst_wpp as mw ON mw.id_wpp = md.id_wpp
                    WHERE tp.aktif = "Ya" AND tp.id_produksi = "'.$id.'"
                    ORDER BY tgl_masuk DESC';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function pipp_data_masuk($tgl_keluar,$id_kapal)
    {
        $query ="SELECT tak.id_aktivitas_kapal, tak.id_kapal, tak.id_pelabuhan as id_pelabuhan_masuk, 
                        tak.tgl_aktivitas as tgl_masuk
                FROM db_pipp.trs_aktivitas_kapal as tak
                WHERE tak.tgl_aktivitas >= '$tgl_keluar' AND tak.id_kapal = $id_kapal AND tak.aktivitas='masuk'
                ORDER BY tak.tgl_aktivitas
        ";

        $run_query = $this->db->query($query);                           
        // echo $this->db->last_query();die();
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function pipp_data_produksi($tgl_keluar, $id_kapal)
    {
        // echo "makan";
        $this->db_pipp = $this->load->database('db_pipp', TRUE);
        $kpl_masuk = $this->pipp_data_masuk($tgl_keluar, $id_kapal);
        
        if($kpl_masuk != false)
        {
            $data = array(
                    'id_aktivitas_kapal' => $kpl_masuk->id_aktivitas_kapal,
                    'id_kapal' => $kpl_masuk->id_kapal,
                    'id_pelabuhan_masuk' => $kpl_masuk->id_pelabuhan_masuk,
                    'tgl_masuk' => $kpl_masuk->tgl_masuk,
                    'id_dpi' => '',
                    'id_jenis_ikan' => '',
                    'jml_ikan' => 0,
                    'harga_pendapatan' => 0
                );

            $query = "SELECT tp.id_kapal,
                                tp.id_dpi,
                                tp.id_jenis_ikan, 
                                tp.jml_ikan, 
                                tp.harga_produsen*tp.jml_ikan as harga_pendapatan
                    FROM db_pipp.trs_produksi as tp 
                    WHERE tp.id_aktivitas_referensi = '".$kpl_masuk->id_aktivitas_kapal."'
                    ORDER BY tp.jml_ikan DESC
            ";

            $run_query = $this->db_pipp->query($query);
            if($run_query->num_rows() > 0){
                $data_produksi = $run_query->result();
                $i = 1;

                vdump($data_produksi);
                foreach ($data_produksi as $value) {
                    $a='';
                    if($i===1){
                        $a = $value->id_jenis_ikan;
                        $data['id_dpi'] = $value->id_dpi; 
                    }else
                    {
                        $a =", ".$value->id_jenis_ikan;
                    }

                    $data['id_jenis_ikan'] .= $a;
                    $data['jml_ikan'] += $value->jml_ikan;
                    $data['harga_pendapatan'] += $value->harga_pendapatan;
                    $i++;
                }
            }

            $result = $data;
        }else
        {
            $result = false;
        }
        return $result;
    }

    public function pipp_data_keluar()
    {
        $query ="SELECT DISTINCT tak.id_aktivitas_kapal, mim.id_kapal as id_kapal_monev, tak.id_kapal, tak.id_pelabuhan as id_pelabuhan_keluar, 
                        tak.tgl_aktivitas as tgl_keluar, tak.nama_nahkoda, 
                        tak.jumlah_abk, tpkm.jml_perbekalan as biaya_operasional,
                        tpks.jml_perbekalan as kebutuhan_bbm
                FROM db_pipp.trs_aktivitas_kapal as tak
                LEFT JOIN db_monev_kapi.trs_produksi as tp ON tp.id_aktivitas_keluar = tak.id_aktivitas_kapal
                INNER JOIN db_monev_kapi.mst_inka_mina as mim ON mim.id_dss = tak.id_kapal 
                LEFT JOIN db_pipp.trs_perbekalan_kapal_keluar as tpkm 
                    ON tak.id_aktivitas_kapal = tpkm.id_aktivitas_kapal AND tpkm.id_jns_perbekalan = '2'
                LEFT JOIN db_pipp.trs_perbekalan_kapal_keluar as tpks 
                    ON tak.id_aktivitas_kapal = tpks.id_aktivitas_kapal AND tpks.id_jns_perbekalan = '1'
                WHERE tak.aktif = 'Ya' AND tak.aktivitas = 'keluar' AND tp.id_aktivitas_keluar IS NULL
        ";

        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_pipp()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $kpl_keluar = $this->pipp_data_keluar();

        foreach ($kpl_keluar as $value) {
            $kpl_masuk = $this->pipp_data_produksi($value->tgl_keluar, $value->id_kapal);
            if($kpl_masuk != false)
            {
                $data = array(
                        'id_kapal' => $value->id_kapal_monev,
                        'id_aktivitas_keluar' => $value->id_aktivitas_kapal,
                        'id_aktivitas_masuk' => $kpl_masuk['id_aktivitas_kapal'],
                        'id_dpi' => $kpl_masuk['id_dpi'],
                        'tgl_keluar' => $value->tgl_keluar,
                        'tgl_masuk' => $kpl_masuk['tgl_masuk'],
                        'id_pelabuhan_keluar' => $value->id_pelabuhan_keluar,
                        'id_pelabuhan_masuk' => $kpl_masuk['id_pelabuhan_masuk'],
                        'nama_nahkoda' => $value->nama_nahkoda,
                        'jml_abk' => $value->jumlah_abk,
                        'jml_ikan' => $kpl_masuk['jml_ikan'],
                        'nilai_pendapatan' => $kpl_masuk['harga_pendapatan'],
                        'id_jenis_ikan' => $kpl_masuk['id_jenis_ikan'],
                        'kebutuhan_bbm' => $value->kebutuhan_bbm,
                        'biaya_operasional' => $value->biaya_operasional,
                        'aktif' => 'Ya',
                    );
                $this->db_monev->insert('trs_produksi',$data);
            }
        }
    }

    public function select2($data = "")
    {
        $sql = "SELECT id_jenis_ikan AS id, nama_jenis_ikan AS text , nama_latin
                FROM db_master.mst_jenis_ikan order by id_jenis_ikan asc";
        if(!empty($data))
        {
            $sql .= " AND nama_jenis_ikan LIKE '%".$data['nama_jenis_ikan']."%'";
        }

        $run_query = $this->db->query($sql);
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function last_pipp()
    {
        $query ="SELECT tp.tgl_masuk
                FROM db_monev_kapi.trs_produksi as tp 
                WHERE tp.id_aktivitas_keluar <> 0
                ORDER BY tp.tgl_masuk DESC
            ";

        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $result = $this->db_monev->insert('trs_produksi', $data);
        if($result)
        {
            return $this->db_monev->insert_id();
        }
    }

    public function input_produksi_ikan($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $result = $this->db_monev->insert('trs_produksi_ikan', $data);
        return $result;
    }

    public function update($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->where('id_produksi', $data['id_produksi']);
        $query = $this->db_monev->update('trs_produksi',$data);

        if($this->db_monev->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function delete($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $sql = " UPDATE trs_produksi SET aktif='Tidak' WHERE id_produksi=$id ";
        
        $query = $this->db_monev->query($sql);
        //var_dump($sql);

    }
}
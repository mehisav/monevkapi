<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="<?=base_url('assets')?>/third_party/js/markerwithlabel.js"></script>
<script src="<?=base_url('assets')?>/third_party/js/map.js"></script>
<script type="text/javascript">
  var base_url = "<?=base_url()?>";
  c_link("dev/get_data");
  google.maps.event.addDomListener(window, "load", initialize);
</script>

<style type="text/css">

      @media screen and (min-width: 992px) {
        .the-icons li {
          width: 12.5%;
        }
      }

      #peta {
        height: 450px;
        width: 100%;
        /*padding: 0px;*/
      }
      .well-peta {
        padding: 0px !important;
      }
        
      .labels {
          background-color: white;
          border-style: solid;
          border-width: 1px;
      }

</style>


<div id="map_canvas" style="width:100%; height:300px; border: 2px solid #3872ac;"></div>


<?php 
if($chart==true)
{
?>
  <script type="text/javascript">
    window.onload = function () {
      var chart = new CanvasJS.Chart("chartContainer",
      {
        title:{
          text: "<?php echo $chart_title?>"
        },
        animationEnabled: true,
        axisY: {
          title: "Jumlah Kapal"
        },
        axisX:{
          labelAngle: -60
        },
        legend: {
          verticalAlign: "bottom",
          horizontalAlign: "center"
        },
        theme: "theme2",
        data: [

        {        
          type: "column",  
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Id/Jenis",
          dataPoints: [
          <?php 
          $i = 0;
      $len = count($chart_isi);
          foreach ($chart_isi AS $item) {
          ?>     
            {y: <?php echo $item->jml_kapal;?>, indexLabel:"<?php echo $item->jml_kapal;?>", label: "<?php echo $item->value?>"}<?php if($i!=$len-1) echo ",";?>      
          <?php 
            $i++;
        }
          ?>     
          ]
        }   
        ]
      });

      chart.render();
    }
    </script>
  <div id="chartContainer" style="height: 300px; width: 100%;">
  </div>
  
<?php
}
?>
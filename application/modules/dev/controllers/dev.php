<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dev extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $id_aplikasi_kapi = "4";

	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
	}
	
	public function get_data()
	{
		$this->load->model('mdl_database');
		echo json_encode($this->mdl_database->get_data());
	}

	public function index( $filter = '' )
	{
		$this->load->model('mdl_database');

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'report_peta_sebaran';
		$data['submit_form'] = "reporting/report_produksi";

		$data['chart'] = true;
		$data['chart_title'] = "Data Kapal Berdasarkan Provinsi";
		$data['chart_isi'] = $this->mdl_database->jumlah_kapal_propinsi();
		
		echo Modules::run('templates/page/v_form', 'dev', 'home', $labels, $add_js, $add_css, $data);

	}

	public function update_database(){
		$this->load->model('mdl_database');
		if( $this->mdl_database->update_database_inka_kub() ){
			$url = base_url();
			redirect($url);
		}else{
			$url = base_url();
			redirect($url);
		}
	}
	
	public function user($id){
		// $this->session->sess_destroy();
		switch ($id) {
			case '1':
				$user_data = array (
									'username' => 'eko',
									'kode_pengguna' => 'eko',
									'id_pengguna' => 269,
									'is_super_admin' => 0,
									'is_admin' => 1,
									'nama_pengguna' => 'eko',
									'id_propinsi' => 12,
									'id_kabupaten_kota' => 158,
									'id_divisi' => 1,
									'id_lokasi' =>  1, //pusat
									'id_grup_pengguna' => 2,
									'fullname' => 'eko',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				break;
			
			case '2':
				$user_data = array (
									'username' => 'hasan',
									'kode_pengguna' => 'hasan',
									'id_pengguna' => 270,
									'is_super_admin' => 0,
									'is_admin' => 0,
									'nama_pengguna' => 'hasan',
									'id_propinsi' => 1,
									'id_kabupaten_kota' => 9,
									'id_divisi' => 1,
									'id_lokasi' =>  2, //propinsi
									'id_grup_pengguna' => 2,
									'fullname' => 'hasan',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				break;

			case '3':
				$user_data = array (
									'username' => 'frendhi',
									'kode_pengguna' => 'frendhi',
									'nama_pengguna' => 'frendhi',
									'id_pengguna' => 708,
									'id_propinsi' => 1,
									'id_kabupaten_kota' => 9,
									'id_divisi' => 1,
									'id_lokasi' =>  3, //kabupaten
									'is_super_admin' => 0,
									'is_admin' => 0,
									'id_grup_pengguna' => 2,
									'fullname' => 'frendhi',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				break;
		}
		// vdump($this->session->all_userdata(), true);
		redirect('index.php?dbg=1');
	}

	public function _fakesession()
	{
			$user_data = array();
			$isLoggedIn = $this->session->userdata('logged_in');
      		$whitelist = array('127.0.0.1', '::1');

			if(!$isLoggedIn) {
				if( in_array($_SERVER['REMOTE_ADDR'], $whitelist ) )
				{
					$user_data = array (
									'username' => 'eko',
									'kode_pengguna' => 'eko',
									'id_pengguna' => 270,
									'is_super_admin' => 0,
									'is_admin' => 1,
									'nama_pengguna' => 'eko',
									'id_divisi' => 1,
									'id_lokasi' =>  1,
									'id_grup_pengguna' => 2,
									'fullname' => 'eko',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				}
			}
	}



	public function login_v()
	{
		$this->load->model('mdl_dev_progress');
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$this->_fakesession();

		$template = 'templates/page/v_form';
		$modules = 'dev';
		$views = 'v_login';
		$labels = 'form_login';
		$data['submit_form'] = 'dev/login';
		
		echo Modules::run($template, //tipe template
							$modules, //nama module
							$views, //nama file view
							$labels, //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function login()
	{

		$array_input = $this->input->post(NULL, TRUE);


		$temp = explode("|", $array_input['prop_kab_kota']);

		// vdump($array_input);

		$id_lokasi = $array_input['kategori_pengguna'];
		$id_propinsi = $temp[0];
		$id_kabupaten_kota = $temp[1];
		
		// vdump($temp, true);


		$this->batam_user($id_lokasi, $id_propinsi, $id_kabupaten_kota);

	}

	public function batam_user($id_lokasi, $id_propinsi, $id_kabupaten_kota){
		
		$this->load->model('mdl_dev_progress');
		$nama_prop_kabkota = $this->mdl_dev_progress->get_nama_prop_kabkota($id_propinsi, $id_kabupaten_kota);
		$data['nama_propinsi'] = $nama_prop_kabkota[0]->nama_propinsi;
		$data['nama_kabkota'] = $nama_prop_kabkota[0]->nama_kabupaten_kota;

		// vdump($data, true);


		switch ($id_lokasi) {
			case '1':
				$user_data = array (
									'username' => "PUSAT",
									'kode_pengguna' => 'PUSAT',
									'id_pengguna' => 111,
									'is_super_admin' => 0,
									'is_admin' => 0,
									'nama_pengguna' => 'PUSAT',
									'id_propinsi' => $id_propinsi,
									'id_kabupaten_kota' => $id_kabupaten_kota,
									'id_divisi' => 1,
									'id_lokasi' =>  1, //pusat
									'id_grup_pengguna' => 2,
									'fullname' => 'PUSAT',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				break;
			
			case '2':
				$user_data = array (
									'username' => $data['nama_propinsi'],
									'kode_pengguna' => $data['nama_propinsi'],
									'id_pengguna' => 222,
									'is_super_admin' => 0,
									'is_admin' => 0,
									'nama_pengguna' => $data['nama_propinsi'],
									'id_propinsi' => $id_propinsi,
									'id_kabupaten_kota' => $id_kabupaten_kota,
									'id_divisi' => 1,
									'id_lokasi' =>  2, //propinsi
									'id_grup_pengguna' => 2,
									'fullname' => $data['nama_propinsi'],
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				break;

			case '3':
				$user_data = array (
									'username' => $data['nama_kabkota'],
									'kode_pengguna' => $data['nama_kabkota'],
									'nama_pengguna' => $data['nama_kabkota'],
									'id_pengguna' => 333,
									'id_propinsi' => $id_propinsi,
									'id_kabupaten_kota' => $id_kabupaten_kota,
									'id_divisi' => 1,
									'id_lokasi' =>  3, //kabupaten
									'is_super_admin' => 0,
									'is_admin' => 0,
									'id_grup_pengguna' => 2,
									'fullname' => $data['nama_kabkota'],
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				break;
		}
		// vdump($this->session->all_userdata(), true);
		redirect('dev/index');
	}

	/*public function alter_table(){
		$this->load->model('mdl_dev_progress');
		$this->mdl_dev_progress->alter_table();
		echo "okee";
	}*/

}

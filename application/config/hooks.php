<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/
/*$hook['post_controller_constructor'][] = array(
                                'class'    => 'init_tasks',
                                'function' => 'check_login',
                                'filename' => 'init_tasks.php',
                                'filepath' => 'hooks'
                                );*/
$hook['post_controller_constructor'][] = array(
                                'class'    => 'init_tasks',
                                'function' => 'ci_profiler',
                                'filename' => 'init_tasks.php',
                                'filepath' => 'hooks'
                                );
/*$hook['post_controller_constructor'][] = array(
                                'class'    => 'init_tasks',
                                'function' => 'get_info_pengguna_kapi',
                                'filename' => 'init_tasks.php',
                                'filepath' => 'hooks'
                                );*/
// $hook['post_controller_constructor'][] = array(
//                                 'class'    => 'init_tasks',
//                                 'function' => 'load_custom_constants',
//                                 'filename' => 'init_user.php',
//                                 'filepath' => 'hooks'
//                                 );

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */
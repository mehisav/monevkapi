<?php

	//Olah data tampil untuk buku kapal
	$template = array( "table_open" 	=> 	"<table id='table_progress' class='table table-bordered '>"
					);
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	$bgcolor = '#3399FF';

	if($list_progress){
		foreach ($list_progress as $item) {

			$link_entry = ''.$item->nama_kapal.'';
			$this->table->add_row(
								$item->no_rekam_pendok,
								$item->nama_kapal,
								$item->nama_penanggung_jawab,
								$item->status_pendok!=NULL ? '<center>&radic;</center>' : '',
								$item->status_entry_bkp=="2" ? '<center>&radic;</center>' : '',
								$item->status_verifikasi=="YA" ? '<center>&radic;</center>' : '',
								$item->status_cetak_bkp=="YA" ? '<center>&radic;</center>' : '',
								$item->status_terima_bkp=="YA" ? '<center>&radic;</center>' : ''
								);
		}
	}

	$table_progress = $this->table->generate();

?>
<!-- Data ditampilkan -->
<?=$table_progress?>

<!-- ADTTIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_progress').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "aaSorting": [[0, 'desc']]
		} );
	} );
</script>
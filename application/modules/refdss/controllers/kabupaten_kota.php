<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kabupaten_kota extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_kabupaten_kota');
		}

	public function index()
	{
		$this->kabupaten_kota();

	}

	public function kabupaten_kota( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_kabupaten_kota';
		$data['list_kabupaten_kota'] = $this->mdl_kabupaten_kota->list_kabupaten_kota();
		
		echo Modules::run('templates/page/v_form', 'refdss', 'tabel_kabupaten_kota', $labels, $add_js, $add_css, $data);

	}

	public function list_kaprop_array()
	{
		$this->load->model('mdl_kabupaten_kota');

		$list_opsi = $this->mdl_kabupaten_kota->list_opsi_kaprop();
		// print_r($list_opsi);die;
		return $list_opsi;
	}
}
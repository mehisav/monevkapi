<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'db_monev_kapi';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


$db['db_dss']['hostname'] = 'localhost';
$db['db_dss']['username'] = 'root';
$db['db_dss']['password'] = '';
$db['db_dss']['database'] = 'db_master';
$db['db_dss']['dbdriver'] = 'mysql';
$db['db_dss']['dbprefix'] = '';
$db['db_dss']['pconnect'] = FALSE;
$db['db_dss']['db_debug'] = TRUE;
$db['db_dss']['cache_on'] = FALSE;
$db['db_dss']['cachedir'] = '';
$db['db_dss']['char_set'] = 'utf8';
$db['db_dss']['dbcollat'] = 'utf8_general_ci';
$db['db_dss']['swap_pre'] = '';
$db['db_dss']['autoinit'] = TRUE;
$db['db_dss']['stricton'] = FALSE;

// $db['db_kapi_lama']['hostname'] = 'localhost';
// $db['db_kapi_lama']['username'] = 'root';
// $db['db_kapi_lama']['password'] = '';
// $db['db_kapi_lama']['database'] = 'kapi_pendaftaran';
// $db['db_kapi_lama']['dbdriver'] = 'mysql';
// $db['db_kapi_lama']['dbprefix'] = '';
// $db['db_kapi_lama']['pconnect'] = FALSE;
// $db['db_kapi_lama']['db_debug'] = TRUE;
// $db['db_kapi_lama']['cache_on'] = FALSE;
// $db['db_kapi_lama']['cachedir'] = '';
// $db['db_kapi_lama']['char_set'] = 'utf8';
// $db['db_kapi_lama']['dbcollat'] = 'utf8_general_ci';
// $db['db_kapi_lama']['swap_pre'] = '';
// $db['db_kapi_lama']['autoinit'] = TRUE;
// $db['db_kapi_lama']['stricton'] = FALSE;

$db['db_pipp']['hostname'] = 'localhost';
$db['db_pipp']['username'] = 'root';
$db['db_pipp']['password'] = '';
$db['db_pipp']['database'] = 'db_pipp';
$db['db_pipp']['dbdriver'] = 'mysql';
$db['db_pipp']['dbprefix'] = '';
$db['db_pipp']['pconnect'] = FALSE;
$db['db_pipp']['db_debug'] = TRUE;
$db['db_pipp']['cache_on'] = FALSE;
$db['db_pipp']['cachedir'] = '';
$db['db_pipp']['char_set'] = 'utf8';
$db['db_pipp']['dbcollat'] = 'utf8_general_ci';
$db['db_pipp']['swap_pre'] = '';
$db['db_pipp']['autoinit'] = TRUE;
$db['db_pipp']['stricton'] = FALSE;

$db['db_monev']['hostname'] = 'localhost';
$db['db_monev']['username'] = 'root';
$db['db_monev']['password'] = '';
$db['db_monev']['database'] = 'monev_kapi';
$db['db_monev']['dbdriver'] = 'mysql';
$db['db_monev']['dbprefix'] = '';
$db['db_monev']['pconnect'] = FALSE;
$db['db_monev']['db_debug'] = TRUE;
$db['db_monev']['cache_on'] = FALSE;
$db['db_monev']['cachedir'] = '';
$db['db_monev']['char_set'] = 'utf8';
$db['db_monev']['dbcollat'] = 'utf8_general_ci';
$db['db_monev']['swap_pre'] = '';
$db['db_monev']['autoinit'] = TRUE;
$db['db_monev']['stricton'] = FALSE;
/* End of file database.php */
/* Location: ./application/config/database.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {

	function __construct()
		{
			parent::__construct();

			$this->load->model('mdl_pengguna_kapi');
		}

	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'pengguna_kapi';
		$views = 'tabel_pengguna_kapi';
		$labels = 'label_view_pengguna_kapi';

		$data['list_pengguna_kapi'] = $this->mdl_pengguna_kapi->list_pengguna_kapi();

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function entry()
	{
		$data['submit_form'] = 'pengguna_kapi/main/input';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengguna_kapi';
		$views = 'form_pengguna_kapi';
		$labels = 'form_pengguna_kapi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$arr_info_lokasi = array();

		$arr_info_lokasi = explode("|", $array_input['prop_kab_kota']);

		$array_input['id_propinsi'] = $arr_info_lokasi[0];
		$array_input['id_kabupaten_kota'] = $arr_info_lokasi[1];
		$array_input['kode_propinsi'] = $arr_info_lokasi[2];
		$array_input['kode_kab_kota'] = $arr_info_lokasi[3];
		$array_input['nama_propinsi'] = $arr_info_lokasi[4];
		$array_input['nama_kabupaten_kota'] = $arr_info_lokasi[5];

		unset($array_input['prop_kab_kota']);


		if($array_input['kategori_pengguna'] === "PUSAT" ){
			$array_input['id_propinsi'] = '11';
			$array_input['id_kabupaten_kota'] = '154';
			$array_input['kode_propinsi'] = '31';
			$array_input['kode_kab_kota'] = '73';
			$array_input['nama_propinsi'] = 'DKI Jakarta';
			$array_input['nama_kabupaten_kota'] = 'Jakarta Pusat';
		}

		// vdump($array_input, true);

		if( $this->mdl_pengguna_kapi->input($array_input) ){
			$url = base_url('pengguna_kapi/main');
			redirect($url);
		}else{
			$url = base_url('pengguna_kapi/main');
			redirect($url);
		}
	}

	public function edit($id_record)
	{

		$get_detail = $this->mdl_pengguna_kapi->detil_pengguna_kapi($id_record);

		if( !$get_detail )
		{
			$data['detail_pengguna_kapi'] = 'Data tidak ditemukan';
		}else{
			$data['detail_pengguna_kapi'] = (array)$get_detail;
			$data['detail_pengguna_kapi']['kategori_pengguna'] = $data['detail_pengguna_kapi']['kategori_pengguna'];
		}

		$data['submit_form'] = 'pengguna_kapi/main/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengguna_kapi';
		$views = 'form_pengguna_kapi';
		$labels = 'form_pengguna_kapi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$array_to_edit = $array_input;
		
		$arr_info_lokasi = array();

		$arr_info_lokasi = explode("|", $array_input['prop_kab_kota']);

		$array_to_edit['id_propinsi'] = $arr_info_lokasi[0];
		$array_to_edit['id_kabupaten_kota'] = $arr_info_lokasi[1];
		$array_to_edit['kode_propinsi'] = $arr_info_lokasi[2];
		$array_to_edit['kode_kab_kota'] = $arr_info_lokasi[3];
		$array_to_edit['nama_propinsi'] = $arr_info_lokasi[4];
		$array_to_edit['nama_kabupaten_kota'] = $arr_info_lokasi[5];

		unset($array_to_edit['prop_kab_kota']);

		if($array_to_edit['kategori_pengguna'] === "PUSAT" ){
			$array_to_edit['id_propinsi'] = '11';
			$array_to_edit['id_kabupaten_kota'] = '154';
			$array_to_edit['kode_propinsi'] = '31';
			$array_to_edit['kode_kab_kota'] = '73';
			$array_to_edit['nama_propinsi'] = 'DKI Jakarta';
			$array_to_edit['nama_kabupaten_kota'] = 'Jakarta Pusat';
		}

		// vdump($array_to_edit, true);
		
		if( $this->mdl_pengguna_kapi->update($array_to_edit) ){
			$url = base_url('pengguna_kapi/main');
			redirect($url);
		}else{
			$url = base_url('pengguna_kapi/main');
			redirect($url);
		}
	}

	public function delete($id)
    {
        $this->mdl_pengguna_kapi->delete($id);

        $url = base_url('pengguna_kapi/main');
        redirect($url);
    }

    /*function get_info_pengguna_kapi()
    {
      $id_pengguna = 269;

      $this->load->model('mdl_pengguna_kapi');

      $arr_info_pengguna_kapi = $this->mdl_pengguna_kapi->info_pengguna_kapi($id_pengguna);

      $userdata_pengguna = array();
      foreach ($arr_info_pengguna_kapi as $key => $value) {
        $userdata_pengguna[$key] = $value;
      }

      vdump($userdata_pengguna, true);
    }*/

}
?>
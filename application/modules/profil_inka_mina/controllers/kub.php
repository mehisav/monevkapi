<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once APPPATH."/third_party/PHPWord.php";

class Kub extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			$this->load->config('globals');
			$this->load->model('mdl_kub');
		}

	public function index()
	{
		$this->kub();

	}
	public function entry_kub_dss($filter = '')
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter == '1' ) $data['filter'] = $filter;
		$data['data_perusahaan_dss'] = $this->mdl_kub->data_perusahaan_dss();
		$data['submit_form'] = 'profil_inka_mina/kub/input_dss';
		$template = 'templates/page/v_form';
		$modules = 'profil_inka_mina';
		$views = 'sinkron_kub';
		$labels = 'sinkron_kub';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input_dss()
	{
		$array_input = $this->input->post(NULL, TRUE);
		// vdump($array_input);	

		foreach ($array_input as $key => $value) {
			// echo substr($key, 0,4);
			if(substr($key, 0,3) == "kub")
			{
				// echo "makan";
				$this->mdl_kub->input_dss($value);
			}
		}
// die();
		$url = base_url('profil_inka_mina/kub/entry_kub_dss');
		redirect($url);
	}

	public function list_kub()
	{
		$this->load->model('mdl_kub');

		$list_opsi = $this->mdl_kub->list_opsi_kub();
		// print_r($list_opsi);die;
		return $list_opsi;
	}

	public function kub( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_kub';
		$data['list_kub'] = $this->mdl_kub->daftar_kub();
		
		echo Modules::run('templates/page/v_form', 'profil_inka_mina', 'tabel_kub', $labels, $add_js, $add_css, $data);

	}

	public function kub_report_word($data)
	{

 		$this->word = new PHPWord();
 		//prepare data
 		// $data = $this->data_export;

		$styleTable2 = array('borderSize'=>6, 'borderColor'=>'4a4849', 'cellMargin'=>80);
 		$this->word->addTableStyle('kub', $styleTable2);

		$section = $this->word->createSection();
		$table2 = $section->addTable('kub');
		$i = 1;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("ID KUB");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->id_kub))? $data['detail_kub']->id_kub : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("ID Perusahaan DSS");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->id_perusahaan))? $data['detail_kub']->id_perusahaan : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Nama KUB");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_kub))? $data['detail_kub']->nama_kub : '');
		$nama_kub = $data['detail_kub']->nama_kub;
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("No SIUP");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->no_siup))? $data['detail_kub']->no_siup : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Tahun Pembentukan");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->tahun_pembentukan))? $data['detail_kub']->tahun_pembentukan : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Sumber Anggaran");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->sumber_anggaran))? $data['detail_kub']->sumber_anggaran : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Oleh");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->nomor))? $data['detail_kub']->nomor : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("No Telepon");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->oleh))? $data['detail_kub']->oleh : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Dana Simpanan");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->dana_simpan))? $data['detail_kub']->dana_simpan : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Status Hukum Kelompok Nelayan");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->status_hukum))? $data['detail_kub']->status_hukum : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Nomor Register");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->nomor_register))? $data['detail_kub']->nomor_register : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("ALAMAT");
		$table2->addCell(100)->addText("");
		$table2->addCell(7000)->addText("");
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Kabupaten/Kota");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->id_kabupaten_kota))? $data['detail_kub']->id_kabupaten_kota : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Alamat Lengkap");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->alamat))? $data['detail_kub']->alamat : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("PENGURUS");
		$table2->addCell(100)->addText("");
		$table2->addCell(7000)->addText("");
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Nama Ketua");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_ketua))? $data['detail_kub']->nama_ketua : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Telepon Ketua");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->telp_ketua))? $data['detail_kub']->telp_ketua : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Nama Sekretaris");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_sekretaris))? $data['detail_kub']->nama_sekretaris : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Telepon Sekretaris");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->telp_sekretaris))? $data['detail_kub']->telp_sekretaris : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Nama Bendahara");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_bendahara))? $data['detail_kub']->nama_bendahara : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Telepon Bendahara");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->telp_bendahara))? $data['detail_kub']->telp_bendahara : '');
		$i++;

		$table2->addRow(200);
		$table2->addCell(2000)->addText("Jumlah Anggota");
		$table2->addCell(100)->addText(":");
		$table2->addCell(7000)->addText((isset($data['detail_kub']->anggota))? $data['detail_kub']->anggota : '');
		$i++;		

		$nama_file = 'Profile '.$nama_kub.'.docx';
		$filename = tempnam(base_url().'uploads/temp_export/', 'PHPWord');
		$objWriter = PHPWord_IOFactory::createWriter($this->word, 'Word2007');
		$objWriter->save($filename);
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.$nama_file);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filename));
		flush();
		readfile($filename);
		unlink($filename);
	}

	public function detail_kub( $filter = '' , $export = null)
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'detail_kub';
		$data['detail_kub'] = $this->mdl_kub->detail_kub($filter);
		$data['id_kub'] =$filter ;

		if($export!=null)
		{
			$this->kub_report_word($data);
		}

		echo Modules::run('templates/page/v_form', 'profil_inka_mina', 'detail_kub', $labels, $add_js, $add_css, $data);

	}

	public function entry_kub( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'form_entry_kub';
		$data['submit_form'] = 'profil_inka_mina/kub/input_kub';
		if($filter != '' )
		{
			$id_kub = $filter;
			$data['detail_kub'] = $this->mdl_kub->detail_kub($id_kub);
			$data['submit_form'] = 'profil_inka_mina/kub/input_kub/update';
		}
		
		echo Modules::run('templates/page/v_form', 'profil_inka_mina', 'form_entry_kub', $labels, $add_js, $add_css, $data);

	}

	public function input_kub( $filter = '' )
	{
		$array_input = $this->input->post(NULL, TRUE);
		if($filter === ''){
			if( $this->mdl_kub->insert_kub($array_input) ){
				$url = base_url('profil_inka_mina/kub');
				redirect($url);
			}else{
				$url = base_url('profil_inka_mina/kub');
				redirect($url);
			}
		}else
		{
			if( $this->mdl_kub->update_kub($array_input) ){
				$url = base_url('profil_inka_mina/kub');
				redirect($url);
			}else{
				$url = base_url('profil_inka_mina/kub');
				redirect($url);
			}
		}
	}

	public function delete_kub($id)
    {
        $this->mdl_kub->delete($id);

        $url = base_url('profil_inka_mina/kub');
        redirect($url);
    }

    public function list_id_kub()
	{

		$list_opsi = $this->mdl_kub->list_opsi();

		return $list_opsi;
	}

    public function export_kub()
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		$template = FCPATH.'assets/kapi/format/format_kub.xls';
		// $template = dirname(__FILE__).'/'.$_GET['kode'].'.xlsx';
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSetting = array('memoryCacheSize' => '100MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSetting);

		$objReader = PHPExcel_IOFactory::createReader('Excel5');
		$objPHPExcel = $objReader->load($template);
		$objWorksheet = $objPHPExcel->getSheet(0);

		/*content*/
		$data = $this->mdl_kub->daftar_kub();
		$rowNumber = 7;
		foreach ($data as $item)
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber, ($rowNumber-6)); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowNumber, $item->nama_kub); 
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowNumber, $item->no_siup); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowNumber, $item->tahun_pembentukan); 
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowNumber, $item->sumber_anggaran); 
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowNumber, $item->oleh); 
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowNumber, $item->no_telp); 
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowNumber, $item->nama_propinsi); 
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowNumber, $item->kabupaten); 
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowNumber, $item->alamat); 
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$rowNumber, $item->nama_ketua); 
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$rowNumber, $item->telp_ketua); 
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowNumber, $item->nama_sekretaris); 
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$rowNumber, $item->telp_sekretaris); 
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$rowNumber, $item->nama_bendahara); 
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$rowNumber, $item->telp_bendahara); 
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowNumber, $item->anggota);

			$rowNumber++;
		}
		/*End Content*/

		//Border
		$styleArray = array(
		    'borders' => array(
		      'allborders' => array(
		          'style' => PHPExcel_Style_Border::BORDER_THIN
		      )
		    )
		);
		$objPHPExcel->getActiveSheet()->getStyle('A7:Q'.($rowNumber-1))->applyFromArray($styleArray);
		// Save as an Excel BIFF (xls) file 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename=" Data KUB - '.date("d-m-Y h:i").'.xlsx"'); 
		header('Cache-Control: max-age=0'); 

		$objWriter->save('php://output'); 

	}
}
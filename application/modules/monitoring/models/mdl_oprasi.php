<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_oprasi extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        $this->load->database();
        
    }

    public function data_kapal($where)
    {
        $this->db_monev = $this->load->database();
        $query = 'SELECT    dmm.id_kapal as "id_kapal",
                            dmm.nama_kapal as "nama_kapal",
                            dmm.gt as "gt",
                            dmm.tanda_selar as "tanda_selar",
                            dmm.sipi,
                            dmm.siup,
                            mkk.nama_kabupaten_kota,
                            tp.jumlah_lapor,
                            mp.nama_propinsi
                FROM db_monev_kapi.mst_inka_mina as dmm
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON dmm.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                LEFT JOIN (SELECT  COUNT(id_kapal) as jumlah_lapor, id_kapal
                            FROM  trs_produksi as tp
                            WHERE tp.aktif = "YA" 
                            GROUP BY id_kapal) as tp ON tp.id_kapal = dmm.id_kapal
                WHERE dmm.aktif = "Ya" '.$where;
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_kapal()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM    mst_inka_mina as dmm
                WHERE dmm.aktif = "YA"
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
            
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_kapal_sipi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM    mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" AND sipi <> ""
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function aktif_berhenti()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM    mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" AND (id_permasalahan = 1 
                    OR id_permasalahan = 2 OR id_permasalahan = 3 
                    OR id_permasalahan = 12 OR id_permasalahan = 4 
                    OR id_permasalahan = 0)
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_kapal_tidak_sipi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM    mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" AND sipi = ""
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function kapal_aktif()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" AND id_permasalahan = 0
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function aktif_sipi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND tanggal_sipi > CURDATE()
                    AND (sipi <> "" 
                    OR LOCATE("izin", sipi) = 0
                    OR LOCATE("daerah", sipi) = 0)
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function habis_sipi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                AND tanggal_sipi < CURDATE()
                AND (sipi <> "" 
                    OR LOCATE("izin", sipi) = 0
                    OR LOCATE("daerah", sipi) = 0)
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function izin_dinas()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND( LOCATE("izin", sipi) <> 0
                    OR LOCATE("daerah", sipi) <> 0)
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function tidak_sipi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" AND sipi = ""
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function total_lapor()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_lapor
                FROM  trs_produksi as tp
                WHERE tp.aktif = "YA" 
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_lapor))?$temp->jumlah_lapor:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function sudah_lapor()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) AS jumlah_kapal
                FROM db_monev_kapi.mst_inka_mina as dmm
                LEFT JOIN (SELECT  COUNT(id_kapal) as jumlah_lapor, id_kapal
                            FROM  trs_produksi as tp
                            WHERE tp.aktif = "YA" 
                            GROUP BY id_kapal) as tp ON tp.id_kapal = dmm.id_kapal
                WHERE dmm.aktif = "Ya" AND tp.jumlah_lapor > 0
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function berhenti_oprasional()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND (id_permasalahan = 1 
                        OR id_permasalahan = 2
                        OR id_permasalahan = 3
                        OR id_permasalahan = 4
                        OR id_permasalahan = 12
                    )
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function rusak_mesin()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 1
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function rusak_body()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 2
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kebakaran()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 3
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function cuaca_buruk()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 4
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function merugi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 12
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function belum_oprasi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                LEFT JOIN trs_produksi as tp ON tp.id_kapal = dmm.id_kapal
                WHERE dmm.aktif = "YA" AND tp.id_produksi IS NULL
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function tidak_biaya()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 5
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kurang_body()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 6
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kurang_api()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 7
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kendala_pemasaran()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 8
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kendala_rumpon()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 13
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kendala_bbm()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 10
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }
    public function kendala_vms()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT  COUNT(*) as jumlah_kapal
                FROM  mst_inka_mina as dmm
                WHERE dmm.aktif = "YA" 
                    AND id_permasalahan = 11
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $temp = $run_query->row();
            $result = (isset($temp->jumlah_kapal))?$temp->jumlah_kapal:0;
        }else{
            $result = false;
        }
        return $result;
    }

    public function daftar_kapal($filter = '')
    {
        $this->load->database();
        $a = ($filter == 'sipi_expired' )? 'AND (mim.tanggal_akhir_sipi < CURDATE() AND  mim.sipi <> "")' : 'AND (mim.sipi IS NULL OR mim.sipi = "")';
        $query = 'SELECT mp.nama_propinsi,
                            mkk.nama_kabupaten_kota,
                            mim.nama_kapal as "nama_kapal",
                            mim.tanda_selar,
                            mim.gross_akte,
                            mim.sipi,
                            mim.tanggal_akhir_sipi,
                            mpb.nama_pelabuhan,
                            mat.nama_alat_tangkap,
                            mim.gt,
                            CONCat("P=", mim.panjang_kapal, ", L=", mim.lebar_kapal, ", D=" ,mim.dalam_kapal ),
                            mim.daya,
                            mim.mesin,
                            mim.no_mesin,
                            mim.kontraktor_pembangunan,
                            mim.lokasi_pembangunan,
                            mim.tahun_pembuatan,
                            mbk.nama_bahan_kapal,
                            kmp.masalah,
                            mk.nama_kub,
                            mk.no_siup,
                            mk.tahun_pembentukan,
                            mk.sumber_anggaran,
                            mk.oleh,
                            mk.no_telp,
                            mk.alamat,
                            mk.nama_ketua,
                            mk.telp_ketua,
                            mk.nama_sekretaris,
                            mk.nama_bendahara,
                            mk.anggota,
                            tp.jml_produksi,
                            tp.jml_opreasi,
                            tp.avg_hari_operasi,
                            tp.avg_produksi,
                            tp.avg_bbm,
                            tp.avg_operasional,
                            tp.avg_pendapatan,
                            mim.keterangan
                FROM db_monev_kapi.mst_inka_mina as mim
                LEFT JOIN db_monev_kapi.mst_kub as mk ON mk.id_kub = mim.id_kub
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON mim.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                LEFT JOIN db_monev_kapi.mst_permasalahan as kmp ON kmp.id = mim.id_permasalahan
                LEFT JOIN db_master.mst_alat_tangkap as mat ON mat.id_alat_tangkap = mim.id_alat_tangkap
                LEFT JOIN db_master.mst_bahan_kapal as mbk ON mbk.id_bahan_kapal = mim.id_bahan_kapal
                LEFT JOIN (SELECT id_kapal, SUM(jml_ikan) AS jml_produksi, 
                                    COUNT(*) AS jml_opreasi,  
                                    AVG(datediff(tgl_keluar,tgl_masuk)) AS avg_hari_operasi,
                                    AVG(jml_ikan) AS avg_produksi,
                                    AVG(kebutuhan_bbm) AS avg_bbm,
                                    AVG(biaya_operasional) AS avg_operasional,
                                    AVG(nilai_pendapatan-biaya_operasional) AS avg_pendapatan
                                FROM trs_produksi 
                                GROUP BY id_kapal) tp ON tp.id_kapal = mim.id_kapal
                LEFT JOIN (SELECT mim.id_kapal, 
                                    group_concat(nama_pelabuhan separator ", ") AS nama_pelabuhan
                            FROM db_monev_kapi.mst_inka_mina as mim
                            LEFT JOIN db_master.mst_pelabuhan as mpb 
                                ON find_in_set(mpb.id_pelabuhan, mim.id_pelabuhan_pangkalan)
                            GROUP BY mim.id_kapal
                         ) mpb ON mpb.id_kapal = mim.id_kapal
                WHERE mim.aktif = "YA" '.$filter;
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}
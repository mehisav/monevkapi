<!-- mulai #kapicontainer --> 
<div id="kapicontainer" class="container">
	<div class="row">
		<!-- <div class="col-lg-6">
			<h5><?php //echo date('D d M Y (d/m/Y H:m)',now()); ?></h5>
		</div>
		<div class="col-lg-6">
			<h5 class="text-right" title="<?php //echo $this->mksess->id_pengguna(); ?>">Pengguna: <?php //echo $this->mksess->nama_pengguna(); ?></h5>
		</div> -->
	</div>
	<div id="main_row" class="row">
		<div id="kapicolumn1" class="col-md-12"><!-- buka #kapicolumn1 -->
			<div class="panel panel-default"><!-- buka .panel -->
				<div class="panel-heading"><!-- buka .panel-heading -->
					<h3 class="panel-title" id='title'><?php echo $panel_title; ?></h3>
				</div><!-- tutup .panel-heading -->
				<div class="panel-body">
					<?php echo $this->load->view($nama_module.'/'.$file_form); ?>
				</div><!-- tutup .panel-body -->
			</div> <!-- tutup .panel -->
		</div><!-- tutup #kapicolumn1 -->
	</div> <!-- akhir #main_row -->
</div><!-- akhir #kapicontainer --> 

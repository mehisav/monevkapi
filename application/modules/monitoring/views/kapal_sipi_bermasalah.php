<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='tabel_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	// echo "makannnn".$hasil;
	$counter = 1;
	if($data_kapal){
		foreach ($data_kapal as $item) {

			// $link_edit = '<a class="btn btn-warning" style="width=120" href="'.base_url('profil_inka_mina/kapal/form_entry_kapal/'.$item->id_kapal).'">Edit</a>';
			// $link_delete = '<a class="btn btn-danger" style="width=120" href="'.base_url('profil_inka_mina/kapal/delete_kapal_inka/'.$item->id_kapal).'">Hapus</a>';
			// $link_view = '<a class="btn btn-primary" style="width=120" href="'.base_url('profil_inka_mina/kapal/detail_kapal_inka/'.$item->id_kapal).'">View</a>';

			$this->table->add_row(
								$counter,
								$item->nama_propinsi,
								$item->nama_kabupaten_kota,
								$item->nama_kapal,
								$item->gt,
								$item->tanda_selar,
								$item->tanggal_akhir_sipi,
								$item->sipi
								);
			$counter++;
		}
	}

	$tabel_mapping_kapal = $this->table->generate();
	$time = date("Y-m-d");
?>
<div class="row">
	<div class="col-lg-3" style="margin-bottom:50px">
		<a class="btn btn-info"  href="<?php echo $link_update;?>">Update</a>
		<a class="btn btn-default"  href="<?php echo base_url('monitoring/sipi/export_kapal/'.$info); ?>">Export to Excel</a>
	</br>
		Last Update DSS <?php echo tgl($tanngal_update);?>
	</div>
</div>
<!-- TAMPIL DATA -->
	<style type="text/css">
		table#table_daftar_produksi{
			width: 2200px;
		}
	</style>
	<div style="width:100%;overflow:auto;">
		<?php
			echo $tabel_mapping_kapal;
		?>
	</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#tabel_kapal').dataTable({

			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"}
                      ],
		});
	} );
</script>
<div class="row">
  <div class="col-lg-12">
          <?php
          // echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
          echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
          if(isset($detail_kub->id_kub)){
            $input_hidden  = array('id_kub' => $detail_kub->id_kub );

            echo form_hidden($input_hidden);
          }
          ?>


         <?php        

          $attr_nama_kub = array( 'name' => $form['nama_kub']['name'],
                                        'label' => $form['nama_kub']['label'],
                                        'value' => (isset($detail_kub->nama_kub)? $detail_kub->nama_kub : '')
                    );          
          echo $this->mkform->input_text($attr_nama_kub);

          $attr_no_siup = array( 'name' => $form['no_siup']['name'],
                                        'label' => $form['no_siup']['label'],
                                        'value' => (isset($detail_kub->no_siup)? $detail_kub->no_siup : '')
                    );          
          echo $this->mkform->input_text($attr_no_siup);

          // $tanggal = (isset($detail_kub->tgl_siup)? $detail_kub->tgl_siup : '');
          // $attr_tgl_siup = array(  'mindate' => array('time' => '1 year'),
          //                                 'default_date' => $tanggal,
          //                                 'placeholder' => '', 
          //                                 'name' => $form['tgl_siup']['name'],
          //                               'label' => $form['tgl_siup']['label']
          //           );
          // echo $this->mkform->input_date($attr_tgl_siup);

          $attr_sumber_anggaran = array( 'name' => $form['sumber_anggaran']['name'],
                                         'label' => $form['sumber_anggaran']['label'],
                                         'opsi' => array(
                                                          'TP Provinsi' => 'TP Provinsi', 
                                                          'TP Kab/Kota' => 'TP Kab/Kota',
                                                          'DAK Propinsi' => 'DAK Propinsi',
                                                          'DAK Kabupaten/Kota' => 'DAK Kabupaten/Kota',
                                                          'Dana Pusat' => 'Dana Pusat'
                                                        ),
                                         'value' => (isset($detail_kub->sumber_anggaran)? $detail_kub->sumber_anggaran : 0)
                    );
          echo $this->mkform->input_select($attr_sumber_anggaran);

          $attr_tahun_pembentukan = array( 'name' => $form['tahun_pembentukan']['name'],
                                         'label' => $form['tahun_pembentukan']['label'],
                                         'opsi' => TahunArray(),
                                         'value' => (isset($detail_kub->tahun_pembentukan)? $detail_kub->tahun_pembentukan : 0)
                    );
          echo $this->mkform->input_select($attr_tahun_pembentukan); 

          // $attr_tahun_pengukuhan = array( 'name' => $form['tahun_pengukuhan']['name'],
          //                                'label' => $form['tahun_pengukuhan']['label'],
          //                                'opsi' => TahunArray(),
          //                                'value' => (isset($detail_kub->tahun_pengukuhan)? $detail_kub->tahun_pengukuhan : 0)
          //           );
          // echo $this->mkform->input_select($attr_tahun_pengukuhan);  

          $attr_oleh = array( 'name' => $form['oleh']['name'],
                                        'label' => $form['oleh']['label'],
                                        'value' => (isset($detail_kub->oleh)? $detail_kub->oleh : '')
                    );          
          echo $this->mkform->sub_input_text($attr_oleh);

          $attr_nomor = array( 'name' => $form['nomor']['name'],
                                        'label' => $form['nomor']['label'],
                                        'value' => (isset($detail_kub->nomor)? $detail_kub->nomor : '')
                    );          
          echo $this->mkform->sub_input_text($attr_nomor);

          echo "</br></br>";
          $attr_dana_simpan = array( 'name' => $form['dana_simpan']['name'],
                                        'label' => $form['dana_simpan']['label'],
                                        'value' => (isset($detail_kub->dana_simpan)? $detail_kub->dana_simpan : '')
                    );          
          echo $this->mkform->input_text($attr_dana_simpan);

          $attr_status_hukum = array( 'name' => $form['status_hukum']['name'],
                                        'label' => $form['status_hukum']['label'],
                                        'value' => (isset($detail_kub->status_hukum)? $detail_kub->status_hukum : '')
                    );          
          echo $this->mkform->input_text($attr_status_hukum);

          $attr_nomor_register = array( 'name' => $form['nomor_register']['name'],
                                        'label' => $form['nomor_register']['label'],
                                        'value' => (isset($detail_kub->nomor_register)? $detail_kub->nomor_register : '')
                    );          
          echo $this->mkform->input_text($attr_nomor_register);
          ?>
          <div class="form-group mkform-text">
            </br>
            </br>
            <label class="col-sm-3 control-label">ALAMAT</label>
          </div>           
          <?php
          $attr_id_kabupaten_kota = array( 'name' => $form['id_kabupaten_kota']['name'],
                                  'label' => $form['id_kabupaten_kota']['label'],
                                  'opsi' => Modules::run('refdss/kabupaten_kota/list_kaprop_array'),
                                 'value' => (isset($detail_kub->id_kabupaten_kota)? $detail_kub->id_kabupaten_kota : 0)

                    );
          echo $this->mkform->input_select_level($attr_id_kabupaten_kota);   

          $attr_alamat = array( 'name' => $form['alamat']['name'],
                                        'label' => $form['alamat']['label'],
                                        'value' => (isset($detail_kub->alamat)? $detail_kub->alamat : '')
                    );          
          echo $this->mkform->input_text($attr_alamat);

          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                        'label' => $form['no_telp']['label'],
                                        'value' => (isset($detail_kub->no_telp)? $detail_kub->no_telp : '')
                    );          
          echo $this->mkform->input_text($attr_no_telp);

          ?>
          <div class="form-group mkform-text">
            </br>
            </br>
            <label class="col-sm-3 control-label">PENGURUS</label>
          </div>           
          <?php
          $attr_ketua = array( 'name' => $form['ketua']['name'],
                                        'label' => $form['ketua']['label'],
                                        'value' => (isset($detail_kub->nama_ketua)? $detail_kub->nama_ketua : '')
                    );          
          echo $this->mkform->input_text($attr_ketua);

          $attr_no_telp_ketua = array( 'name' => $form['no_telp_ketua']['name'],
                                        'label' => $form['no_telp_ketua']['label'],
                                        'value' => (isset($detail_kub->telp_ketua)? $detail_kub->telp_ketua : '')
                    );          
          echo $this->mkform->sub_input_text($attr_no_telp_ketua);
          
          // $attr_wakil = array( 'name' => $form['wakil']['name'],
          //                               'label' => $form['wakil']['label'],
          //                               'value' => (isset($detail_kub->nama_wakil)? $detail_kub->nama_wakil : '')
          //           );          
          // echo $this->mkform->input_text($attr_wakil);

          // $attr_no_telp_wakil = array( 'name' => $form['no_telp_wakil']['name'],
          //                               'label' => $form['no_telp_wakil']['label'],
          //                               'value' => (isset($detail_kub->telp_wakil)? $detail_kub->telp_wakil : '')
          //           );          
          // echo $this->mkform->sub_input_text($attr_no_telp_wakil);

          $attr_bendahara = array( 'name' => $form['bendahara']['name'],
                                        'label' => $form['bendahara']['label'],
                                        'value' => (isset($detail_kub->nama_bendahara)? $detail_kub->nama_bendahara : '')
                    );          
          echo $this->mkform->input_text($attr_bendahara);

          $attr_no_telp_bendahara = array( 'name' => $form['no_telp_bendahara']['name'],
                                        'label' => $form['no_telp_bendahara']['label'],
                                        'value' => (isset($detail_kub->telp_bendahara)? $detail_kub->telp_bendahara : '')
                    );          
          echo $this->mkform->sub_input_text($attr_no_telp_bendahara);

          $attr_sekretaris = array( 'name' => $form['sekretaris']['name'],
                                        'label' => $form['sekretaris']['label'],
                                        'value' => (isset($detail_kub->nama_sekretaris)? $detail_kub->nama_sekretaris : '')
                    );          
          echo $this->mkform->input_text($attr_sekretaris);

          $attr_no_telp_sekretaris = array( 'name' => $form['no_telp_sekretaris']['name'],
                                        'label' => $form['no_telp_sekretaris']['label'],
                                        'value' => (isset($detail_kub->telp_sekretaris)? $detail_kub->telp_sekretaris : '')
                    );          
          echo $this->mkform->sub_input_text($attr_no_telp_sekretaris);

          $attr_anggota = array( 'name' => $form['anggota']['name'],
                                        'label' => $form['anggota']['label'],
                                        'value' => (isset($detail_kub->anggota)? $detail_kub->anggota : '')
                    );          
          echo $this->mkform->input_text($attr_anggota);
         ?>
  </div>
</div>   
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>

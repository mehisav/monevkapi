<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_produksi' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_produksi){
		foreach ($list_produksi as $item) {
			$link_edit = '<a class="btn btn-warning" style="font-size:10px" href="'.base_url('profil_inka_mina/produksi/form_entry_produksi/'.$item->id_produksi).'">&nbsp;&nbsp;Edit&nbsp;&nbsp;</a>';
			$link_delete = '<a class="btn btn-danger" style="font-size:10px" href="'.base_url('profil_inka_mina/produksi/delete/'.$item->id_produksi).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								$item->nama_kapal,
								$item->nama_wpp,
								$item->nama_dpi,
								tgl($item->tgl_keluar),
								tgl($item->tgl_masuk),
								$item->nama_pelabuhan_keluar,
								$item->nama_pelabuhan_masuk,
								$item->nama_nahkoda,
								$item->jml_abk,
								$item->id_jenis_ikan,
								$item->jml_ikan,
								"RP ".number_format($item->nilai_pendapatan,2),
								$item->kebutuhan_bbm,
								"RP ".number_format($item->biaya_operasional,2),
								"RP ".number_format((($item->nilai_pendapatan - $item->biaya_operasional) - ($item->kebutuhan_bbm*7400)),2),
								$link_edit.' '.$link_delete
								);
			$counter++;
		}
	}

	$table_list_produksi = $this->table->generate();
?>
<div class="row">
	<div class="col-lg-3" style="margin-bottom:50px">
		<a class="btn btn-info"  href="<?php echo base_url('profil_inka_mina/produksi/ambil_data'); ?>">Update data Produksi</a> 
<!-- 		<a class="btn btn-info"  href="#">Update data Produksi</a> -->
	</br>
		Last Update <?php echo (isset($last_update->tgl_masuk))?$last_update->tgl_masuk:'';?>
	</div>
</div>
<!-- TAMPIL DATA -->
	<style type="text/css">
		table#table_daftar_produksi{
			width: 2200px;
		}
	</style>
	<div style="width:100%;overflow:auto;">
		<?php
			echo $table_list_produksi;
		?>
	</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_produksi').dataTable({

			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-center"}
                      ],
		});
	} );
</script>
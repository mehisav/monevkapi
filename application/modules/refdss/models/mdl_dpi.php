<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_dpi extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }

	public function list_dpi()
    {
        $this->db_dss->select('nama_wpp, nama_dpi, nama_dpi_inggris, mst_dpi.lintang, mst_dpi.bujur');
        $this->db_dss->join('mst_wpp', 'mst_wpp.id_wpp = mst_dpi.id_wpp', 'left');
        $this->db_dss->where('mst_dpi.aktif', 'YA');
    	$run_query = $this->db_dss->get('mst_dpi');                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi()
    {
        $query = "SELECT id_dpi AS id, nama_dpi as text FROM mst_dpi WHERE aktif='Ya'";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
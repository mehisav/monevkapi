<?php

	/*Tabel dss*/
	$template = array( "table_open" => "<table id='table_dss' class='table-hover table-bordered'>");
	$this->table->set_template($template);
	$heading = Array(
					'0' => Array('data' => '<input type="checkbox" name="select-all" id="select-all" />', 'width' => '20'),
					'1' => 'Nama Kapal',
					'2' => 'Nama DPI',
					'3' => 'Nama KUB',
					'4' => 'ID Transmitter',
					'5' => 'Tanda Selar',
					'6' => 'Gross Akte',
					'7' => 'No SIPI',
					'8' => 'Masa Berlaku SIPI',
					'9' => 'Pelabuhan Pangkalan',
					'10' => 'Alat Tangkap',
					'11' => 'Panjang Kapal',
					'12' => 'Lebar Kapal',
					'13' => 'Dalam Kapal',
					'14' => 'Daya Kapal',
					'15' => 'GT',
					'16' => 'Mesin',
					'17' => 'No Mesin',
					'18' => 'Lokasi Pembangunan',
					'19' => 'Bahan Kapal'
					
				);
	$this->table->set_heading($heading);

	if($data_kapal_dss){
		foreach ($data_kapal_dss as $item) {
			$link_delete = '<a class="btn btn-danger" style="width=120" href="#">Hapus</a>';
			$attr_id_kapal = array( 'name' => "kapal".$item->id_kapal,
                                         'value' => (isset($item->id_kapal)? $item->id_kapal : 0)
                    );
			// $temp = false;
			// if(strpos($item->nama_kapal, "INKA") !== false 
			// 	&& strpos($item->nama_kapal, "MINA") !== false
			// 	&& isset($item->kapal_inka)==NULL)
			// {
			// 	$temp = true;
			// }
			$temp = true;
			$this->table->add_row(
								($temp===true) ? $this->mkform->input_checkbox($attr_id_kapal) : "",
								$item->nama_kapal,
								$item->nama_dpi,
								$item->nama_kub,
								$item->id_transmitter,
								$item->tanda_selar,
								$item->nomor_gross_akte,
								$item->no_sipi,
								tgl($item->tgl_sipi).'/'.tgl($item->tgl_akhir_sipi),
								$item->nama_pelabuhan,
								$item->nama_alat_tangkap,
								$item->panjang_kapal,
								$item->lebar_kapal,
								$item->dalam_kapal,
								$item->daya_kapal,
								$item->gt_kapal,
								$item->merek_mesin,
								$item->no_mesin,
								$item->tempat_pembangunan,
								$item->nama_bahan_kapal
								);
		}
	
	}
	$table_kapal = $this->table->generate();
?>
<div style="width:100%;overflow:auto;">
	<?php
		echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
		echo $table_kapal;
	?>
</div>

		<div class="row">
			<div class="col-lg-12"> 
				<div class="form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</form>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {

		$('#table_dss').dataTable({
		    "aoColumns": [
		      { "bSortable": false },
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null,
		      null
		    ] } );

		$('#select-all').click(function(event) {   
		    if(this.checked) {
		        // Iterate each checkbox
		        $(':checkbox').each(function() {
		            this.checked = true;                        
		        });
		    }
		});
	} );
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_peta_sebaran extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->load->database();
    }

    public function get_data()
    {
        $query = "SELECT mp.id_propinsi as id_provinsi,
                        mp.nama_propinsi as provinsi,
                        count(*) as jumlah_kapal
                FROM db_monev_kapi.mst_inka_mina as mim 
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON mkk.id_kabupaten_kota = mim.id_kab_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                WHERE mp.id_propinsi IS NOT NULL AND mim.aktif = 'Ya'
                GROUP BY mp.id_propinsi
        ";
        // $query = 'SELECT id_propinsi as id_provinsi, provinsi, count(*) as jumlah_kapal FROM db_monev_kapi.mst_inka_mina mim, db_master.mst_propinsi mp where provinsi <> "null" AND mim.provinsi = mp.nama_propinsi group by provinsi;';
    	$run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}
<?php
	$item = '$item->';
	$config = Array(
					'label_view_table_index' => Array(
													'title' => Array(
															'page' => 'Tabel Proses Pendaftaran',
															'panel'=> 'Tabel Proses Pendaftaran'

														),
													'form'		=> '',
													'th_table'	=> Array(

																	'0' => Array('data' => 'No Pendoks', 'width' => '30', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'1' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'2' => Array('data' => 'Pemilik Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'3' => Array('data' => 'Pendok', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'4' => Array('data' => 'Entri Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'5' => Array('data' => 'Verifikasi Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'6' => Array('data' => 'Cetak BKP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'7' => Array('data' => 'Terima BKP', 'width' => '100', 'style' => 'padding: 10px 10px;font-style:bold;')

																	)
												),
					'oprasi_kapal' => Array(
													'title' => Array(
															'page' => 'Laporan Kapal Bantuan',
															'panel'=> 'Laporan Kapal Bantuan'

														),
													'form'		=> '',
												),
					'sinkron_kub' => Array(
													'title' => Array(
															'page' => 'Tabel Daftar Data DSS KUB',
															'panel'=> 'Tabel Daftar Data DSS KUB'

														),
													'form'		=> '',
												),
					'detail_kub' => Array(
													'title' => Array(
															'page' => 'Detail KUB',
															'panel'=> 'Detail KUB'

														),
													'form'		=> '',
												),
					'sinkron_kapal' => Array(
													'title' => Array(
															'page' => 'Tabel Daftar Data DSS Kapal Bantuan Belum Terdaftar',
															'panel'=> 'Tabel Daftar Data DSS Kapal Bantuan Belum Terdaftar'

														),
													'form'		=> '',
												),
					'tabel_jenis_ikan' => Array(
											'title' => Array(
															'page' => 'Tabel Jenis Ikan',
															'panel'=> 'Tabel Jenis Ikan'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'No', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Nama Ikan', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Nama Inggris Ikan', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama Latin Ikan', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Nama Daerah Ikan', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																// '5' => Array('data' => 'Foto Ikan', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
															)
											),
					'tabel_wpp' => Array(
											'title' => Array(
															'page' => 'Tabel WPP',
															'panel'=> 'Tabel WPP'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'No', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'No WPP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Nama WPP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama WPP Inggris', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Lintang', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Bujur', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_dpi' => Array(
											'title' => Array(
															'page' => 'Tabel DPI',
															'panel'=> 'Tabel DPI'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'No', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Nama WPP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Nama DPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama DPI Inggris', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Lintang', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Bujur', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_alat_tangkap' => Array(
											'title' => Array(
															'page' => 'Tabel Alat Tangkap',
															'panel'=> 'Tabel Alat Tangkap'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'No', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Kode ', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Nama ', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama Inggris', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Jenis Alat Tangkap', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_kapal_inka' => Array(
											'title' => Array(
															'page' => 'Tabel Daftar Kapal Bantuan',
															'panel'=> 'Tabel Daftar Kapal Bantuan'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'Propinsi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Kabupaten/Kota', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'GT', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Tanda Selar', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'SIUP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'Detail', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'8' => Array('data' => 'Aksi', 'width' => '150', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_produksi' => Array(
											'title' => Array(
															'page' => 'Data Produksi Kapal Bantuan',
															'panel' => 'Data Produksi Kapal Bantuan'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Kapal', 'width' => '150'),
															'2' => Array('data' => 'WPP', 'width' => '200'),
															'3' => Array('data' => 'Daerah Penangkapan', 'width' => '100'),
															'5' => Array('data' => 'Tanggal Berangkat', 'width' => '100'),
															'6' => Array('data' => 'Tanggal Kembali', 'width' => '100'),
															'7' => Array('data' => 'Pelabuhan Keluar', 'width' => '100'),
															'8' => Array('data' => 'Pelabuhan Masuk', 'width' => '100'),
															'9' => 'Nama Nahkoda',
															'10' => 'ABK',
															'11' => Array('data' => 'Jenis Ikan', 'width' => '100'),
															'12' => 'Volume Ikan',
															'13' => 'Jumlah Pendapatan',
															'14' => 'Kebutuhan BBM',
															'15' => 'Biaya Operasional',
															'16' => 'Pendapatan Bersih',
															'17' => Array('data' => 'Aksi', 'width' => '120')
														)
											),
					'tabel_sipi' => Array(
											'title' => Array(
															'page' => 'Data Kapal SIPI Expired',
															'panel' => 'Data Kapal SIPI Expired'
															),
											'form' => '',
											'th_table'	=> Array(
																'0' => Array('data' => 'No.', 'width' => '15', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Propinsi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Kabupaten/Kota', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'GT', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Tanda Selar', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'Tanggal Akhir SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_tanpa_sipi' => Array(
											'title' => Array(
															'page' => 'Data Kapal Belum Memiliki SIPI',
															'panel' => 'Data Kapal Belum Memiliki SIPI'
															),
											'form' => '',
											'th_table'	=> Array(
																'0' => Array('data' => 'NO.', 'width' => '15', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Propinsi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Kabupaten/Kota', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'GT', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Tanda Selar', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'Tanggal SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_siup' => Array(
											'title' => Array(
															'page' => 'Data Kapal Belum Memiliki SIUP',
															'panel' => 'Data Kapal Belum Memiliki SIUP'
															),
											'form' => '',
											'th_table'	=> Array(
																'0' => Array('data' => 'NO.', 'width' => '15', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Propinsi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Kabupaten/Kota', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'GT', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Nama KUB', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'Nama Ketua KUB', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'SIUP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'8' => Array('data' => 'Aksi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'tabel_gross_akte' => Array(
											'title' => Array(
															'page' => 'Data Kapal Belum Memiliki Gross Akte',
															'panel' => 'Data Kapal Belum Memiliki Gross Akte'
															),
											'form' => '',
											'th_table'	=> Array(
																'0' => Array('data' => 'NO.', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Propinsi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Kabupaten/Kota', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'GT', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Tanda Selar', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'Dimensi Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'8' => Array('data' => 'Aksi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					
					'tabel_jumlah_kapal' => Array(
											'title' => Array(
															'page' => 'Monitoring Jumlah Kapal',
															'panel'=> 'Monitoring Jumlah Kapal'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'Propinsi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Kabupaten/Kota', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'GT', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Tanda Selar', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'SIUP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'SIPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'Detail', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'label_view_kapal' => Array(
											'title' => Array(
															'page' => 'Kapal DSS >= 30 GT',
															'panel' => 'Kapal DSS >= 30 GT'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Kapal',
															'2' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'3' => 'Nama Kapal Sebelumnya',
															'4' => 'SIPI',
															'5' => Array('data' => 'Tanggal Sipi', 'width' => '150'),
															'6' => Array('data' => 'Tonase <br> (GT & NT)', 'width' => '150'),
															'7' => Array('data' => 'Ukuran Kapal', 'width' => '100'),
															'8' => 'Jenis Kapal',
															'9' => 'Bahan Kapal',
															'10' => Array('data' => 'No. Mesin<br>Merek Mesin', 'width' => '150'),
															'11' => Array('data' => 'Alat Tangkap', 'width' => '300')
														)
											),
					'label_view_kapal_daerah' => Array(
											'title' => Array(
															'page' => 'Tabel Kapal Daerah di DSS',
															'panel' => 'Tabel Kapal Daerah di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Kapal Daerah',
															'2' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'3' => 'Nama Kapal Sebelumnya',
															'4' => 'SIPI',
															'5' => Array('data' => 'Tanggal Sipi', 'width' => '150'),
															'6' => Array('data' => 'Tonase <br> (GT & NT)', 'width' => '150'),
															'7' => Array('data' => 'Ukuran Kapal', 'width' => '100'),
															'8' => 'Jenis Kapal',
															'9' => 'Bahan Kapal',
															'10' => Array('data' => 'No. Mesin<br>Merek Mesin', 'width' => '150'),
															'11' => Array('data' => 'Alat Tangkap', 'width' => '300')
														)

											),
					'tabel_propinsi' => Array(
											'title' => Array(
															'page' => 'Tabel Daftar Propinsi',
															'panel' => 'Tabel Daftar Propinsi'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Propinsi',
															'2' => Array('data' => 'Jumlah Kabupaten/Kota', 'width' => '40'),
															'3' => Array('data' => 'Jumlah Pelabuhan', 'width' => '40')
														)

											),
					'tabel_kabupaten_kota' => Array(
											'title' => Array(
															'page' => 'Tabel Daftar Kabupaten/Kota',
															'panel' => 'Tabel Daftar Kabupaten/Kota'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Propinsi',
															'2' => 'Nama Kabupaten/Kota',
															'3' => Array('data' => 'Jumlah Pelabuhan', 'width' => '40')
														)

											),
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Entry Kapal Monev',
													'panel' => 'Entry Kapal'
													),
									'form' => Array(
														'nama_kapal' 				=> Array( 	'name' 	=> 'nama_kapal',
																						 		'label'	=> 'Nama Kapal'
																							),
														'provinsi' 					=> Array( 	'name' 	=> 'provinsi',
																	 					  		'label' => 'Provinsi'
	 																						),
														'id_kub' 					=> Array( 	'name' 	=> 'id_kub',
																	 					  		'label' => 'Nama KUB'
	 																						),
														'kab_kota' 					=> Array( 	'name' 	=> 'id_kab_kota',
																	 					  		'label' => 'Kabupaten / Kota'
	 																						),
														'id_dpi' 					=> Array( 	'name' 	=> 'id_dpi',
																	 					  		'label' => 'DPI'
	 																						),
														'id_transmitter_vms' 					=> Array( 	'name' 	=> 'id_transmitter_vms',
																	 					  		'label' => 'ID Transmitter VMS'
	 																						),
														'tahun_pembuatan' 		=> Array( 	'name' 	=> 'tahun_pembuatan',
																						 		'label'	=> 'Tahun Pembuatan'
																							),
														'tanda_selar' 				=> Array( 	'name' 	=> 'tanda_selar',
																						 		'label'	=> 'Tanda Selar'
																							),
														'tahun_pembangunan' 		=> Array( 	'name' 	=> 'tahun_pembangunan',
																						 		'label'	=> 'Tahun Pembangunan'
																							),
														'kub' 						=> Array( 	'name' 	=> 'id_kub',
																						 		'label'	=> 'KUB Penerima'
																							),
														'bahan_kapal' 				=> Array( 	'name' 	=> 'id_bahan_kapal',
																						 		'label'	=> 'Bahan Kapal'
																							),
														'gt' 						=> Array( 	'name' 	=> 'gt',
																						 		'label'	=> 'GT'
																							),
														'panjang_kapal' 			=> Array( 	'name' 	=> 'panjang_kapal',
																						 		'label'	=> 'Panjang Kapal'
																							),
														'lebar_kapal' 				=> Array( 	'name' 	=> 'lebar_kapal',
																						 		'label'	=> 'Lebar Kapal'
																							),
														'dalam_kapal' 				=> Array( 	'name' 	=> 'dalam_kapal',
																						 		'label'	=> 'Dalam Kapal'
																							),
														'no_mesin' 					=> Array( 	'name' 	=> 'no_mesin',
																						 			'label'	=> 'No Mesin Utama Kapal'
																								),
														'mesin' 					=> Array( 	'name' 	=> 'mesin',
																						 			'label'	=> 'Mesin Utama Kapal'
																								),
														'daya' 						=> Array( 	'name' 	=> 'daya',
																						 		'label'	=> 'Daya Mesin Utama Kapal'
																							),
														'id_pelabuhan_pangkalan' 		=> Array( 	'name' 	=> 'id_pelabuhan_pangkalan',
																						 		'label'	=> 'Pelabuhan Pangkalan'
																							),
														'pelabuhan_pangkalan' 		=> Array( 	'name' 	=> 'pelabuhan_pangkalan',
																						 		'label'	=> 'Pelabuhan Pangkalan'
																							),
														'jenis_alat_tangkap' 		=> Array( 	'name' 	=> 'id_alat_tangkap',
																						 		'label'	=> 'Jenis Alat Tangkap'
																							),
														'gross_akte' 				=> Array( 	'name' 	=> 'gross_akte',
																						 		'label'	=> 'Gross Akte'
																							),
														'no_siup' 					=> Array( 	'name' 	=> 'siup',
																						 		'label'	=> 'No. SIUP'
																							),
														'no_sipi' 					=> Array( 	'name' 	=> 'sipi',
																						 		'label'	=> 'No. SIPI'
																							),
														'tanpa_sipi' 					=> Array( 	'name' 	=> 'tanpa_sipi',
																						 		'label'	=> 'Tanpa Sipi'
																							),
														'kontraktor_pembangunan' 	=> Array( 	'name' 	=> 'kontraktor_pembangunan',
																						 		'label'	=> 'Kontraktor Pembangunan'
																							),
														'lokasi_pembangunan' 		=> Array( 	'name' 	=> 'lokasi_pembangunan',
																						 		'label'	=> 'Lokasi Pembangunan'
																							),
														'id_permasalahan' 		=> Array( 	'name' 	=> 'id_permasalahan',
																						 		'label'	=> 'Permasalahan Kapal'
																							),
														'keterangan' 		=> Array( 	'name' 	=> 'keterangan',
																						 		'label'	=> 'Keterangan'
																							),
														'foto_pembuatan' 			=> Array( 	'name' 	=> 'foto_pembuatan',
																						 		'label'	=> 'Foto Proses Pembuatan'
																							),
														'foto_sea_trial' 			=> Array( 	'name' 	=> 'foto_sea_trial',
																						 		'label'	=> 'Foto Sea Trial'
																							),
														'foto_oprasional' 			=> Array( 	'name' 	=> 'foto_oprasional',
																						 		'label'	=> 'Foto Oprasional'
																							),
														'foto_siup1' 			=> Array( 	'name' 	=> 'foto_siup1',
																						 		'label'	=> 'Foto SIPI'
																							),
														'foto_siup2' 			=> Array( 	'name' 	=> 'foto_siup2',
																						 		'label'	=> 'Foto SIUP'
																							),
														'foto_bast1' 			=> Array( 	'name' 	=> 'foto_bast1',
																						 		'label'	=> 'Foto BAST ke KUB 1'
																							),
														'foto_bast2' 			=> Array( 	'name' 	=> 'foto_bast2',
																						 		'label'	=> 'Foto BAST ke KUB 2'
																							)
													)
											),
					'form_entry_kub' => Array(
									'title' => Array( 
													'page' => 'Entry KUB',
													'panel' => 'Entry KUB'
													),
									'form' => Array(
														'nama_kub' 		=> Array( 	'name' 	=> 'nama_kub',
																					'label'	=> 'Nama KUB'
																				),
														'id_kapal' 	=> Array( 	'name' 	=> 'id_kapal',
																					'label'	=> 'Nama Kapal'
																				),
														'nama_kapal' 	=> Array( 	'name' 	=> 'nama_kapal',
																					'label'	=> 'Nama Kapal'
																				),
														'sumber_anggaran' 			=> Array( 	'name' 	=> 'sumber_anggaran',
																						 		'label'	=> 'Sumber Anggaran'
																							),
														'no_siup' 		=> Array( 	'name' 	=> 'no_siup',
																					'label'	=> 'NO SIUP'
																				),
														'tgl_siup' 		=> Array( 	'name' 	=> 'tgl_siup',
																					'label'	=> 'Tanggal SIUP'
																				),
														'tahun_pembentukan' 		=> Array( 	'name' 	=> 'tahun_pembentukan',
																					'label'	=> 'Tahun Pembentukan'
																				),
														'tahun_pengukuhan' 		=> Array( 	'name' 	=> 'tahun_pengukuhan',
																					'label'	=> 'Tahun Pengukuhan'
																				),
														'oleh' 	=> Array( 	'name' 	=> 'oleh',
																	 				'label' => 'Oleh'
	 																			),
														'nomor' 	=> Array( 	'name' 	=> 'nomor',
																	 				'label' => 'Nomor'
	 																			),
														'id_kabupaten_kota' 	=> Array( 	'name' 	=> 'id_kabupaten_kota',
																	 				'label' => 'Kabupaten/Kota'
	 																			),
														'alamat' 		=> Array( 	'name' 	=> 'alamat',
																					'label'	=> 'Alamat Lengkap'
																				),
														'dana_simpan' 		=> Array( 	'name' 	=> 'dana_simpan',
																					'label'	=> 'Dana Simpanan'
																				),
														'status_hukum' 		=> Array( 	'name' 	=> 'status_hukum',
																					'label'	=> 'Status Hukum Kelompok Nelayan'
																				),
														'nomor_register' 		=> Array( 	'name' 	=> 'nomor_register',
																					'label'	=> 'Nomor Register'
																				),
														'no_telp' 	=> Array( 	'name' 	=> 'no_telp',
																	 					'label' => 'No Telp.'
	 																			),
														'ketua' 	=> Array( 	'name' 	=> 'nama_ketua',
																	 					'label' => 'Nama Ketua'
	 																			),
														'no_telp_ketua' 	=> Array( 	'name' 	=> 'telp_ketua',
																	 					'label' => 'No Telp.'
	 																			),
														'wakil' 	=> Array( 	'name' 	=> 'nama_wakil',
																	 					'label' => 'Nama Wakil'
	 																			),
														'no_telp_wakil' 	=> Array( 	'name' 	=> 'telp_wakil',
																	 					'label' => 'No Telp.'
	 																			),
														'bendahara' 	=> Array( 	'name' 	=> 'nama_bendahara',
																	 					'label' => 'Bendahara'
	 																			),
														'no_telp_bendahara' 	=> Array( 	'name' 	=> 'telp_bendahara',
																	 					'label' => 'No Telp.'
	 																			),
														'sekretaris' 	=> Array( 	'name' 	=> 'nama_sekretaris',
																	 					'label' => 'Sekretaris'
	 																			),
														'no_telp_sekretaris' 	=> Array( 	'name' 	=> 'telp_sekretaris',
																	 					'label' => 'No Telp.'
	 																			),
														'anggota' 	=> Array( 	'name' 	=> 'anggota',
																	 					'label' => 'Jumlah Anggota'
	 																			),
														
													)
											),
					'form_produksi' => Array(
									'title' => Array( 
													'page' => 'Entry Produksi',
													'panel' => 'Entry Produksi'
													),
									'form' => Array(
													'id_kapal'					=> Array(	'name' => 'id_kapal',
																							'label' => 'Id Kapal'
																						),
													'nama_kapal' 				=> Array( 	'name' 	=> 'nama_kapal',
																					 		'label'	=> 'Nama Kapal'
																						),
													'id_dpi'					=> Array(	'name' => 'id_dpi',
																							'label' => 'Id DPI'
																						),
													'nama_dpi' 					=> Array( 	'name' 	=> 'nama_dpi',
																 					  		'label' => 'Daerah Penangkapan'
 																						),
													'tgl_keluar' 		=> Array( 	'name' 	=> 'tgl_keluar',
																 					  		'label' => 'Tanggal Keluar Pelabuhan'
 																						),
													'tgl_masuk' 		=> Array( 	'name' 	=> 'tgl_masuk',
																 					  		'label' => 'Tanggal Masuk Pelabuhan'
 																						),
													'id_pelabuhan_keluar'		=> Array(	'name' => 'id_pelabuhan_keluar',
																							'label' => 'Nama Pelabuhan Keluar'
																						),
													'id_pelabuhan_masuk'		=> Array(	'name' => 'id_pelabuhan_masuk',
																							'label' => 'Nama Pelabuhan Masuk'
																						),
													'nama_nahkoda' 				=> Array( 	'name' 	=> 'nama_nahkoda',
																					 		'label'	=> 'Nama Nahkoda'
																						),
													'jumlah_abk' 				=> Array( 	'name' 	=> 'jml_abk',
																					 		'label'	=> 'Jumlah ABK (orang)'
																						),
													'jml_hari_operasi' 		=> Array( 	'name' 	=> 'jml_hari_operasi',
																					 		'label'	=> 'Jumlah Hari Operasi'
																						),
													'jml_ikan' 				=> Array( 	'name' 	=> 'jml_ikan',
																					 		'label'	=> 'Volume (Kg)'
																						),
													'harga_jual' 				=> Array( 	'name' 	=> 'harga_jual',
																					 		'label'	=> 'Harga Jual'
																						),
													'nilai_pendapatan' 			=> Array( 	'name' 	=> 'nilai_pendapatan',
																					 		'label'	=> 'Jumlah Nilai Pendapatan (Rp)'
																						),
													'id_jenis_ikan'				=> Array(	'name' => 'id_jenis_ikan',
																							'label' => 'Id Jenis Ikan'
																						),
													'jenis_ikan' 				=> Array( 	'name' 	=> 'jenis_ikan',
																					 		'label'	=> 'Jenis Ikan Hasil Tangkapan Dominan'
																						),
													'kebutuhan_bbm' 			=> Array( 	'name' 	=> 'kebutuhan_bbm',
																					 		'label'	=> 'Kebutuhan BBM / Trip (L)'
																						),
													'biaya_operasional' 		=> Array( 	'name' 	=> 'biaya_operasional',
																					 		'label'	=> 'Biaya Operasional / Trip (Rp)'
																						)
													)
					
									),
					'tabel_kub' => Array(
											'title' => Array(
															'page' => 'Tabel KUB',
															'panel'=> 'Tabel KUB'
														),
											'form'		=> ''											),
					'report_data_kapal' => Array(
											'title' => Array(
															'page' => 'Grafik Kapal',
															'panel'=> 'Grafik Kapal'
														),
											'form'		=> Array(
																'dasar_kapal' => Array(
																					'name' => 'dasar_kapal',
																					'label' => 'Laporan Berdasarkan Kapal'
																				)
															),
											'th_table'	=> Array(
																'0' => Array('data' => 'No', 'width' => '20'),
																'1' => 'Nama KUB',
																'2' => 'Nama Ketua',
																'3' => Array('data' => 'Jumlah Anggota', 'width' => '40'),
																'4' => Array('data' => 'Kontak', 'width' => '80'),
																'5' => Array('data' => 'Alamat', 'width' => '250'),
																'6' => Array('data' => 'Sumber Anggaran', 'width' => '80'),
																'7' => Array('data' => 'Aksi', 'width' => '40')
																
															)
											),
					'tabel_get_produksi' => Array(
											'title' => Array(
															'page' => 'Ambil Data Produksi',
															'panel'=> 'Ambil Data Produksi'
														),
											'form'		=> '',
											'th_table'	=> Array(
																'0' => Array('data' => 'Nama Kpal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'1' => Array('data' => 'Tgl Berangkat', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'2' => Array('data' => 'DPI', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'3' => Array('data' => 'WPP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'4' => Array('data' => 'Hari Operasi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'5' => Array('data' => 'Biaya Oprasional', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'6' => Array('data' => 'Produksi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'7' => Array('data' => 'Pendapatan Bersih', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																'8' => Array('data' => 'Aksi', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')
																
															)
											),
					'report_data_produksi' => Array(
											'title' => Array(
															'page' => 'Diagram Kolom Kapal',
															'panel'=> 'Diagram Kolom Kapal'
														),
											'form'		=> Array(
																'id_kapal'					=> Array(	'name' => 'id_kapal',
																							'label' => 'Id Kapal'
																						),
																'nama_kapal' => Array(
																					'name' => 'nama_kapal',
																					'label' => 'Dasar Data Produksi'
																				),
																'propinsi' => Array(
																					'name' => 'propinsi',
																					'label' => 'Propinsi'
																				),
																'dasar_tipe' => Array(
																					'name' => 'dasar_tipe',
																					'label' => 'Dasar Tipe'
																				),
																'waktu' => Array(
																					'name' => 'waktu',
																					'label' => 'Dasar Perwaktu'
																				),
																// 'dasar_kapal' => Array(
																// 					'name' => 'dasar_kapal',
																// 					'label' => 'Dasar Data Kapal'
																// 				),
															),
											'th_table'	=> Array(
																'0' => Array('data' => 'No', 'width' => '20'),
																'1' => 'Nama KUB',
																'2' => 'Nama Ketua',
																'3' => Array('data' => 'Jumlah Anggota', 'width' => '40'),
																'4' => Array('data' => 'Kontak', 'width' => '80'),
																'5' => Array('data' => 'Alamat', 'width' => '250'),
																'6' => Array('data' => 'Sumber Anggaran', 'width' => '80'),
																'7' => Array('data' => 'Aksi', 'width' => '40')
																
															)
											),
					'form_pengguna_kapi' => Array(
												'title'	=> Array(
															'page'	=> 'Pengaturan Pejabat',
															'panel'	=> 'Pengaturan Pejabat'
															),
												'form'	=> Array(
															'nama_pengguna'	=> Array(
																				'name'	=> 'nama_pengguna',
																				'label'	=> 'Nama Pengguna'
																				),
															'kategori_pengguna'	=> Array(
																				'name'	=> 'kategori_pengguna',
																				'label'	=> 'Kategori Pengguna'
																				),
															'nama_propinsi' => Array(
																				'name'	=> 'nama_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'nama_kabupaten_kota'=> Array(
																				'name'	=> 'nama_kabupaten_kota',
																				'label'	=> 'Kode Kab/Kota'
																				),
															'kode_propinsi' => Array(
																				'name'	=> 'kode_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'kode_kab_kota'	=> Array(
																				'name'	=> 'kode_kab_kota',
																				'label'	=> 'Kode Kab/Kota'
																				)
															)
												),
					'form_login' => Array(
												'title'	=> Array(
															'page'	=> 'Login',
															'panel'	=> 'Login'
															),
												'form'	=> Array(
															'nama_pengguna'	=> Array(
																				'name'	=> 'nama_pengguna',
																				'label'	=> 'Nama Pengguna'
																				),
															'kategori_pengguna'	=> Array(
																				'name'	=> 'kategori_pengguna',
																				'label'	=> 'Kategori Pengguna'
																				),
															'nama_propinsi' => Array(
																				'name'	=> 'nama_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'nama_kabupaten_kota'=> Array(
																				'name'	=> 'nama_kabupaten_kota',
																				'label'	=> 'Kode Kab/Kota'
																				),
															'kode_propinsi' => Array(
																				'name'	=> 'kode_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'kode_kab_kota'	=> Array(
																				'name'	=> 'kode_kab_kota',
																				'label'	=> 'Kode Kab/Kota'
																				)
															)
												)
					
			);

/* PETUNJUK PEMAKAIAN :

---Masih di controller---
Load dulu file config labels.php:
$this->load->config('labels');

Simpan ke data constant:
$data['constant'] = $this->config->item('form_pendok');
---Selesai di controller---

---Penggunaan Di file views---
// Menampilkan text untuk page title (biasanya di gunakan di templates header untuk judul page)
echo $constant['title']['page']; 

// Menampilkan text untuk judul panel
echo $constant['title']['panel'];

// Saat set attribut untuk form input, misal field Nomor Surat Permohonan
<label for="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"> 
<?php echo $constant['form']['no_surat_permohonan']['label'];?>
</label>
<input 
type="text" 
id="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
name="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
value=""/>

---Selesai di file views---
*/



/* TEMPLATE:
	// PERHATIKAN TANDA KOMA SEBELUM DAN SESUDAH ARRAY
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Halaman Entri Kapal',
													'panel' => 'Entri Kapal'
													),
									'form' => Array(
													'nama_kapal' => Array( 'name' => 'nama_kapal',
																					'label'	=> 'Nama Kapal'
																				),
													'' => Array( 'name' => '',
																 'label' => ''
 																)
													)
									)
*/
?>
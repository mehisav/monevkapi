<div class="row">
  <div class="col-lg-12">
          <?php

          // echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
            echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
            if(isset($detail_inka->id_kapal)){
              $input_hidden  = array('id_kapal' => $detail_inka->id_kapal );

              echo form_hidden($input_hidden);
            }
          ?>


         <?php 

          $attr_nama_kapal = array( 'name' => $form['nama_kapal']['name'],
                                        'label' => $form['nama_kapal']['label'],
                                        'value' => (isset($detail_inka->nama_kapal)? $detail_inka->nama_kapal : '')
                    );
          echo $this->mkform->input_text($attr_nama_kapal);  
          $attr_kab_kota = array( 'name' => $form['kab_kota']['name'],
                                  'label' => $form['kab_kota']['label'],
                                  'opsi' => Modules::run('refdss/kabupaten_kota/list_kaprop_array'),
                                 'value' => (isset($detail_inka->kab_kota)? $detail_inka->kab_kota : 0)

                    );
          echo $this->mkform->input_select_level($attr_kab_kota);

          $attr_id_dpi = array( 'name' => $form['id_dpi']['name'].'[]',
                                  'label' => $form['id_dpi']['label'],
                                  'opsi' => Modules::run('refdss/dpi/list_dpi_array'),
                                  'is_multipel' => TRUE,
                                  'value_m' => (isset($detail_inka->id_dpi)? $detail_inka->id_dpi : '')
                    );
          echo $this->mkform->input_select2($attr_id_dpi);        

          $attr_tahun_pembuatan = array( 'name' => $form['tahun_pembuatan']['name'],
                                         'label' => $form['tahun_pembuatan']['label'],
                                         'opsi' => TahunArray(),
                                         'value' => (isset($detail_kub->tahun_pembuatan)? $detail_kub->tahun_pembuatan : date('Y'))
                    );
          echo $this->mkform->input_select($attr_tahun_pembuatan); 

          $attr_id_transmitter_vms = array( 'name' => $form['id_transmitter_vms']['name'],
                                        'label' => $form['id_transmitter_vms']['label'],
                                        'value' => (isset($detail_inka->id_transmitter_vms)? $detail_inka->id_transmitter_vms : '')
                    );
          echo $this->mkform->input_text($attr_id_transmitter_vms);   


          $attr_tanda_selar = array( 'name' => $form['tanda_selar']['name'],
                                        'label' => $form['tanda_selar']['label'],
                                        'value' => (isset($detail_inka->tanda_selar)? $detail_inka->tanda_selar : '')
                    );          
          echo $this->mkform->input_text($attr_tanda_selar);  

          $attr_id_kub = array('name' => $form['id_kub']['name'],
                                    'label' => $form['id_kub']['label'],
                                    'opsi' => Modules::run('profil_inka_mina/kub/list_id_kub'),
                                    'value' => (isset($detail_inka->id_kub)? $detail_inka->id_kub : '')
                    );
          echo $this->mkform->input_select2($attr_id_kub);

          $attr_bahan_kapal = array('name' => $form['bahan_kapal']['name'],
                                    'label' => $form['bahan_kapal']['label'],
                                    'opsi' => Modules::run('refdss/bahan_kapal/list_bahan_kapal'),
                                    'value' => (isset($detail_inka->bahan_kapal)? $detail_inka->bahan_kapal : '')
                    );
          echo $this->mkform->input_select2($attr_bahan_kapal);

          $attr_gt = array( 'name' => $form['gt']['name'],
                                        'label' => $form['gt']['label'],
                                        'value' => (isset($detail_inka->gt)? $detail_inka->gt : '')
                    );
          echo $this->mkform->input_text($attr_gt);

          $attr_panjang_kapal = array( 'name' => $form['panjang_kapal']['name'],
                                        'label' => $form['panjang_kapal']['label'],
                                        'value' => (isset($detail_inka->panjang_kapal)? $detail_inka->panjang_kapal : '')
                    );
          echo $this->mkform->input_text($attr_panjang_kapal);

          $attr_lebar_kapal = array( 'name' => $form['lebar_kapal']['name'],
                                        'label' => $form['lebar_kapal']['label'],
                                        'value' => (isset($detail_inka->lebar_kapal)? $detail_inka->lebar_kapal : '')
                    );
          echo $this->mkform->input_text($attr_lebar_kapal);

          $attr_dalam_kapal = array( 'name' => $form['dalam_kapal']['name'],
                                        'label' => $form['dalam_kapal']['label'],
                                        'value' => (isset($detail_inka->dalam_kapal)? $detail_inka->dalam_kapal : '')
                    );
          echo $this->mkform->input_text($attr_dalam_kapal);

          $attr_no_mesin = array( 'name' => $form['no_mesin']['name'],
                                        'label' => $form['no_mesin']['label'],
                                        'value' => (isset($detail_inka->no_mesin)? $detail_inka->no_mesin : '')
                    );
          echo $this->mkform->input_text($attr_no_mesin);

          $attr_mesin = array( 'name' => $form['mesin']['name'],
                                        'label' => $form['mesin']['label'],
                                        'value' => (isset($detail_inka->mesin)? $detail_inka->mesin : '')
                    );
          echo $this->mkform->input_text($attr_mesin);
          
          $attr_daya = array( 'name' => $form['daya']['name'],
                                        'label' => $form['daya']['label'],
                                        'value' => (isset($detail_inka->daya)? $detail_inka->daya : '')
                    );
          echo $this->mkform->input_text($attr_daya);
          
          $attr_pelabuhan_pangkalan = array( 'name' => $form['id_pelabuhan_pangkalan']['name'].'[]',
                                  'label' => $form['pelabuhan_pangkalan']['label'],
                                  'opsi' => Modules::run('refdss/pelabuhan/list_pelabuhan_array'),
                                  'is_multipel' => TRUE,
                                  'value_m' => (isset($detail_inka->id_pelabuhan_pangkalan)? $detail_inka->id_pelabuhan_pangkalan : '')
                    );
          echo $this->mkform->input_select2($attr_pelabuhan_pangkalan);
          
          $attr_jenis_alat_tangkap = array( 'name' => $form['jenis_alat_tangkap']['name'],
                                            'label' => $form['jenis_alat_tangkap']['label'],
                                            'opsi' => Modules::run('refdss/alat_tangkap/list_alat_tangkap'),
                                            'value' => (isset($detail_inka->jenis_alat_tangkap)? $detail_inka->jenis_alat_tangkap : 0)
                    );
          echo $this->mkform->input_select2($attr_jenis_alat_tangkap);
          
          $attr_gross_akte = array( 'name' => $form['gross_akte']['name'],
                                        'label' => $form['gross_akte']['label'],
                                        'value' => (isset($detail_inka->gross_akte)? $detail_inka->gross_akte : '')
                    );
          echo $this->mkform->input_text($attr_gross_akte);
          
          // $attr_no_siup = array( 'name' => $form['no_siup']['name'],
          //                               'label' => $form['no_siup']['label'],
          //                               'value' => (isset($detail_inka->siup)? $detail_inka->siup : '')
          //           );
          // echo $this->mkform->input_text($attr_no_siup);
          
          $attr_no_sipi = array( 'name' => $form['no_sipi']['name'],
                                        'label' => $form['no_sipi']['label'],
                                        'value' => (isset($detail_inka->sipi)? $detail_inka->sipi : '')
                    );
          echo $this->mkform->input_text($attr_no_sipi);

          $attr_tanpa_izin = array( 'name' => $form['tanpa_sipi']['name'],
                                         'label' => $form['tanpa_sipi']['label'],
                                         'opsi' => array(
                                                          '' => '-', 
                                                          'Izin Daerah' => 'Izin Daerah',
                                                          'Izin Sementara' => 'Izin Sementara'
                                                        ),
                                         'value' => (isset($detail_kub->tanpa_izin)? $detail_kub->tanpa_izin : 0)
                    );
          echo $this->mkform->input_select($attr_tanpa_izin);
          
          $attr_kontraktor_pembangunan = array( 'name' => $form['kontraktor_pembangunan']['name'],
                                        'label' => $form['kontraktor_pembangunan']['label'],
                                        'value' => (isset($detail_inka->kontraktor_pembangunan)? $detail_inka->kontraktor_pembangunan : '')
                    );
          echo $this->mkform->input_text($attr_kontraktor_pembangunan);

          $attr_lokasi_pembangunan = array( 'name' => $form['lokasi_pembangunan']['name'],
                                        'label' => $form['lokasi_pembangunan']['label'],
                                        'value' => (isset($detail_inka->lokasi_pembangunan)? $detail_inka->lokasi_pembangunan : '')
                    );
          echo $this->mkform->input_text($attr_lokasi_pembangunan);

          $attr_foto_pembuatan = array( 'name' => $form['foto_pembuatan']['name'],
                                        'label' => $form['foto_pembuatan']['label']
                    );
          echo $this->mkform->input_file($attr_foto_pembuatan);

          $attr_foto_sea_trial = array( 'name' => $form['foto_sea_trial']['name'],
                                        'label' => $form['foto_sea_trial']['label']
                    );
          echo $this->mkform->input_file($attr_foto_sea_trial);

          $attr_foto_oprasional = array( 'name' => $form['foto_oprasional']['name'],
                                        'label' => $form['foto_oprasional']['label']
                    );
          echo $this->mkform->input_file($attr_foto_oprasional);

          $attr_foto_siup1 = array( 'name' => $form['foto_siup1']['name'],
                                        'label' => $form['foto_siup1']['label']
                    );
          echo $this->mkform->input_file($attr_foto_siup1);

          $attr_foto_siup2 = array( 'name' => $form['foto_siup2']['name'],
                                        'label' => $form['foto_siup2']['label']
                    );
          echo $this->mkform->input_file($attr_foto_siup2);

          $attr_foto_bast1 = array( 'name' => $form['foto_bast1']['name'],
                                        'label' => $form['foto_bast1']['label']
                    );
          echo $this->mkform->input_file($attr_foto_bast1);

          $attr_foto_bast2 = array( 'name' => $form['foto_bast2']['name'],
                                        'label' => $form['foto_bast2']['label']
                    );
          echo $this->mkform->input_file($attr_foto_bast2);

          $attr_id_permasalahan = array( 'name' => $form['id_permasalahan']['name'],
                                            'label' => $form['id_permasalahan']['label'],
                                            'opsi' => Modules::run('profil_inka_mina/kapal/list_opsi_permasalahan'),
                                            'value' => (isset($detail_inka->id_permasalahan)? $detail_inka->id_permasalahan : 0)
                    );
          echo $this->mkform->input_select2($attr_id_permasalahan);

          $attr_keterangan = array( 'name' => $form['keterangan']['name'],
                                        'label' => $form['keterangan']['label'],
                                        'value' => (isset($detail_inka->keterangan)? $detail_inka->keterangan : '')
                    );
          echo $this->mkform->input_text($attr_keterangan);
         ?>
  </div>
</div>   
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>


<script>
    var set_validation = function() 
    {
      $("#id_no_surat_permohonan").addClass('validate[required]');

      $('#form_entry').validationEngine();
    }//end set_validation

    var typingTimer;                //timer identifier
    var doneTypingInterval = 1500;
    var search_kapal_listener = function () {
   
      function doneTyping(query){
        $("#id_nama_kapal").popover('destroy');
        if(query !== '')
        {
          $("#id_nama_kapal").popover({ content: 'Cari Kapal : <button type="button" class="btn btn-info btn-cari-kapal">'+query+'</button>', html: true, placement: 'top'});
          $("#id_nama_kapal").popover('show');
        }
      }

      $("#id_nama_kapal").keydown(function(){
        clearTimeout(typingTimer);
      });

      $("#id_nama_kapal").keyup(function(){
        var query = $(this).val();
          typingTimer = setTimeout(function(){ doneTyping(query); }, doneTypingInterval);
      });

      function update_result(data)
      {
        // console.log(data);
        $("#modal-search-kapal .modal-title").html('');
        $("#modal-search-kapal .modal-title").text('Pencarian Kapal "'+data.search_like+'" ('+data.filter+'). Ditemukan '+data.jumlah_result+' kapal.');
        $("#modal-search-kapal tbody").html(''); // Kosongin
          if(data.jumlah_result > 0)
          {
            data.result.forEach(function(d, i){
              $("#modal-search-kapal tbody").append('<tr><td>'+(i+1)+'.</td> <td>'+d.nama_kapal+'</td><td>'+d.no_sipi+'</td><td>'+d.tanggal_sipi+' s/d '+d.tanggal_akhir_sipi+'</td>  </tr>');
            });
          }

        $("#modal-search-kapal").modal('show');
      }

      $(".form-group").on("click", ".btn-cari-kapal",function(){
          var query = $(this).text();
          // $("#modal-search-kapal .modal-title").text("Pencarin Kapal : "+query);
              $.ajax({
                dataType: "json",
                url: link_search_kapal, //
                data: { i: 'pusat', q: query}, // 
                success: update_result // 
              });
          
      });
    }//end search_kapal_listener

  s_func.push(set_validation);
  s_func.push(search_kapal_listener);
</script>
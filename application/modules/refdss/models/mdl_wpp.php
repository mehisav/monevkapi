<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_wpp extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }

	public function list_wpp()
    {
        $this->db_dss->where('aktif', 'YA');
    	$run_query = $this->db_dss->get('mst_wpp');                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function list_opsi()
    {
        $query = "SELECT id_wpp AS id, nama_wpp as text FROM mst_wpp";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}
<!-- mulai #kapinav -->
<div id="kapinav">

	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?=base_url()?>" title="Kapal Perikanan dan Alat Penangkapan Ikan">KAPI</a>
			</div>
			<div class="navbar-collapse">
				<ul class="nav navbar-nav">
					<?php
						foreach ($menus as $index => $values):
							if (is_array($values)) {
							?>
								<li class="menu-item dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<?php echo $index; ?>
										<b class="caret"></b>
									</a>
									<ul class="dropdown-menu" style="font-size:15px">
										<?php
										foreach ($values as $index_2 => $values_2) :
											if (is_array($values_2)) {
												?>
												<li class="menu-item dropdown dropdown-submenu" style="padding: 0px 0px 0px 0px;" >
													<a href="#"><?php echo $index_2; ?></a>
													<ul class="dropdown-menu">
														<?php
														foreach ($values_2 as $index_3 => $values_3):
															?>
															<li class="menu-item" style="padding: 0px 0px 0px 0px;" >
																<a href="<?php echo base_url().$values_3 ?>"><?php echo $index_3; ?></a>
															</li>
															<?php
														endforeach
														?>
													</ul>
												</li>
												<?php
											}
											else {
												?>
												<li class="menu-item" style="padding: 0px 0px 0px 0px;" >
													<a href="<?php echo base_url().$values_2 ?>"><?php echo $index_2; ?></a>
												</li>
												<?php
											}
										endforeach
										?>
									</ul>
								</li>
							<?php	
							}
							else {
							?>
								<li class="menu-item">
									<a href="<?php echo base_url().$link ?>"><?php echo $index; ?></a>
								</li>
							<?php
							}
						endforeach
					?>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>
<!-- akhir #kapinav -->

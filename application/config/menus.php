<?php
	$config = Array(
					'lists' => Array(
		        					'Master Data' 	=> Array( 		'Master Kapal DSS' =>  Array(	'Kapal >= 30 GT' => 'refdss/kapal_dss/',
		        																							'Kapal < 30 GT' => 'refdss/kapal_dss/kapal/daerah'
		        																							),
		        													'Master SDI' 	=> Array(	
		        																							'Jenis Ikan' => 'refdss/jenis_ikan',
		        																							'Alat Tangkap' => 'refdss/alat_tangkap',
		        																							),
		        																							
		        													'Master Data Umum' 			=> Array(	
		        																							
		        																							'WPP' => 'refdss/wpp',
		        																							'DPI' => 'refdss/dpi',
		        																							'Kabupaten/Kota' => 'refdss/kabupaten_kota',
		        																							'Propinsi' => 'refdss/propinsi'
		        																							)
		        													
		        									 		),
		        					'Profil Kapal Bantuan' 		=> Array(	'Profil KUB' 		=> Array(	'Entry KUB' => 'profil_inka_mina/kub/entry_kub',
        																							'Daftar KUB' => 'profil_inka_mina/kub',
        																							'Sinkron KUB' => 'profil_inka_mina/kub/entry_kub_dss'
        																					),
		        													'Kapal Bantuan' 		=> Array(	'Sinkron Kapal Bantuan' => 'profil_inka_mina/kapal',
        																							'Entry Form Kapal Bantuan' => 'profil_inka_mina/kapal/form_entry_kapal'
        																					),
		        													'Oprasi dan Produksi Kapal'	=> Array('Entry Form Produksi' => 'profil_inka_mina/produksi/form_entry_produksi',
        																							'Produksi' => 'profil_inka_mina/produksi'
        																					),
		        												),
		        					'Reporting'			=> Array(	'Grafik Data Kapal'		=> 'reporting/report_kapal',
		        													'Grafik Produksi Kapal' 	=> 'reporting/report_produksi',
		        													'Data Kapal Bantuan' => 'profil_inka_mina/kapal/tabel_kapal_inka',
		        													'Operasi Kapal'		=> 'reporting/report_oprasi_kapal',
		        													'Peta Sebaran'		=> 'reporting/report_peta_sebaran'
		        											),
		        					'Monitoring'		=> Array(	'Kapal SIPI Expired'	 =>'monitoring/sipi',
		        													'Kapal Tanpa SIPI'	 =>'monitoring/sipi/tanpa_sipi',
		        													'Kapal Tanpa SIUP'	 =>'monitoring/siup',
		        													'Kapal Belum Memiliki Gross Akte'	 =>'monitoring/gross_akte',
		        													"Oprasional Kapal" 	=> 'monitoring/oprasi'
		        											),
		        					'Admin'				=> Array(	'Admin Pusat' 		=> '#',
		        													'Admin Propinsi' 	=> '#',
		        													'Admin Daerah' 		=> '#'
		        											)
		        					)
					);
?>
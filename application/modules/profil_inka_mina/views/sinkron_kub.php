<?php

	/*Tabel dss*/
	$template = array( "table_open" => "<table id='table_dss' class='table-hover table-bordered'>");
	$this->table->set_template($template);
	$heading = Array(
					'0' => Array('data' => '<input type="checkbox" name="select-all" id="select-all" />', 'width' => '20'),
					'1' => 'Nama Perusahaan',
					'2' => 'No SIUP',
					'3' => 'Nama Ketua',
					'4' => 'Alamat'
					
				);
	$this->table->set_heading($heading);

	if($data_perusahaan_dss){
		foreach ($data_perusahaan_dss as $item) {
			$link_delete = '<a class="btn btn-danger" style="width=120" href="#">Hapus</a>';
			$attr_id_perusahaan = array( 'name' => "kub".$item->id_perusahaan,
                                         'value' => (isset($item->id_perusahaan)? $item->id_perusahaan : 0)
                    );
			$temp = true;
			$this->table->add_row(
								($temp===true) ? $this->mkform->input_checkbox($attr_id_perusahaan) : "",
								$item->nama_perusahaan,
								$item->no_siup,
								$item->nama_penanggung_jawab,
								$item->alamat_perusahaan
								);
		}
	
	}
	$table_kapal = $this->table->generate();
?>
<?php
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	echo $table_kapal;
?>
		<div class="row">
			<div class="col-lg-12"> 
				<div class="form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</form>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {

		$('#table_dss').dataTable({
		    "aoColumns": [
		      { "bSortable": false },
		      null,
		      null,
		      null,
		      null
		    ] } );

		$('#select-all').click(function(event) {   
		    if(this.checked) {
		        // Iterate each checkbox
		        $(':checkbox').each(function() {
		            this.checked = true;                        
		        });
		    }
		});
	} );
</script>
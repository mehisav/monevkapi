<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bahan_kapal extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_bahan_kapal()
	{
		$this->load->model('mdl_bahan_kapal');

		$list_opsi = $this->mdl_bahan_kapal->list_opsi();

		return $list_opsi;
	}
	
}
?>
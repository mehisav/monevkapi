<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="<?=base_url('assets')?>/third_party/js/markerwithlabel.js"></script>
<script src="<?=base_url('assets')?>/third_party/js/map.js"></script>
<script type="text/javascript">
  
  var base_url = "<?=base_url('report_peta_sebaran')?>";
  c_link("report_peta_sebaran/get_data");
  google.maps.event.addDomListener(window, "load", initialize);
</script>

<style type="text/css">

      @media screen and (min-width: 992px) {
        .the-icons li {
          width: 12.5%;
        }
      }

      #peta {
        height: 450px;
        width: 100%;
        /*padding: 0px;*/
      }
      .well-peta {
        padding: 0px !important;
      }
        
      .labels {
          background-color: white;
          border-style: solid;
          border-width: 1px;
      }

</style>


<div id="map_canvas" style="width:100%; height:550px; border: 2px solid #3872ac;"></div>

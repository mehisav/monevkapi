<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Kapal extends MX_Controller {

	private $data_export;

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_kapal');
		}

	public function index()
	{
		$this->entry_kapal();

	}

	public function list_inka_mina_array()
	{
		// $this->load->model('mdl_inka_mina');

		$list_opsi = $this->mdl_kapal->list_opsi();
		// vdump($list_opsi);die();
		return $list_opsi;
	}

	public function entry_kapal($filter = '')
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter == '1' ) $data['filter'] = $filter;
		$data['data_kapal_dss'] = $this->mdl_kapal->data_kapal_dss();
		$data['submit_form'] = 'profil_inka_mina/kapal/input_dss';
		$template = 'templates/page/v_form';
		$modules = 'profil_inka_mina';
		$views = 'sinkron_kapal';
		$labels = 'sinkron_kapal';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input_dss()
	{
		$array_input = $this->input->post(NULL, TRUE);
		// vdump($array_input);	

		foreach ($array_input as $key => $value) {
			// echo substr($key, 0,4);
			if(substr($key, 0,5) == "kapal")
			{
				// echo "makan";
				$this->mdl_kapal->input_dss($value);
			}
		}
// die();
		$url = base_url('profil_inka_mina/kapal/entry_kapal');
		redirect($url);
	}

	public function form_entry_kapal($filter = '')
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['submit_form'] = 'profil_inka_mina/kapal/input_kapal';
		if($filter != '' )
		{
			$id_kapal = $filter;
			$data['detail_inka'] = $this->mdl_kapal->detail_kapal_inka($id_kapal);
			$data['submit_form'] = 'profil_inka_mina/kapal/input_kapal/update';
		}
		$data['data_kapal_dss'] = $this->mdl_kapal->data_kapal_dss();
		$template = 'templates/page/v_form';
		$modules = 'profil_inka_mina';
		$views = 'form_entry_kapal_dss';
		$labels = 'form_kapal';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input_kapal($filter = '')
	{
		$array_input = $this->input->post(NULL, TRUE);
		$array_input['id_pelabuhan_pangkalan'] = implode(',' ,  $array_input['id_pelabuhan_pangkalan']);
		$array_input['id_dpi'] = implode(',' ,  $array_input['id_dpi']);
		/* Awal Program Upload */
		// vdump($array_input);
		// die();
		// Ambil nama kapal untuk bikin nama file gambar yang diupload
		$nama_kapal = $array_input['nama_kapal'];
		$nama_kapal = preg_replace("/[^A-Za-z0-9 ]/", '', $nama_kapal);
		$nama_kapal = str_replace(" ", "_", $nama_kapal);

		// Timestamp supaya nama file unique
		$getdate = new DateTime();
		$time_upload = $getdate->getTimestamp();
		// Ambil path ke folder uploads yang diset di custom constants
		$assets_paths = $this->config->item('assets_paths');

		// Rules upload nya di set di custom constants
		// $rules = $this->config->item('upload_rules');

		// Persiapkan config untuk upload
		$config['upload_path'] = 'uploads';  // Set path tempat file disimpan di server

		// Set rules ini dari custom constants
		$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
		$config['max_size']	= '2048'; 
		$config['max_width']  = '2048';
		$config['max_height']  = '1536';
		$this->load->library('upload', $config);

		//proses upload foto proses pembuatan
		if( $_FILES['foto_pembuatan'] !== UPLOAD_ERR_NO_FILE) {
			$foto_pembuatan = 'pembuatan_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_pembuatan; //  Set nama file yang bakal disimpan
			// Set config 
			// vdump($config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto_pembuatan') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_pembuatan'] = $foto_pembuatan;
			}
			$this->load->library('upload', $config, FALSE);
		} 
		else {
			// $array_input['foto_pembuatan'] = '';
			unset($array_input['foto_pembuatan']);
		}

		//proses upload foto proses sea trial
		if( $_FILES['foto_sea_trial'] !== UPLOAD_ERR_NO_FILE) {
			$foto_sea_trial = 'sea_trial_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
			
			$config['file_name'] = $foto_sea_trial; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('foto_sea_trial') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_sea_trial'] = $foto_sea_trial;
			}
		} 
		else {
			// $array_input['foto_sea_trial'] = '';
			unset($array_input['foto_sea_trial']);
		}

		//proses upload foto orpasional
		if( $_FILES['foto_oprasional'] !== UPLOAD_ERR_NO_FILE) {
			$foto_oprasional = 'oprasional_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_oprasional; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_oprasional') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_oprasional'] = $foto_oprasional;
			}
		} 
		else {
			// $array_input['foto_oprasional'] = '';
			unset($array_input['foto_oprasional']);
		}

		//proses upload foto siup1
		if( $_FILES['foto_siup1'] !== UPLOAD_ERR_NO_FILE) {
			$foto_siup1 = 'siup1_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_siup1; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_siup1') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_siup1'] = $foto_siup1;
			}
		} 
		else {
			// $array_input['foto_siup1'] = '';
			unset($array_input['foto_siup1']);
		}

		//proses upload foto siup2
		if( $_FILES['foto_siup2'] !== UPLOAD_ERR_NO_FILE) {
			$foto_siup2 = 'siup2_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_siup2; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_siup2') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_siup2'] = $foto_siup2;
			}
		} 
		else {
			// $array_input['foto_siup2'] = '';
			unset($array_input['foto_siup2']);
		}
		//proses upload foto bast1
		if( $_FILES['foto_bast1'] !== UPLOAD_ERR_NO_FILE) {
			$foto_bast1 = 'bast1_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_bast1; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_bast1') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_bast1'] = $foto_bast1;
			}
		} 
		else {
			// $array_input['foto_bast1'] = '';
			unset($array_input['foto_bast1']);
		}

		//proses upload foto bast2
		if( $_FILES['foto_bast2'] !== UPLOAD_ERR_NO_FILE) {
			$foto_bast2 = 'bast2_'.$nama_kapal.'_'.$time_upload.'.jpg';
			// Pasang input gambar untuk diinsert ke database dari nama file yang udah disiapkan sebelumnya
				
			$config['file_name'] = $foto_bast2; //  Set nama file yang bakal disimpan
			// Set config 
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('foto_bast2') ) // Kondisi kalau upload gagal;
			{
				$info_error_upload = array('error' => $this->upload->display_errors());
				// var_dump($info_error_upload);
				//$this->index();
			} 
			else // Kondisi upload berhasil
			{
				//$data = array('upload_data' => $this->upload->data()); /* Akhir Program Upload */

				$array_input['foto_bast2'] = $foto_bast2;
			}
		} 
		else {
			// $array_input['foto_bast2'] = '';
			unset($array_input['foto_bast2']);
		}

		if($array_input['sipi'] == '')
		{
			$array_input['sipi'] = $array_input['tanpa_sipi'];
			unset($array_input['tanpa_sipi']);
		}else
		{
			unset($array_input['tanpa_sipi']);
		}
		// echo $filter;
		if($filter === ''){
			if( $this->mdl_kapal->input($array_input) ){
				$url = base_url('profil_inka_mina/kapal/form_entry_kapal');
				redirect($url);
			}else{
				$url = base_url('profil_inka_mina/kapal/tabel_kapal_inka');
				redirect($url);
			}
		}else
		{
			if( $this->mdl_kapal->update($array_input) ){
				$url = base_url('profil_inka_mina/kapal/form_entry_kapal/'.$array_input['id_kapal']);
				redirect($url);
			}else{
				$url = base_url('profil_inka_mina/kapal/tabel_kapal_inka');
				redirect($url);
			}
		}
	}

	public function tabel_kapal_inka($filter = '')
	{
		$array_input = $this->input->post(NULL, TRUE);
		$propinsi = isset($array_input['provinsi'])?  $array_input['provinsi'] : 0;
		
		if(isset($array_input['export'])){
			$this->export_kapal($propinsi);
		}

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter != '' ) $data['filter'] = $filter;
		$data['data_kapal'] = $this->mdl_kapal->data_kapal_inka($propinsi);
		$data['submit_form'] = "profil_inka_mina/kapal/tabel_kapal_inka";
		$template = 'templates/page/v_form';
		$modules = 'profil_inka_mina';
		$views = 'tabel_kapal_inka';
		$labels = 'tabel_kapal_inka';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function kapal_report_word($data)
	{
		$this->load->library('word');
 		$this->word = new \PhpOffice\PhpWord\TemplateProcessor('text.docx');
 		//prepare data
 		// $data = $this->data_export;

		$styleTable = array('borderSize'=>6, 'borderColor'=>'4a4849', 'cellMargin'=>80);
 		$this->word->addTableStyle('kapal', $styleTable);

		$section = $this->word->createSection();
		$table = $section->addTable('kapal');
		$i = 1;
		/*echo "<pre>";
		print_r($this->session->all_userdata());
		echo "</pre>";
		die();*/
		
		// foreach ($data['detail_inka'] as $data_table) {
			$table->addRow(200);
			$table->addCell(3000)->addText("ID Kapal Bantuan");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->id_kapal);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("ID Kapal DSS");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->id_dss);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Nama Kapal");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->nama_kapal);
			$nama_kapal_doc = $data['detail_inka']->nama_kapal;
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Kabupaten/Kota");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->nama_kabupaten_kota);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Propinsi");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->nama_propinsi);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("DPI");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->nama_dpi);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("WPP");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_wpp']);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("ID Transmitter VMS");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->id_transmitter_vms);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Tanda Selar");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->tanda_selar);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Gross Akte");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->gross_akte);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("No. SIPI");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->sipi);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Masa Berlaku SIPI");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->tanggal_sipi." - ".$data['detail_inka']->tanggal_akhir_sipi);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Pelabuhan Pangkalan");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->pelabuhan_pangkalan);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Alat Tangkap");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->nama_alat_tangkap);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Dimensi Kapal");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->panjang_kapal." X ".$data['detail_inka']->lebar_kapal." X ".$data['detail_inka']->dalam_kapal);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Daya Kapal");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->daya);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("GT");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->gt);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Mesin");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->mesin);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("No Mesin");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->no_mesin);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Kontraktor Pembangunan");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->kontraktor_pembangunan);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Lokasi Pembuatan");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->lokasi_pembangunan);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Tahun Pembuatan");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->tahun_pembuatan);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Bahan Kapal");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->nama_bahan_kapal);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Permasalahan Kapal");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->masalah);
			$i++;
			$table->addRow(200);
			$table->addCell(3000)->addText("Keterangan");
			$table->addCell(100)->addText(":");
			$table->addCell(7000)->addText($data['detail_inka']->keterangan);
			$i++;

			$styleTable2 = array('borderSize'=>6, 'borderColor'=>'4a4849', 'cellMargin'=>80);
	 		$this->word->addTableStyle('kub', $styleTable2);

			$section = $this->word->createSection();
			$table2 = $section->addTable('kub');
			$i = 1;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("ID KUB");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->id_kub))? $data['detail_kub']->id_kub : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("ID Perusahaan DSS");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->id_perusahaan))? $data['detail_kub']->id_perusahaan : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Nama KUB");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_kub))? $data['detail_kub']->nama_kub : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("No SIUP");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->no_siup))? $data['detail_kub']->no_siup : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Tahun Pembentukan");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->tahun_pembentukan))? $data['detail_kub']->tahun_pembentukan : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Sumber Anggaran");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->sumber_anggaran))? $data['detail_kub']->sumber_anggaran : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("No Telepon");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->oleh))? $data['detail_kub']->oleh : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Oleh");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->nomor))? $data['detail_kub']->nomor : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("ALAMAT");
			$table2->addCell(100)->addText("");
			$table2->addCell(7000)->addText("");
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Kabupaten/Kota");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->id_kabupaten_kota))? $data['detail_kub']->id_kabupaten_kota : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Alamat Lengkap");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->alamat))? $data['detail_kub']->alamat : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("PENGURUS");
			$table2->addCell(100)->addText("");
			$table2->addCell(7000)->addText("");
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Nama Ketua");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_ketua))? $data['detail_kub']->nama_ketua : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Telepon Ketua");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->telp_ketua))? $data['detail_kub']->telp_ketua : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Nama Sekretaris");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_sekretaris))? $data['detail_kub']->nama_sekretaris : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Telepon Sekretaris");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->telp_sekretaris))? $data['detail_kub']->telp_sekretaris : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Nama Bendahara");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->nama_bendahara))? $data['detail_kub']->nama_bendahara : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Telepon Bendahara");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->telp_bendahara))? $data['detail_kub']->telp_bendahara : '');
			$i++;

			$table2->addRow(200);
			$table2->addCell(2000)->addText("Jumlah Anggota");
			$table2->addCell(100)->addText(":");
			$table2->addCell(7000)->addText((isset($data['detail_kub']->anggota))? $data['detail_kub']->anggota : '');
			$i++;		

		$styleTable3 = array('borderSize'=>6, 'borderColor'=>'4a4849', 'cellMargin'=>80);
 		$this->word->addTableStyle('produksi', $styleTable3);

		$section2 = $this->word->createSection();
		$sectionStyle2 = $section2->getSettings();
		$sectionStyle2->setLandscape();
		$sectionStyle2->setMarginLeft(5);
		$sectionStyle2->setMarginRight(5);


		$table3 = $section2->addTable('produksi');
		$i = 1;
		$table3->addRow(400);
		$table3->addCell(2000)->addText("No");
		$table3->addCell(2000)->addText("Nama Kapal");
		$table3->addCell(2000)->addText("WPP");
		$table3->addCell(2000)->addText("Daerah Penangkapan");
		$table3->addCell(2000)->addText("Tanggal Berangkat");
		$table3->addCell(2000)->addText("Tanggal Kembali");
		$table3->addCell(2000)->addText("Pelabuhan Keluar");
		$table3->addCell(2000)->addText("Pelabuhan Masuk");
		$table3->addCell(2000)->addText("Nama Nahkoda");
		$table3->addCell(2000)->addText("ABK");
		$table3->addCell(2000)->addText("Jenis Ikan");
		$table3->addCell(2000)->addText("Volume Ikan");
		$table3->addCell(2000)->addText("Jumlah Pendapatan");
		$table3->addCell(2000)->addText("Kebutuhan BBM");
		$table3->addCell(2000)->addText("Biaya Operasional");
		$table3->addCell(2000)->addText("Pendapatan Bersih");
		$i++;
		foreach ($data['detail_produksi'] as $produksi => $data_table) {
			$table3->addRow(400);
			$table3->addCell(2000)->addText($i-1);
			$table3->addCell(2000)->addText($data_table->nama_kapal);
			$table3->addCell(2000)->addText($data_table->nama_wpp);
			$table3->addCell(2000)->addText($data_table->nama_dpi);
			$table3->addCell(2000)->addText($data_table->tgl_keluar);
			$table3->addCell(2000)->addText($data_table->tgl_masuk);
			$table3->addCell(2000)->addText($data_table->nama_pelabuhan_keluar);
			$table3->addCell(2000)->addText($data_table->nama_pelabuhan_masuk);
			$table3->addCell(2000)->addText($data_table->nama_nahkoda);
			$table3->addCell(2000)->addText($data_table->jml_abk);
			$table3->addCell(2000)->addText($data_table->id_jenis_ikan);
			$table3->addCell(2000)->addText($data_table->jml_ikan);
			$table3->addCell(2000)->addText("Rp ".number_format($data_table->nilai_pendapatan,2));
			$table3->addCell(2000)->addText($data_table->kebutuhan_bbm);
			$table3->addCell(2000)->addText("Rp ".number_format($data_table->biaya_operasional,2));
			$table3->addCell(2000)->addText("RP ".number_format((($data_table->nilai_pendapatan - $data_table->biaya_operasional) - ($data_table->kebutuhan_bbm*7400)),2));
			$i++;
		}
		$nama_file = 'Report Kapal '.$nama_kapal_doc.'.docx';
		$filename = tempnam(base_url().'uploads/temp_export/', 'PHPWord');
		$objWriter = PHPWord_IOFactory::createWriter($this->word, 'Word2007');
		$objWriter->save($filename);
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.$nama_file);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filename));
		flush();
		readfile($filename);
		unlink($filename);
	}

	public function detail_kapal_inka($filter = '', $export = null)
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter != '' ) $id_kapal = $filter;
		$data['detail_inka'] = $this->mdl_kapal->detail_kapal_inka($id_kapal);
		$temp = array();
		if($data['detail_inka']->id_dpi != '')
		{		
			foreach ($this->mdl_kapal->detail_wpp_kapal($data['detail_inka']->id_dpi) as $row)
			{
			  $temp[] =$row->nama_wpp;
			}
			$data['detail_wpp'] = implode(' | ', $temp);
		}else
		{
			$data['detail_wpp'] = '';
		}
		$data['id_kapal'] = $id_kapal;
		$data['detail_kub'] = $this->mdl_kapal->detail_kub($data['detail_inka']->id_kub);
		$data['detail_produksi'] = $this->mdl_kapal->detail_produksi($id_kapal);
		$template = 'templates/page/v_view';
		$modules = 'profil_inka_mina';
		$views = 'detail_kapal';
		$labels = 'tabel_produksi';

		if($export!=null)
		{
			$this->kapal_report_word($data);
		}
		// $this->session->set_userdata('data_inka',$data);

		/*echo "<pre>";
		print_r($this->session->userdata('data_inka'));
		echo "</pre>";*/
		

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function delete_kapal_inka($id)
    {
        $this->mdl_kapal->delete($id);

        $url = base_url('profil_inka_mina/kapal/tabel_kapal_inka');
        redirect($url);
    }

    public function list_opsi_permasalahan()
	{

		$list_opsi = $this->mdl_kapal->list_opsi_permasalahan();

		return $list_opsi;
	}

	public function export_kapal($id_propinsi)
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		$template = FCPATH.'assets\kapi\format\format_kapal.xlsx';
		// $template = dirname(__FILE__).'/'.$_GET['kode'].'.xlsx';
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSetting = array('memoryCacheSize' => '100MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSetting);

		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($template);
		$objWorksheet = $objPHPExcel->getSheet(0);

		/*content*/
		$data = $this->mdl_kapal->daftar_kapal($id_propinsi);
		$column = Array(
					"A","B","C","D","E","F","G","H","I","J","K","L","M",
					"N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
					"AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM",
					"AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
					"BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM",
					"BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ",
					"CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM",
					"CN","CO","CP","CQ","CR","CS","CT","CU","CV","CW","CX","CY","CZ",
					"DA","DB","DC","DD","DE","DF","DG","DH","DI","DJ","DK","DL","DM",
					"DN","DO","DP","DQ","DR","DS","DT","DU","DV","DW","DX","DY","DZ",
					"EA","EB","EC","ED","EE","EF","EG","EH","EI","EJ","EK","EL","EM",
					"EN","EO","EP","EQ","ER","ES","ET","EU","EV","EW","EX","EY","EZ",
					"FA","FB","FC","FD","FE","FF","FG","FH","FI","FJ","FK","FL","FM",
					"FN","FO","FP","FQ","FR","FS","FT","FU","FV","FW","FX","FY","FZ",
					"GA","GB","GC","GD","GE","GF","GG","GH","GI","GJ","GK","GL","GM",
					"GN","GO","GP","GQ","GR","GS","GT","GU","GV","GW","GX","GY","GZ",
					"HA","HB","HC","HD","HE","HF","HG","HH","HI","HJ","HK","HL","HM",
					"HN","HO","HP","HQ","HR","HS","HT","HU","HV","HW","HX","HY","HZ",
					"IA","IB","IC","ID","IE","IF","IG","IH","II","IJ","IK","IL","IM",
					"IN","IO","IP","IQ","IR","IS","IT","IU","IV","IW","IX","IY","IZ"
					);
		$rowNumber = 6;
		foreach ($data as $item)
		{
			$col=1;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber, ($rowNumber-5));
			foreach($item as $key => $cell) {
				// $objPHPExcel->getActiveSheet()->setCellValue($column[$col].$rowNumber, "A");
				$objPHPExcel->getActiveSheet()->setCellValue($column[$col].$rowNumber, $cell);
				$col++;
			}

			$rowNumber++;
		}
		/*End Content*/
		// die();
		//Border
		$styleArray = array(
		    'borders' => array(
		      'allborders' => array(
		          'style' => PHPExcel_Style_Border::BORDER_THIN
		      )
		    )
		);
		$objPHPExcel->getActiveSheet()->getStyle('A6:AM'.($rowNumber-1))->applyFromArray($styleArray);
		// Save as an Excel BIFF (xls) file 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename=" Data Master Kapal - '.date("d-m-Y h:i").'.xlsx"'); 
		header('Cache-Control: max-age=0'); 

		$objWriter->save('php://output'); 

	}
}
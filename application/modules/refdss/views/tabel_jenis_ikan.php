<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_jenis_ikan' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_ikan){
		foreach ($list_ikan as $item) {

			// $image_properties['src'] = 'uploads/'.$item->foto_ikan;
			// $image_properties['height'] = "150";
			// img($image_properties);

			$this->table->add_row(
								$counter.'.',
								$item->nama_jenis_ikan,
								$item->nama_jenis_ikan_inggris,
								$item->nama_latin,
								$item->nama_daerah
								// '<img src="'.base_url().$item->foto_ikan.'" alt="Smiley face" height="42" width="42">'
								);
			$counter++;
		}
	}

	$table_jenis_ikan = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_jenis_ikan;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_jenis_ikan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"}
                       
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>
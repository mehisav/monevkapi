<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_jenis_ikan extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }

	public function list_jenis_ikan()
    {
        $this->db_dss->where('aktif', 'YA');
    	$run_query = $this->db_dss->get('mst_jenis_ikan');                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi()
    {
        $query = "SELECT id_jenis_ikan AS id, nama_jenis_ikan as text FROM mst_jenis_ikan WHERE aktif='Ya'";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
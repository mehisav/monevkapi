
<table border=2>
	<tr>
		<td style='background-color:#A8A8A8; text-align:center; width:25px'>NO</td>
		<td style='background-color:#A8A8A8; text-align:center' colspan=3>Uraian</td>
		<td style='background-color:#A8A8A8; text-align:center' >Total Kapal</td>
		<td style='background-color:#A8A8A8; text-align:center' >Detail</td>
	</tr>
	<tr>
		<td style='text-align:center;'>1</td>
		<td colspan=3>Total Kapal</td>
		<td style='text-align:right;'> <?php echo $jumlah_kapal;?> </td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/jumlah_kapal');?>">View</a></td>
	</tr>
	<tr>
		<td style='text-align:center;'>2</td>
		<td colspan=3>SIPI Terbit</td>
		<td style='text-align:right;'> <?php echo $jumlah_sipi;?> </td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/jumlah_sipi');?>">View</a></td>
	</tr>
	<tr>
		<td style='text-align:center;'>3</td>
		<td colspan=3>Belum ada SIPI</td>
		<td style='text-align:right;'> <?php echo $jumlah_belum_sipi;?> </td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/jumlah_belum_sipi');?>">View</a></td>
	</tr>
	<tr>
		<td style='text-align:center;'>4</td>
		<td colspan=3>Kapal Operasional & Berhenti Operasional</td>
		<td style='text-align:right;'><?php echo $aktif_berhenti;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/aktif_berhenti');?>">View</a></td>
	</tr>	
	<tr>
		<td rowspan=7 style='text-align:center;'>5</td>
		<td rowspan=7>Kapal Oprasional</td>
		<td colspan=2 >Total</td>
		<td style='text-align:right;'> <?php echo $kapal_aktif;?> </td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kapal_aktif');?>">View</a></td>
	</tr>	
	<tr>
		<td rowspan=2>Mengunakan SIPI</td>
		<td>Aktif</td>
		<td style='text-align:right;'> <?php echo $aktif_sipi;?> </td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/aktif_sipi');?>">View</a></td>
	</tr>
	<tr>
		<td>Expired</td>
		<td style='text-align:right;'> <?php echo $habis_sipi;?> </td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/habis_sipi');?>">View</a></td>
	</tr>
	<tr>
		<td colspan=2>Menggunakan Rekomendasi Dinas</td>
		<td style='text-align:right;'><?php echo $izin_dinas;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/izin_dinas');?>">View</a></td>
	</tr>
	<tr>
		<td colspan=2>Total Laporan</td>
		<td style='text-align:right;'><?php echo $total_lapor;?></td>
		<td style='text-align:center;'></td>
	</tr>
	<tr>
		<td colspan=2>Sudah Lapor</td>
		<td style='text-align:right;'><?php echo $sudah_lapor;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/sudah_lapor');?>">View</a></td>
	</tr>
	<tr>
		<td colspan=2>Belum Lapor</td>
		<td style='text-align:right;'><?php echo ($jumlah_kapal - $sudah_lapor);?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/belum_lapor');?>">View</a></td>
	</tr>
	<tr>
		<td rowspan=6 style='text-align:center;'>6</td>
		<td rowspan=6>Berhenti Operasional</td>
		<td colspan=2>Total</td>
		<td style='text-align:right;'><?php echo ($berhenti_oprasional);?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/berhenti_orpasional');?>">View</a></td>
	</tr>
	<tr>
		<td rowspan=4>Kerusakan Pada Kapal</td>
		<td>Mesin</td>
		<td style='text-align:right;'><?php echo $rusak_mesin;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/rusak_mesin');?>">View</a></td>
	</tr>
	<tr>
		<td>Body</td>
		<td style='text-align:right;'><?php echo $rusak_body;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/rusak_body');?>">View</a></td>
	</tr>
	<tr>
		<td>Kebakaran</td>
		<td style='text-align:right;'><?php echo $kebakaran;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kebakaran');?>">View</a></td>
	</tr>
	<tr>
		<td>Cuaca Buruk</td>
		<td style='text-align:right;'><?php echo $cuaca_buruk;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/cuaca_buruk');?>">View</a></td>
	</tr>
	<tr>
		<td colspan=2>Merugi</td>
		<td style='text-align:right;'><?php echo $merugi;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/merugi');?>">View</a></td>
	</tr>
	<tr>
		<td rowspan=5 style='text-align:center;'>7</td>
		<td rowspan=5>Belum Oprasional</td>
		<td colspan=2>Total</td>
		<td style='text-align:right;'><?php echo ($belum_oprasi);?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/belum_oprasi');?>">View</a></td>
	</tr>	
	<tr>
		<td colspan=2>Belum Memiliki Sokumen Perizinan (SIPI/Surat Rekomendasi)</td>
		<td style='text-align:right;'><?php echo $jumlah_belum_sipi;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/jumlah_belum_sipi');?>">View</a></td>
	</tr>
	<tr>
		<td colspan=2>Tidak Ada Biaya Oprasional</td>
		<td style='text-align:right;'><?php echo $tidak_biaya;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/tidak_biaya');?>">View</a></td>
	</tr>
	<tr>
		<td rowspan=2>Sepsfikasi Tidak Susuai Dengan Harapan KUB</td>
		<td>Body</td>
		<td style='text-align:right;'><?php echo $kurang_body;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kurang_body');?>">View</a></td>
	</tr>
	<tr>
		<td>API</td>
		<td style='text-align:right;'> <?php echo $kurang_api;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kurang_api');?>">View</a></td>
	</tr>
	<tr>
		<td rowspan=5 style='text-align:center;'>8</td>
		<td rowspan=5>Kendala Oprasional</td>
		<td colspan=2>Total</td>
		<td style='text-align:right;'><?php echo ($kendala_pemasaran+$kendala_rumpon+$kendala_bbm+$kendala_vms);?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kendala_oprasional');?>">View</a></td>
	</tr>	
	<tr>
		<td colspan=2>Pemasaran Hasil Tangkapan</td>
		<td style='text-align:right;'><?php echo $kendala_pemasaran;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kendala_pemasaran');?>">View</a></td>
	</tr>	
	<tr>
		<td colspan=2>Kerusakan Rumpon</td>
		<td style='text-align:right;'><?php echo $kendala_rumpon;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kendala_rumpon');?>">View</a></td>
	</tr>	
	<tr>
		<td colspan=2>Harga BBM Tinggi</td>
		<td style='text-align:right;'><?php echo $kendala_bbm;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kendala_bbm');?>">View</a></td>
	</tr>	
	<tr>
		<td colspan=2>Tidak asa VMS & Rumpon</td>
		<td style='text-align:right;'><?php echo $kendala_vms;?></td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/kendala_vms');?>">View</a></td>
	</tr>
	<tr>
		<td rowspan=5 style='text-align:center;'>9</td>
		<td rowspan=5>Kapal Digunakan Jadi Bagan Tancap</td>
		<td colspan=2>Total</td>
		<td style='text-align:right;'>0</td>
		<td style='text-align:center;'><a style="width=120" href="<?php echo base_url('monitoring/oprasi/info_kapal/bagan_tancap');?>">View</a></td>
	</tr>
</table>
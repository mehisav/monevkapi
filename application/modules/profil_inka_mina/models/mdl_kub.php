<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kub extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        $this->load->database();
        
    }

    public function data_perusahaan_dss()
    {
        $this->load->database();
        $query = 'SELECT    dmm.id_perusahaan,
                            dmm.no_siup,
                            dmm.nama_perusahaan,
                            dmm.alamat_perusahaan,
                            dmm.nama_penanggung_jawab
                FROM    db_master.mst_perusahaan as dmm
                LEFT JOIN db_monev_kapi.mst_kub as mkub ON mkub.id_perusahaan = dmm.id_perusahaan
                WHERE dmm.aktif = "YA" AND mkub.id_perusahaan IS NULL AND
                    (dmm.nama_perusahaan LIKE "KUB %"
                    OR dmm.nama_perusahaan LIKE "Koperasi%"
                    OR dmm.nama_perusahaan LIKE "Kelompok Nelayan%"
                    OR dmm.nama_perusahaan LIKE "KN%"
                    OR dmm.nama_perusahaan LIKE "KUD%"
                    OR dmm.nama_perusahaan LIKE "Koperasi Unit Desa%"
                    OR dmm.nama_perusahaan LIKE "%LEPP%"
                    OR dmm.nama_perusahaan LIKE "%PUSKONELI%"
                    OR dmm.nama_perusahaan LIKE "%Perkumpulan Nelayan%"
                    )
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function perusahaan_dss($id)
    {
        $this->load->database();
        $query = "SELECT mk.*
                FROM db_master.mst_perusahaan AS mk
                WHERE mk.id_perusahaan = $id";
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input_dss($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $data_dss = $this->perusahaan_dss($id);
        // vdump($kapal_dss);die();
        $data = array();
        $data['id_perusahaan'] = $data_dss->id_perusahaan;
        $data['nama_kub'] = $data_dss->nama_perusahaan;
        $data['no_siup'] = $data_dss->no_siup;
        $data['tgl_siup'] = $data_dss->tanggal_siup;
        $data['nama_ketua'] = $data_dss->nama_penanggung_jawab;
        $data['alamat'] = $data_dss->alamat_perusahaan;

        $this->db_monev->insert('mst_kub',$data);  
        if( $this->db_monev->affected_rows() > 0)
        {
            $result = true;
        }else
        {
            $result = false;
        }
        return $result;
    }

    public function daftar_kub()
    {
        // $this->db_monev->where('aktif', 'YA');
        $query = "SELECT mk.*,
                    mim.nama_kapal,
                    mkk.nama_kabupaten_kota AS 'kabupaten',
                    mp.nama_propinsi
                FROM db_monev_kapi.mst_kub AS mk
                LEFT JOIN db_monev_kapi.mst_inka_mina AS mim ON mim.id_kapal = mk.id_kapal
                LEFT JOIN db_master.mst_kabupaten_kota AS mkk ON mkk.id_kabupaten_kota = mk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi AS mp ON mp.id_propinsi = mkk.id_propinsi
                WHERE mk.aktif = 'Ya'
            ";
      $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_kub($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT mk.*
                FROM mst_kub AS mk
                WHERE mk.id_kub = $id";
        $run_query = $this->db_monev->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_kub()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->select('id_kub AS "id",
                                nama_kub AS "text"');
        $run_query = $this->db_monev->get('mst_kub');   
        // $str = $this->db_monev->last_query();
        // echo $str; die;
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function insert_kub($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->insert('mst_kub', $data);
        if( $this->db_monev->affected_rows() > 0)
        {
            $result = true;
        }else
        {
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $sql = " UPDATE mst_kub SET aktif='Tidak' WHERE id_kub=$id ";
        
        $query = $this->db_monev->query($sql);
    }

    public function update_kub($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->where('id_kub', $data['id_kub']);
        $query = $this->db_monev->update('mst_kub',$data);

        if($this->db_monev->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function list_opsi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT id_kub AS id, nama_kub as text FROM mst_kub WHERE aktif='Ya'";

        $run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_kapal extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_report_kapal');
		}

	public function index()
	{
		$this->report_kapal();

	}

	public function report_kapal( $filter = '' )
	{
		$array_input = $this->input->post(NULL, TRUE);
		$data_dasar = isset($array_input['dasar_kapal'])?  $array_input['dasar_kapal'] : "0";

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'report_data_kapal';
		$data['submit_form'] = "reporting/report_kapal";
		if (strcmp($data_dasar,'0')==0) 
		{
			$data['chart'] = false;
		}else if(strcmp($data_dasar,'gt')==0)
		{
			$data['data_dasar'] = 'gt';
			$data['chart'] = true;
			$data['chart_title'] = "Data Kapal Berdasarkan GT";
			$data['chart_isi'] = $this->mdl_report_kapal->jumlah_kapal_gt();
		}else if(strcmp($data_dasar, 'propinsi')==0)
		{
			$data['data_dasar'] = 'propinsi';
			$data['chart'] = true;
			$data['chart_title'] = "Data Kapal Berdasarkan Propinsi";
			$data['chart_isi'] = $this->mdl_report_kapal->jumlah_kapal_propinsi();
		}else if(strcmp($data_dasar, 'bahan_kapal')==0)
		{
			$data['data_dasar'] = 'bahan_kapal';
			$data['chart'] = true;
			$data['chart_title'] = "Data Kapal Berdasarkan Bahan Kapal";
			$data['chart_isi'] = $this->mdl_report_kapal->jumlah_kapal_bahan_kapal();
		}else if(strcmp($data_dasar, 'alat_tangkap')==0)
		{
			$data['data_dasar'] = 'alat_tangkap';
			$data['chart'] = true;
			$data['chart_title'] = "Data Kapal Berdasarkan Alat Tangkap";
			$data['chart_isi'] = $this->mdl_report_kapal->jumlah_kapal_alat_tangkap();
		}
		
		echo Modules::run('templates/page/v_form', 'reporting', 'kapal_chart_column', $labels, $add_js, $add_css, $data);

	}

	public function list_kapal_array()
	{
		$list_opsi = $this->mdl_report_kapal->list_kapal();
		
		return $list_opsi;
	}

}
<!-- mulai #kapiheader --> 
<div id="kapiheader">  
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<h1 id="kapilogo"><a href="<?php echo base_url();?>"><img src="<?php echo $logo_url;?>/logo.png" class="img-responsive" alt="KAPI | Aplikasi Monitoring Evaluasi Bantuan Kapal"></a></h1>
			</div>
			<div class="col-lg-2">
		        <?php $cek_login = ($this->session->userdata('logged_in')) ? '' : header( 'Location: http://integrasi.djpt.kkp.go.id/login_baru/' ); ?>
		        <?php $is_admin = ($this->mksess->info_is_admin()) ? ' (ADMIN)' : ' (OPERATOR)'; ?>
				<h5><b>Pengguna: <?php echo $this->mksess->nama_pengguna().$is_admin; ?> </b></h5>
				<h5><b><?php //echo $cek_login; ?> </b></h5>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div style="text-align: right; font-size: 0.9em; background-color: rgba(0, 128, 255, 0.3); padding: 10px; display: none;">
							&nbsp;
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- akhir #kapiheader -->
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_propinsi extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->load->database();
    }

	public function list_propinsi()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $query = 'SELECT mp.nama_propinsi, jp.jumlah_pelabuhan, jk.jumlah_kabupaten
                    FROM mst_propinsi AS mp
                    LEFT JOIN (
                        SELECT mp.id_propinsi, COUNT(mpb.id_pelabuhan) AS "jumlah_pelabuhan"
                        FROM mst_pelabuhan AS mpb
                        LEFT JOIN mst_kabupaten_kota AS mk ON mk.id_kabupaten_kota = mpb.id_kabupaten_kota
                        LEFT JOIN mst_propinsi AS mp ON mp.id_propinsi = mk.id_propinsi
                        WHERE mpb.aktif = "YA" AND mk.aktif = "YA" AND mp.aktif = "YA"
                        GROUP BY mk.id_propinsi
                        ) AS jp ON jp.id_propinsi = mp.id_propinsi
                    LEFT JOIN (
                        SELECT mp.id_propinsi, COUNT(mk.id_kabupaten_kota) AS "jumlah_kabupaten"
                        FROM mst_kabupaten_kota AS mk
                        LEFT JOIN mst_propinsi AS mp ON mp.id_propinsi = mk.id_propinsi
                        WHERE mk.aktif = "YA" AND mp.aktif = "YA"
                        GROUP BY mk.id_propinsi
                        ) AS jk ON jk.id_propinsi = mp.id_propinsi
                    WHERE mp.aktif = "YA"
        
        ';
    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);

        $query = "SELECT DISTINCT mp.id_propinsi AS id, mp.nama_propinsi as text 
                FROM  db_master.mst_kabupaten_kota as mkk
                LEFT JOIN db_master.mst_propinsi as mp ON mkk.id_propinsi = mp.id_propinsi
                WHERE mkk.aktif='Ya'";

        $run_query = $this->db_dss->query($query);                         
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
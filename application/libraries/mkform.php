<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * Library untuk bikin form input 
 */
 
class Mkform
{
	/*  @input_text
		$attr = array( 'value' => 'default value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_text($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		//Menambah fasilitas disabled pada satu input_text
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}

		$html = '
				<script>
					
					// function auto_width(){
				 //      //jQuery.fn.exists = function(){return this.length>0;}
				 //        if($("#id_'.$attr['name'].'").val().length < 30  )
				 //        {
					// 		$("#id_'.$attr['name'].'").css({"width":"32%"});
				 //        }
				 //        else{
					// 		var panjang = $("#id_'.$attr['name'].'").val().length;
				 //            $("#id_'.$attr['name'].'").css({"width":(panjang)+32+"%"}); 
				 //        }
				 //    }

				 //    s_func.push(auto_width);
      
				</script>
				<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				type="text"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" 
	      				'.$disabled.' >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	function sub_input_text($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		//Menambah fasilitas disabled pada satu input_text
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}

		$html = '
				<script>
					
					// function auto_width(){
				 //      //jQuery.fn.exists = function(){return this.length>0;}
				 //        if($("#id_'.$attr['name'].'").val().length < 30  )
				 //        {
					// 		$("#id_'.$attr['name'].'").css({"width":"32%"});
				 //        }
				 //        else{
					// 		var panjang = $("#id_'.$attr['name'].'").val().length;
				 //            $("#id_'.$attr['name'].'").css({"width":(panjang)+32+"%"}); 
				 //        }
				 //    }

				 //    s_func.push(auto_width);
      
				</script>
				<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-4 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				style="width:145px"
	      				type="text"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" 
	      				'.$disabled.' >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	function input_range($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		//Menambah fasilitas disabled pada satu input_text
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}

		$html = '
				<script>
					
					function auto_width(){
				      //jQuery.fn.exists = function(){return this.length>0;}
				        if($("#id_'.$attr['name'].'").val().length < 30  )
				        {
							$("#id_'.$attr['name'].'").css({"width":"32%"});
				        }
				        else{
							var panjang = $("#id_'.$attr['name'].'").val().length;
				            $("#id_'.$attr['name'].'").css({"width":(panjang)+32+"%"}); 
				        }
				    }

				    s_func.push(auto_width);
      
				</script>
				<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				type="number"
	      				min="'.$attr['min_gt'].'"
	      				max="'.$attr['limit_gt'].'"
	      				step="2"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" 
	      				'.$disabled.' >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	function input_hidden($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		//Menambah fasilitas disabled pada satu input_text
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}
		else
		{
			$disabled = '';
		}
		$html = '<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				type="hidden"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" 
	      				'.$disabled.' >
	    		<!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_textarea
		$attr = array( 'value' => 'default value', // optional
					   'rows' => '3', // wajib ada
					   'name' => 'input name', // wajib ada
					   'label' => 'text label for input' // wajib ada
	 				);
	 */
	function input_textarea($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$html = '<script>
					
					function auto_width(){
				      //jQuery.fn.exists = function(){return this.length>0;}
				        if($("#id_'.$attr['name'].'").val().length < 30  )
				        {
							$("#id_'.$attr['name'].'").css({"width":"32%"});
				        }
				        else{
							var panjang = $("#id_'.$attr['name'].'").val().length;
				            $("#id_'.$attr['name'].'").css({"width":(panjang/3)+32+"%"}); 
				        }
				    }

				    s_func.push(auto_width);
      
				</script>';
		$html .= '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<textarea id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				rows="'.$attr['rows'].'" >'.$default_value.'</textarea>
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_select
		$attr = array( 'array_opsi' => array('1' => 'Baru', '2'=> 'Lama'), // optional 
					   'value' => 'selected value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_select($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$is_enable = isset($attr['enable']) ? $attr['enable'] : 'true';
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}
		$html = '
				<script>
					
					function auto_width(){
				      //jQuery.fn.exists = function(){return this.length>0;}
				        if($("#id_'.$attr['name'].'").val().length < 30  )
				        {
							$("#id_'.$attr['name'].'").css({"width":"32%"});
				        }
				        else{
							var panjang = $("#id_'.$attr['name'].'").val().length;
				            $("#id_'.$attr['name'].'").css({"width":(panjang/3)+32+"%"}); 
				        }
				    }

				    s_func.push(auto_width);
      
				</script>
				<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$attr['name'].'" name="'.$attr['name'].'" class="form-control" ';

	    if(isset($attr['OnChange'])):
	    	if($attr['OnChange']!==''): $html .= 'OnChange='.$attr['OnChange'];endif;
	    endif;

	    $html .= ' '.$disabled.' >';
	      			foreach ($array_opsi as $value => $text) {
	      				/*if($default_value == $value)
	      				{
	      					$selected = 'selected';
	      				}
	      				else{
	      					$selected = '';
	      				}*/
	      				$selected = $default_value == $value ? 'selected' : ''; 
	      				$html .= '<option value="'.$value.'" '.$selected.' >'.$text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div><!-- akhir form select '.$attr['name'].' -->';
	  	if($is_enable == "false")
	  	{
	  	$html .= '<script>
         			$("#id_'.$attr['name'].'").prop("disabled", true).removeAttr("class").css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
         		   </script>';
        }
	 return $html;
	}

	/*  @input_select
		$attr = array( 'array_opsi' => array('1' => 'Baru', '2'=> 'Lama'), // optional 
					   'value' => 'selected value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_select2($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$id_elemen = str_replace('[]', '', $attr['name']);

		if(isset($attr['is_multipel']) && $attr['is_multipel'] === TRUE){
			
			$multipel = 'multiple';

			if(!empty($attr['value_m'])){
				
				$value = 'var values="'.$attr['value_m'].'";
						$.each(values.split(","), function(i,e){
						    $("#id_'.$id_elemen.' option[value=\'" + e + "\']").prop("selected", true);
						});';
			}
			else $value = '';
		} 
		else {
			$multipel = '';
			$value ='';
		}

		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select '.$multipel.' id="id_'.$id_elemen.'" name="'.$attr['name'].'" style="width: 100% !important;">
	      			<option value="0">'.(isset($attr['all']) && $attr['all']===TRUE ? 'Semua':'-').'</option>';
	      			foreach ($array_opsi as $item) {
	      				$selected = $default_value === $item->id ? 'selected' : ''; 
	      				$html .= '<option value="'.$item->id.'" '.$selected.'>'.$item->text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div>';
	  	$js   = '<script>
	  				
	  				'.$value.'
	  				var func_'.$id_elemen.' = function() {
									  					$("#id_'.$id_elemen.'").select2();
									  				};
					s_func.push(func_'.$id_elemen.');
	  			</script>
	  			<!-- akhir form select '.$id_elemen.' -->';
	 return $html.$js;
	}

	/*  @input_checkbox
		$attr = array( 'value' => 'checked value', // Wajib ada
					   'name' => 'input name', // Wajib ada
					   'label' => 'text label for input' // Wajib ada
					   'is_checked' => '1' // 1 : checked , 0 : not checked , default is 0
	 				);
	 */
	function input_checkbox($attr)
	{
		$is_checked = '';
		$class = '';
		if(isset($attr['is_checked']))
		{
			$is_checked = $attr['is_checked'] === $attr['value'] ? ' checked ' : '';
		}
		if(isset($attr['class']))
		{
			$class = $attr['class'];
		}


		$html = '<input class="'.$class.'" type="checkbox" id="id_'.$attr['name'].'" name="'.$attr['name'].'" value="'.$attr['value'].'" '.$is_checked.'>
        		 ';
        return $html;
	}

	function input_checkbox_perubahan($attr)
	{
		$is_checked = '';
		if(isset($attr['is_checked']))
		{
			$is_checked = $attr['is_checked'] === $attr['value'] ? ' checked ' : '';
		}
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$html = '<ul class="list-group">';
		$temp = 0;
		foreach ($array_opsi as $item) {

			if($temp != $item->id_kategori)
			{
				$html .= '<b> > '.$item->kategori.'</b>';
			}

				if(isset($attr['trs']))
				{
					$is_checked = $item->isi === $attr['value'] ? ' checked ' : '';
				}
				$html .= '<li class="list-group-item kel-dok">
					  <input type="checkbox" id="'.$item->text.'" name="cekbox['.$item->id.']" value="'.$item->kolom_db.'|'.$item->variable.'" '.$is_checked.'>
		      		  <label for="id_'.$item->text.'">'.$item->text.'</label></li>';

		    $temp = $item->id_kategori;
		}
		$html .= '</ul>';
	    return $html;
	}

	function input_checkbox_perubahan_2($attr)
	{
		
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$array_checked = !empty($attr['cheked']) ? $attr['cheked'] : array('0' => 'kosong');

		// vdump($array_opsi);
		// vdump($array_checked);

		$html = '<ul class="list-group">';
		$temp = 0;
		foreach ($array_opsi as $item) {

			if($temp != $item->id_kategori)
			{
				$html .= '<b> > '.$item->kategori.'</b>';
			}

			$is_checked = '';
			foreach ($array_checked as $cek) {
				if($item->id === $cek->id_tipe_perubahan)
				{
					$is_checked = ' checked ';
					break;
				}
			}

			$html .= '<li class="list-group-item kel-dok">
				  <input type="checkbox" id="'.$item->text.'" name="cekbox_perubahan['.$item->id.']" value="'.$item->kolom_db.'|'.$item->variable.'" '.$is_checked.'>
	      		  <label for="id_'.$item->text.'">'.$item->text.'</label></li>';


		    $temp = $item->id_kategori;
		}
		$html .= '</ul>';
	    return $html;
	}

	function radio_dokumen($attr)
	{
		$is_checked = '';
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : false;
		$html = '<table id="table_buku_kapal" class="table table-bordered">
					<thead>
						<tr>
							<th width="20" style="padding-right: 0px;padding-left: 0px;">No. </th>
							<th width="50">Nama Dokumen</th>
							<th width="20" style="padding-right: 0px;padding-left: 0px;"">Ada</th>
							<th width="20" style="padding-right: 0px;padding-left: 0px;">Tidak</th>
							<th width="20" style="padding-right: 0px;padding-left: 0px;">Upload</th>
						</tr>
					</thead>
				<tbody>
					';


		$counter = 1;
		if($array_opsi){
			foreach ($array_opsi as $item) {
				$ya_checked = '';
				$file = '';
				$tidak_checked = 'checked';
				if(isset($attr['trs']))
				{
					$ya_checked = $item->isi == $attr['value'] ? ' checked ' : '';
					$tidak_checked = '';
					if(!empty($item->path_dok))
					{

						$file = explode('uploads/document/',$item->path_dok);
						$file = $file[1];

					}
				}
				$html .= '<tr>
							<td align="center">
								'.$counter.'.
							</td>
							<td>
								'.$item->text.'
							</td>
							<td align="center">
								<input type="radio" id="'.$item->text.'" name="cekbox['.$item->id.']" value="ADA" '.$ya_checked.'></input>
							</td>
							<td align="center">
								<input type="radio" id="'.$item->text.'" name="cekbox['.$item->id.']" value="TIDAK" '.$tidak_checked.'></input>
							</td>
							<td>
								<input type="file" style="width:190px;" id="file_'.$item->id.'" name="file_'.$item->id.'" /> <b>'.$file.'</b>
							</td>
		      			</tr>';
		      			$counter++;
			}
		}else{
			$html .= '<tr><td colspan="5" align="center" ><b>Tidak mempunyai dokumen.</b></td></tr>';
		}
		$html .= '		</tbody>
					</table>';
	    return $html;
	}


	/*  @input_date
		$attr = array( 'mindate' => 'various', // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
					   'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
					   'defaultdate' => '2013-20-12', // opsi: '', tidak wajib ada
					   'placeholder' => '', // wajib ada atau '' (kosong)
					   'name' => 'input name', // wajib ada
					   'label' => 'text label for input' // wajib ada
	 				);
	 */
	function input_date($attr)
	{	

		if( !isset($attr['mindate']) || empty($attr['mindate']) )
		{
			$set_mindate = '';
		}elseif(is_array($attr['mindate'])){
			$timestamp = strtotime('-'.$attr['mindate']['time']);
			$mindate =  date('Y-m-d', $timestamp);
			$set_mindate = 'minDate: new Date("'.$mindate.'"),';
		}else{
			$set_mindate = 'new Date("'.$attr['mindate'].'"),';
		}

		if( !isset($attr['maxdate']) || empty($attr['mindate']) )
		{
			$set_maxdate = 'maxDate: new Date(),';
		}elseif(is_array($attr['maxdate'])){
			$timestamp = strtotime('+'.$attr['maxdate']['time']);
			$maxdate =  date('Y-m-d', $timestamp);
			$set_maxdate = 'minDate: new Date("'.$maxdate.'"),';
		}else{
			$set_maxdate = 'new Date("'.$attr['maxdate'].'"),';
		}

		if( !isset($attr['default_date']) || empty($attr['default_date']) )
		{
			$set_default_date = "new Date()";
		}else{
			$set_default_date = "new Date('".$attr['default_date']."')";
		}
         $html = '<div class="form-group mkform-date"><!-- mulai form date '.$attr['name'].' -->';
         $html .= '<label for="id_'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>';
         $html .=       '<div class="col-sm-8">
                         <input id="id_'.$attr['name'].'" 
                         		name="'.$attr['name'].'"
                         		type="hidden"
                         		>
                         <input id="id_'.$attr['name'].'_display" 
                         			readonly="true"
                         			type="text"
                         			class="form-control">
                         </div>
                       </div>';
         $js =  "<script>
                $(document).ready(function() {
		                $('#id_".$attr['name']."_display').datepicker({  
		                                                            altField: '#id_".$attr['name']."',
		                                                            altFormat: 'yy-mm-dd',
		                                                            ".$set_mindate."
		                                                            ".$set_maxdate."
		                                                            changeMonth: true,
		                                                            changeYear: true,
		                                                            dateFormat: 'dd/mm/yy',
		                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
		                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab']
		                                                         });
		                  $('#id_".$attr['name']."_display').datepicker( 'setDate', ".$set_default_date." );
		                  $('#id_".$attr['name']."_display').focus(function(){ 
		                  $('#ui-datepicker-div').css('z-index', '9999');
	                  	});";
          $js .= "});
                </script> <!-- akhir form date ".$attr['name']." -->";                      
         return $html.$js;
	}

	function dropdown_dss($attr)
      {
		 $is_enable = isset($attr['enable']) ? $attr['enable'] : 'true';
		 $is_pencarian = isset($attr['pencarian']) ? $attr['pencarian'] : 'false';
         $array_opsi = array();
         if( $attr['array_opsi'] === '' )
         {
            $get_opsi = Modules::run('tables/dss_list_all', $attr['from_table'], $attr['field_text']);
            //var_dump('holaho',$get_opsi);
            foreach ($get_opsi as $index => $record) {
                $temp_array = array();
                $temp_value = '';
                $temp_text = '';
                  foreach ($record as $key => $value) {
                      if($key === $attr['field_value'] )
                      { 
                        $temp_value = $value;
                      }

                      if($key === $attr['field_text'] )
                      {
                        $temp_text = $value;
                      }
                  }
                $temp_array = array( $temp_value => $temp_text );
                $array_opsi = $temp_array + $array_opsi;
          }

         } else {
            $array_opsi = $attr['array_opsi'];
         }
         $html = '<div class="form-group">';
         $html .= $attr['label_class'] !== "none" ? '<label for="'.$attr['input_name'].'" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
         $html .= '<div class="'.$attr['input_width'].'">
                         <select id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" class="'.$attr['input_class'].'">';
                  foreach ($array_opsi as $value => $text) {
                        $selected = (String)$value === $attr['opsi_selected'] ? 'selected' : '';
                        $html .= '<option value="'.$value.'" '.$selected.'>'.$text.'</option>';

                  }
 				  if($is_pencarian == "TRUE"){
 					$html .= '<option value="semua" '.$selected.'>Semua</option>';
     				}
         $html .= '</select>
                       </div>
                  </div>';
         if($is_enable == "false")
         {
         $html .= '<script>
         			$("#'.$attr['input_id'].'").prop("disabled", true).removeAttr("class").css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
         		   </script>';
          }
         return $html;
	}


	function input_select_level($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		// print_r($attr['opsi']);die;
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$id_elemen = str_replace('[]', '', $attr['name']);

		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$id_elemen.'" name="'.$attr['name'].'" style="width: 100% !important;">
	      				<option value="0">-</option>';
                   $t_label = '####';
                   foreach ($array_opsi as $item) {
	      				$selected = $default_value === $item->id ? 'selected' : ''; 
                   		if(strcmp($item->label, $t_label) != 0 )
                   		{
                   			if(strcmp('####', $t_label)!=0 ) $html .='</optgroup>';
                   			$t_label = $item->label;
                   			$html .= '<optgroup label="'.$item->label.'">';

                   		}
	      				$html .= '<option value="'.$item->id.'" '.$selected.'>'.$item->text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div>';
	  	$js   = '<script>
        			var func_'.$id_elemen.' = function() {
									  					$("#id_'.$id_elemen.'").select2();
									  				};
					s_func.push(func_'.$id_elemen.');
    			</script>
	  			<!-- akhir form select '.$id_elemen.' -->';
	  	return $html.$js;
	}

	function input_image($attr)
	{
		// vdump($attr);
		//$default_value = isset($attr['value'])&&($attr['value']!='') ? $attr['value'] : 'assets/third_party/images/nophoto.jpg';
		if(!isset($attr['view_file'])){
			$attr['view_file'] = true;
		}
		//Menambah fasilitas disabled pada satu input_image
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
				 <label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
				 <div class="col-sm-8">
				 	<div id="div_'.$attr['name'].'" class="col-lg-6">';
		if($attr['view_file']==true){

				$html .= '<div class="row">';
				$count = 0;

				// echo $attr['value'][0]->path_foto;
				if(isset($attr['value'][0]->path_foto))
				{
					$foto_utama = $attr['value'][0];
					$attr['value'][0]->path_foto = 'uploads/foto_kapal/'.$attr['value'][0]->path_foto;

				}else{
					$foto_utama = (object)array( 'path_foto'=>'assets/third_party/images/nophoto.jpg');
				}
				/*vdump(array( (object)array( 'path_foto'=>'assets/third_party/images/nophoto.jpg')));
				vdump($foto_utama);
				vdump($attr['value'][0]);*/

				$html .= '<div class="col-lg-6">';
					$html .= '<a href="#" class="thumbnail" ><img src="'.base_url($foto_utama->path_foto).'"
				    		  id="id_img_'.$foto_utama->path_foto.'"
				    		  name="img_'.$foto_utama->path_foto.'"
				    		  data-src="holder.js/100%x180"
				    			></a>';
				    $html .= '</div>';

				   if($attr['value']!==NULL){
					$html .= '<div class="col-lg-6">';

					foreach($attr['value'] as $data ){

						if($count != 0){
							$html .= '<a href="#" class="thumbnail" ><img src="'.base_url('uploads/foto_kapal/'.$data->path_foto).'"
						    		  id="id_img_'.$data->path_foto.'"
						    		  name="img_'.$data->path_foto.'"
						    		  data-src="holder.js/100%x180"
						    			></a>';
						} 
						$count++;
					}
			    $html .= '</div>';
				}

				$html .= '	</div>';

		}

		/*else{
			if($default_value == 'assets/third_party/images/nophoto.jpg'){
				$default_value = '';
			}
			if(strpos($default_value, 'foto_kapal'))
			{
				
				$value = explode("/foto_kapal/", $default_value);
				$default_value = $value[1];

			}elseif (strpos($default_value, 'file_gross')) 
			{
				
				$value = explode("/file_gross/", $default_value);
				$default_value = $value[1];

			}

			$html .= $default_value;
		}		    			*/
		
		$html .=		'<input id="id_'.$attr['name'].'" 
		      				name="'.$attr['name'].'"
		      				type="file"
		      				class="form-control"
		      				'.$disabled.' >
		    		</div>
		    		</div>
	  			 </div><!-- akhir form text '.$attr['name'].' -->';
	  	return $html;
	}

	function dupe_form($attr)
	{
		$html  = '<div class="form-group mkform-select">
				  <div class="clearfix"></div>
			      <label for="'.$attr['name'].'" class="col-sm-3 control-label"></label>
			      ';
		$html .= '<div id="add_upload_gambar" class="col-sm-3"><a class="btn glyphicon glyphicon-plus" style="font-size: 1em;"></a>Tambah Foto</div><div class="clearfix"></div><label for="'.$attr['name'].'" class="col-sm-3 control-label"></label><div id="dupe_form_gambar" class="col-sm-3" ></div>';
		$html .='<script>
	                    var count = 0;
	                    var id_field = "'.$attr['name'].'";
	                $("#add_upload_gambar").click(function(){
	                    $("#id_"+id_field).clone().attr({id:"id_"+id_field+"_"+count,name:id_field+"_"+count}).appendTo("#dupe_form_gambar");
	                    count++;
                	})
	              </script>
	              <div class="clearfix"></div>';
	    $html .= '</div>';
	    return $html;
	}

	function one_row($attr)
	{

		//$default_value = isset($attr['value']) ? $attr['value'] : '';
		//$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		//Menambah fasilitas disabled pada satu input_text
		$disabled = '';
		if(isset($attr['disabled']))
		{

			$disabled = $attr['disabled']==TRUE ? 'disabled' : '';

		}

		$html = '
				<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="row">';
	    foreach ($attr['arr'] as $data) {
	    	# code...
	    	$default_value = isset($data['value']) ? $data['value'] : '';
			$placeholder_text = isset($data['placeholder']) ? $data['placeholder'] : '';
			if(isset($attr['tipe'])){
				if($data['tipe'] === 'select'){
					$is_enable = isset($data['enable']) ? $data['enable'] : 'true';
					
					$html 	.='<div class="col-sm-1">
			      				<select id="id_'.$data['name'].'" name="'.$data['name'].'" class="form-control"
				      				value="'.$default_value.'" ';
				    $html .= ' '.$disabled.' >';
		      			foreach ($data['opsi'] as $value => $text) {
		      				$selected = (String)$default_value == $value ? 'selected' : ''; 
		      				$html .= '<option value="'.$value.'" '.$selected.' >'.$text.'</option>';
		      			}
					$html .= '</select>
				    		</div>';
				    if($is_enable == "false")
				  	{
				  	$html .= '<script>
			         			$("#id_'.$data['name'].'").prop("disabled", true).removeAttr("class").css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
			         		   </script>';
			        }
		  					
				}else if($data['tipe'] === 'text'){
					$html 	.='<div class="col-sm-1">
			      				<input id="id_'.$data['name'].'" 
				      				name="'.$data['name'].'"
				      				type="'.$data['tipe'].'"
				      				class="form-control"
				      				placeholder="'.$placeholder_text.'"
				      				value="'.$default_value.'" 
				      				'.$disabled.' >
			      			</div>';
				}else{
			    	$html 	.='<div class="col-sm-1">
			      				<input id="id_'.$data['name'].'" 
				      				name="'.$data['name'].'"
				      				type="text"
				      				class="form-control"
				      				placeholder="'.$placeholder_text.'"
				      				value="'.$default_value.'" 
				      				'.$disabled.' >
			      			</div>';
				}
			}else{
				$html 	.='<div class="col-sm-1">
			      				<input id="id_'.$data['name'].'" 
				      				name="'.$data['name'].'"
				      				type="text"
				      				class="form-control"
				      				placeholder="'.$placeholder_text.'"
				      				value="'.$default_value.'" 
				      				'.$disabled.' >
			      			</div>';
	      		$html .= (next($attr)!=null)? '<div class="col-sm-1 x">x</div>' : '';
			}
	    }
	    $html 	.='</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';

	 	return $html;
	}

	function dropdown_propkota_kode($attr)
	{
		$get_opsi_prov = Modules::run('tables/kapi_list_all', 'mst_propinsi', 'nama_propinsi');
		$html = '<div class="form-group">';
		$html .= $attr['label_class'] !== "none" ? '<label for="propinsi" class="'.$attr['label_class'].'">Kabupaten / Kota</label>' : '';
		$html .= '<div class="'.$attr['input_width'].'">
		             <select id="propinsi" name="prop_kab_kota" >';
		foreach($get_opsi_prov as $data_provinsi){
			$html .= "<optgroup label='$data_provinsi->nama_propinsi' >";

			$get_opsi_kab = Modules::run('tables/kapi_list_all_by_id', 'mst_kabupaten_kota', $data_provinsi->id_propinsi);
			foreach ($get_opsi_kab as $key) {
				$html .= "<option value='".$data_provinsi->id_propinsi."|".$key->id_kabupaten_kota."|".$data_provinsi->kode."|".$key->kode."|".$data_provinsi->nama_propinsi."|".$key->nama_kabupaten_kota." '>
						$key->nama_kabupaten_kota</option>";
				//echo $data_provinsi->nama_propinsi.'|'.$key->nama_kabupaten_kota."<br/>";
			}
			$html .= "</optgroup>";
		}
		$html .= '</select>
		           </div>
		      </div>';
		$html .= '<script>

						function hasil(state){
							opt = $("select").find(":selected");
					        sel = opt.text();
					        og = opt.closest("optgroup").attr("label");
							return state.text+", "+og;
						}

				        $(document).ready(function() { 
				        	$("#propinsi").select2({
								width:"100%",
								formatSelection: hasil
								}); 
							});
				  </script>';

		echo $html;
	}

	function dropdown_kabkota_prop($attr)
	{
		$is_enable = isset($attr['enable']) ? $attr['enable'] : 'true';
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$get_opsi_prov = Modules::run('tables/kapi_list_all', 'mst_propinsi', 'nama_propinsi');
		$html = '<div class="form-group">';
		$html .= $attr['label_class'] !== "none" ? '<label for="propinsi" class="'.$attr['label_class'].'">'.$attr['label_text'].'</label>' : '';
		$html .= '<div class="'.$attr['input_width'].'">
		             <select id="propinsi" name="kabkota_prop" >';
		foreach($get_opsi_prov as $data_provinsi){
			$html .= "<optgroup label='$data_provinsi->nama_propinsi' >";

			$get_opsi_kab = Modules::run('tables/kapi_list_all_by_id', 'mst_kabupaten_kota', $data_provinsi->id_propinsi);
			foreach ($get_opsi_kab as $key) {

				$selected = $default_value === $data_provinsi->id_propinsi.'|'.$key->id_kabupaten_kota.'|'.$data_provinsi->nama_propinsi.'|'.$key->nama_kabupaten_kota? ' selected ' : ''; 
				// $selected = $default_value === "17|275|Bali|Kab. Jembrana" ? 'selected' : 'xxxx'; 
				$html .= "<option value='".$data_provinsi->id_propinsi."|".$key->id_kabupaten_kota."|".$data_provinsi->nama_propinsi."|".$key->nama_kabupaten_kota."' ".$selected." >
						$key->nama_kabupaten_kota</option>";
				//echo $data_provinsi->nama_propinsi.'|'.$key->nama_kabupaten_kota."<br/>";
			}
			$html .= "</optgroup>";
		}
		$html .= '</select>
		           </div>
		      </div>';
		$html .= '<script>

						function hasil(state){
							opt = $("select").find(":selected");
					        sel = opt.text();
					        og = opt.closest("optgroup").attr("label");
							return state.text+", "+og;
						}

				        $(document).ready(function() { 
				        	$("#propinsi").select2({
								width:"100%",
								formatSelection: hasil
								}); 
							});
				  </script>';
		if($is_enable == "false")
         {
         $html .= '<script>
         			$("#propinsi").prop("disabled", true).removeAttr("class").css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
         		   </script>';
          }
		echo $html;
	}

	function input_file($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				type="file"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	function column_chart($attr)
	{
		$html = '
				<script type="text/javascript">
					  window.onload = function () {
					    var chart = new CanvasJS.Chart("chartContainer",
					    {
					      title:{
					        text: "'.$attr['chart_title'].'"    
					      },
					      animationEnabled: true,
					      axisY: {
					        title: "Reserves(MMbbl)"
					      },
					      legend: {
					        verticalAlign: "bottom",
					        horizontalAlign: "center"
					      },
					      theme: "theme2",
					      data: [

					      {        
					        type: "column",  
					        showInLegend: true, 
					        legendMarkerColor: "grey",
					        legendText: "MMbbl = one million barrels",
					        dataPoints: [      
					        {y: 297571, label: "Venezuela"},
					        {y: 267017,  label: "Saudi" },
					        {y: 175200,  label: "Canada"},
					        {y: 154580,  label: "Iran"},
					        {y: 116000,  label: "Russia"},
					        {y: 97800, label: "UAE"},
					        {y: 20682,  label: "US"},        
					        {y: 20350,  label: "China"}        
					        ]
					      }   
					      ]
					    });

					    chart.render();
					  }
					  </script>
				<div id="chartContainer" style="height: 300px; width: 100%;">
    			</div>
			';
		return $html;
	}
}

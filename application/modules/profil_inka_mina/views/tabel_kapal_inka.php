<?php
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	// print_r(Modules::run('laporan/mst_filter/list_propinsi_array'));
	$attr_provinsi = array( 'name' => 'provinsi',
                                  'label' => 'Provinsi',
                                  'opsi' => Modules::run('refdss/propinsi/list_propinsi'),
                                  'all' => TRUE,
                                  'value' => (isset($id_propinsi))? $id_propinsi :''
                    );
          echo $this->mkform->input_select2($attr_provinsi);
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Filter</button>
              <button type="submit" class="btn btn-default" name="export">Export to Excel</button>
            </div>
          </div>
  </div>
</div>

<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='tabel_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	// echo "makannnn".$hasil;
	$counter = 1;
	if($data_kapal){
		foreach ($data_kapal as $item) {

			$link_edit = '<a class="btn btn-warning" style="width=120" href="'.base_url('profil_inka_mina/kapal/form_entry_kapal/'.$item->id_kapal).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" style="width=120" href="'.base_url('profil_inka_mina/kapal/delete_kapal_inka/'.$item->id_kapal).'">Hapus</a>';
			$link_view = '<a class="btn btn-primary" style="width=120" href="'.base_url('profil_inka_mina/kapal/detail_kapal_inka/'.$item->id_kapal).'">View</a>';

			$this->table->add_row(
								$item->nama_propinsi,
								$item->nama_kabupaten_kota,
								$item->nama_kapal,
								$item->gt,
								$item->tanda_selar,
								$item->siup,
								$item->sipi,
								$link_view,
								$link_edit." ".$link_delete
								);
			$counter++;
		}
	}

	$tabel_mapping_kapal = $this->table->generate();
?>
<div class="row">
	<div class="col-lg-3" style="margin-bottom:50px">
		<!-- <a class="btn btn-info"  href="<?php echo base_url('kapal/kapal/mapping_kapal_baru'); ?>">Klik Untuk Melakukan Mapping</a> -->
<!-- 		Jumlah Kapal: <?php echo $jumlah_kapal->jumlah_kapal?></br>
		Tidak Terdaftar DSS: <?php echo $jumlah_kapal->bukan_dss?></br>
		Tidak Terdaftar Pendaftaran Kapal: <?php echo $jumlah_kapal->bukan_kapi?></br> -->

	</div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $tabel_mapping_kapal;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#tabel_kapal').dataTable();
	} );
</script>
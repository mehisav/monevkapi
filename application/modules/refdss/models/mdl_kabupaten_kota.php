<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kabupaten_kota extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }

	public function list_kabupaten_kota()
    {
        $query = 'SELECT mp.nama_propinsi, mk.nama_kabupaten_kota, jp.jumlah_pelabuhan
                    FROM mst_kabupaten_kota AS mk
                    LEFT JOIN (
                        SELECT mpb.id_kabupaten_kota, COUNT(mpb.id_pelabuhan) AS "jumlah_pelabuhan"
                        FROM mst_pelabuhan AS mpb
                        WHERE mpb.aktif = "YA"
                        GROUP BY mpb.id_kabupaten_kota
                        ) AS jp ON jp.id_kabupaten_kota = mk.id_kabupaten_kota
                    LEFT JOIN mst_propinsi AS mp ON mp.id_propinsi = mk.id_propinsi
                    WHERE mp.aktif = "YA" AND mk.aktif = "YA"
        ';
    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_kaprop()
    {
        $this->db_dss->select('id_kabupaten_kota AS id,
                                nama_propinsi AS label,
                                nama_kabupaten_kota AS text');
        $this->db_dss->from('mst_kabupaten_kota');
        $this->db_dss->join('mst_propinsi', 'mst_kabupaten_kota.id_propinsi = mst_propinsi.id_propinsi');
        $this->db_dss->order_by('mst_kabupaten_kota.id_propinsi');
        $run_query = $this->db_dss->get();   
        // $str = $this->db_dss->last_query();
        // echo $str; die;
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
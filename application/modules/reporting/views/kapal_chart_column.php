<?php
	$this->load->helper('html');
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	// print_r(Modules::run('laporan/mst_filter/list_propinsi_array'));
	$attr_dasar_kapal = array( 'name' => $form['dasar_kapal']['name'],
                                         'label' => $form['dasar_kapal']['label'],
                                         'opsi' => array(
                                                          'gt' => 'GT', 
                                                          'propinsi' => 'Propinsi',
                                                          'bahan_kapal' => 'Bahan Kapal',
                                                          'alat_tangkap' => 'Alat Tangkap'
                                                        ),
                                         'value' => (isset($data_dasar)? $data_dasar : 0)
                    );
          echo $this->mkform->input_select($attr_dasar_kapal); 
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Filter</button>
              <!-- <button type="submit" class="btn btn-primary" name="export">Export</button> -->
            </div>
          </div>
  </div>
</div>
<?php 
if($chart==true)
{
?>
	<script type="text/javascript">
	  window.onload = function () {
	    var chart = new CanvasJS.Chart("chartContainer",
	    {
	      title:{
	        text: "<?php echo $chart_title?>"
	      },
	      animationEnabled: true,
	      axisY: {
	        title: "Jumlah Kapal"
	      },
	      axisX:{
	      	labelAngle: -60
	      },
	      legend: {
	        verticalAlign: "bottom",
	        horizontalAlign: "center"
	      },
	      theme: "theme2",
	      data: [

	      {        
	        type: "column",  
	        showInLegend: true, 
	        legendMarkerColor: "grey",
	        legendText: "Id/Jenis",
	        dataPoints: [
	        <?php 
	        $i = 0;
			$len = count($chart_isi);
	       	foreach ($chart_isi AS $item) {
	        ?>     
	        	{y: <?php echo $item->jml_kapal;?>, indexLabel:"<?php echo $item->jml_kapal;?>", label: "<?php echo $item->value?>"}<?php if($i!=$len-1) echo ",";?>      
	        <?php 
	        	$i++;
	    	}
	        ?>     
	        ]
	      }   
	      ]
	    });

	    chart.render();
	  }
	  </script>
	<div id="chartContainer" style="height: 300px; width: 100%;">
	</div>
	<div class="col-md-3 pull-right" >
		<a href="report_kapal/word_report_kapal" id="report_word" class="btn btn-primary" >Export to Word</a>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<?php echo ($data_dasar=='alat_tangkap')?"<th>Id</th>":"";?>
				<th>Jenis</th>
				<th>Jumlah Kapal</th>
			</tr>
		</thead>
		<tbody>
			
			<?php
				foreach ($chart_isi as $data_table) {
					echo "<tr>";
					echo "<th>".$data_table->value."</th>";
					echo ($data_dasar=='alat_tangkap')?"<th>".$data_table->nama."</th>":"";

					echo "<td align='center' >".$data_table->jml_kapal."</td>";
					echo "</tr>";
				}
			?>
			
		</tbody>
	</table>
	<script type="text/javascript">
		
	</script>
<?php
}
?>
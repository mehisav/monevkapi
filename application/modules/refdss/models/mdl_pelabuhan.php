<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_pelabuhan extends CI_Model
{
    private $db_monev;

    function __construct()
    {
        parent::__construct();
    }

    public function list_opsi()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $query = "SELECT id_pelabuhan AS id, nama_pelabuhan as text FROM mst_pelabuhan WHERE aktif='Ya'";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
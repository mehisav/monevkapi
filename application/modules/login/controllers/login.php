<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -  
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    private $id_aplikasi_kapi = "4";

    function __construct()
    {
            parent::__construct();
            $this->load->config('menus');
            $this->load->config('db_timeline');
            $this->load->config('globals');
    }


    public function login_v()
    {

        $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
        $add_css = array('select2.css', 'jquery.dataTables.css');

        $template = 'templates/page/v_form';
        $modules = 'dev';
        $views = 'v_login';
        $labels = 'form_login';
        $data['submit_form'] = 'dev/login';
        
        echo Modules::run($template, //tipe template
                            $modules, //nama module
                            $views, //nama file view
                            $labels, //dari labels.php
                            $add_js, //plugin javascript khusus untuk page yang akan di load
                            $add_css, //file css khusus untuk page yang akan di load
                            $data); //array data yang akan digunakan di file view
    }

    public function login()
    {

        $array_input = $this->input->post(NULL, TRUE);


        $temp = explode("|", $array_input['prop_kab_kota']);

        // vdump($array_input);

        $id_lokasi = $array_input['kategori_pengguna'];
        $id_propinsi = $temp[0];
        $id_kabupaten_kota = $temp[1];
        
        // vdump($temp, true);


        $this->batam_user($id_lokasi, $id_propinsi, $id_kabupaten_kota);

    }

    public function batam_user($id_lokasi, $id_propinsi, $id_kabupaten_kota){
        
        $this->load->model('mdl_dev_progress');
        $nama_prop_kabkota = $this->mdl_dev_progress->get_nama_prop_kabkota($id_propinsi, $id_kabupaten_kota);
        $data['nama_propinsi'] = $nama_prop_kabkota[0]->nama_propinsi;
        $data['nama_kabkota'] = $nama_prop_kabkota[0]->nama_kabupaten_kota;

        // vdump($data, true);


        switch ($id_lokasi) {
            case '1':
                $user_data = array (
                                    'username' => "PUSAT",
                                    'kode_pengguna' => 'PUSAT',
                                    'id_pengguna' => 111,
                                    'is_super_admin' => 0,
                                    'is_admin' => 0,
                                    'nama_pengguna' => 'PUSAT',
                                    'id_propinsi' => $id_propinsi,
                                    'id_kabupaten_kota' => $id_kabupaten_kota,
                                    'id_divisi' => 1,
                                    'id_lokasi' =>  1, //pusat
                                    'id_grup_pengguna' => 2,
                                    'fullname' => 'PUSAT',
                                    'email' => '',
                                    'app_access' => 0,
                                    'arr_access' => 0,
                                    'logged_in' => TRUE,
                                    'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
                                    'id_aplikasi' => '72,6,9,4,13,75',
                                    'firstpage' => 'administrator/masterpengguna_views',
                                    'data_otoritas_array' => array(
                                                    0 => array(
                                                        'id_aplikasi_otoritas'      => "4",
                                                        'id_grup_otoritas'      => "8",
                                                        'id_pelabuhan_otoritas'     => ""
                                                    ),
                                                    1 => array(
                                                        'id_aplikasi_otoritas'      => "75",
                                                        'id_grup_otoritas'      => "8",
                                                        'id_pelabuhan_otoritas'     => "1291"
                                                    )
                                                )
                                );
                    $this->session->set_userdata($user_data);
                break;
            
            case '2':
                $user_data = array (
                                    'username' => $data['nama_propinsi'],
                                    'kode_pengguna' => $data['nama_propinsi'],
                                    'id_pengguna' => 222,
                                    'is_super_admin' => 0,
                                    'is_admin' => 0,
                                    'nama_pengguna' => $data['nama_propinsi'],
                                    'id_propinsi' => $id_propinsi,
                                    'id_kabupaten_kota' => $id_kabupaten_kota,
                                    'id_divisi' => 1,
                                    'id_lokasi' =>  2, //propinsi
                                    'id_grup_pengguna' => 2,
                                    'fullname' => $data['nama_propinsi'],
                                    'email' => '',
                                    'app_access' => 0,
                                    'arr_access' => 0,
                                    'logged_in' => TRUE,
                                    'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
                                    'id_aplikasi' => '72,6,9,4,13,75',
                                    'firstpage' => 'administrator/masterpengguna_views',
                                    'data_otoritas_array' => array(
                                                    0 => array(
                                                        'id_aplikasi_otoritas'      => "4",
                                                        'id_grup_otoritas'      => "8",
                                                        'id_pelabuhan_otoritas'     => ""
                                                    ),
                                                    1 => array(
                                                        'id_aplikasi_otoritas'      => "75",
                                                        'id_grup_otoritas'      => "8",
                                                        'id_pelabuhan_otoritas'     => "1291"
                                                    )
                                                )
                                );
                    $this->session->set_userdata($user_data);
                break;

            case '3':
                $user_data = array (
                                    'username' => $data['nama_kabkota'],
                                    'kode_pengguna' => $data['nama_kabkota'],
                                    'nama_pengguna' => $data['nama_kabkota'],
                                    'id_pengguna' => 333,
                                    'id_propinsi' => $id_propinsi,
                                    'id_kabupaten_kota' => $id_kabupaten_kota,
                                    'id_divisi' => 1,
                                    'id_lokasi' =>  3, //kabupaten
                                    'is_super_admin' => 0,
                                    'is_admin' => 0,
                                    'id_grup_pengguna' => 2,
                                    'fullname' => $data['nama_kabkota'],
                                    'email' => '',
                                    'app_access' => 0,
                                    'arr_access' => 0,
                                    'logged_in' => TRUE,
                                    'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
                                    'id_aplikasi' => '72,6,9,4,13,75',
                                    'firstpage' => 'administrator/masterpengguna_views',
                                    'data_otoritas_array' => array(
                                                    0 => array(
                                                        'id_aplikasi_otoritas'      => "4",
                                                        'id_grup_otoritas'      => "8",
                                                        'id_pelabuhan_otoritas'     => ""
                                                    ),
                                                    1 => array(
                                                        'id_aplikasi_otoritas'      => "75",
                                                        'id_grup_otoritas'      => "8",
                                                        'id_pelabuhan_otoritas'     => "1291"
                                                    )
                                                )
                                );
                    $this->session->set_userdata($user_data);
                break;
        }
        // vdump($this->session->all_userdata(), true);
        redirect('dev/index');
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gross_akte extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_gross_akte');
		}

	public function index()
	{
		$this->gross_akte();

	}



	public function gross_akte( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter != '' ) $data['filter'] = $filter;
		$data['data_kapal'] = $this->mdl_gross_akte->data_tanpa_gross();
		$data['tanngal_update'] = $this->mdl_gross_akte->data_update('gross_akte');
		// $data['last_update'] = $this->mdl_produksi->last_pipp();
		$template = 'templates/page/v_form';
		$modules = 'monitoring';
		$views = 'gross_akte';
		$labels = 'tabel_gross_akte';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}
    
    public function update_gross_akte()
    {
        $this->mdl_gross_akte->update_gross_akte();

        $url = base_url('monitoring/gross_akte/gross_akte');
        redirect($url);
    }

    public function export_kapal($filter = '')
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		$template = FCPATH.'assets\kapi\format\format_kapal.xlsx';
		// $template = dirname(__FILE__).'/'.$_GET['kode'].'.xlsx';
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSetting = array('memoryCacheSize' => '100MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSetting);

		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($template);
		$objWorksheet = $objPHPExcel->getSheet(0);

		/*content*/
		$data = $this->mdl_gross_akte->daftar_kapal_gross_akte();
		$column = Array(
					"A","B","C","D","E","F","G","H","I","J","K","L","M",
					"N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
					"AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM",
					"AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
					"BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM",
					"BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ",
					"CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM",
					"CN","CO","CP","CQ","CR","CS","CT","CU","CV","CW","CX","CY","CZ",
					"DA","DB","DC","DD","DE","DF","DG","DH","DI","DJ","DK","DL","DM",
					"DN","DO","DP","DQ","DR","DS","DT","DU","DV","DW","DX","DY","DZ",
					"EA","EB","EC","ED","EE","EF","EG","EH","EI","EJ","EK","EL","EM",
					"EN","EO","EP","EQ","ER","ES","ET","EU","EV","EW","EX","EY","EZ",
					"FA","FB","FC","FD","FE","FF","FG","FH","FI","FJ","FK","FL","FM",
					"FN","FO","FP","FQ","FR","FS","FT","FU","FV","FW","FX","FY","FZ",
					"GA","GB","GC","GD","GE","GF","GG","GH","GI","GJ","GK","GL","GM",
					"GN","GO","GP","GQ","GR","GS","GT","GU","GV","GW","GX","GY","GZ",
					"HA","HB","HC","HD","HE","HF","HG","HH","HI","HJ","HK","HL","HM",
					"HN","HO","HP","HQ","HR","HS","HT","HU","HV","HW","HX","HY","HZ",
					"IA","IB","IC","ID","IE","IF","IG","IH","II","IJ","IK","IL","IM",
					"IN","IO","IP","IQ","IR","IS","IT","IU","IV","IW","IX","IY","IZ"
					);
		$rowNumber = 6;
		foreach ($data as $item)
		{
			$col=1;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber, ($rowNumber-5));
			foreach($item as $key => $cell) {
				// $objPHPExcel->getActiveSheet()->setCellValue($column[$col].$rowNumber, "A");
				$objPHPExcel->getActiveSheet()->setCellValue($column[$col].$rowNumber, $cell);
				$col++;
			}

			$rowNumber++;
		}
		/*End Content*/
		// die();
		//Border
		$styleArray = array(
		    'borders' => array(
		      'allborders' => array(
		          'style' => PHPExcel_Style_Border::BORDER_THIN
		      )
		    )
		);
		$objPHPExcel->getActiveSheet()->getStyle('A6:AM'.($rowNumber-1))->applyFromArray($styleArray);
		// Save as an Excel BIFF (xls) file 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename=" Data Master Kapal - '.date("d-m-Y h:i").'.xlsx"'); 
		header('Cache-Control: max-age=0'); 

		$objWriter->save('php://output'); 

	}
}
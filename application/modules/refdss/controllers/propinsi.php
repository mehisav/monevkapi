<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Propinsi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_propinsi');
		}

	public function index()
	{
		$this->propinsi();

	}

	public function propinsi( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_propinsi';
		$data['list_propinsi'] = $this->mdl_propinsi->list_propinsi();
		
		echo Modules::run('templates/page/v_form', 'refdss', 'tabel_propinsi', $labels, $add_js, $add_css, $data);

	}

	public function list_propinsi()
    {

        $list_opsi = $this->mdl_propinsi->list_opsi();

        return $list_opsi;
    }

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_produksi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_report_produksi');
		}

	public function index()
	{
		$this->report_produksi();

	}

	public function report_produksi( $filter = '' )
	{
		$array_input = $this->input->post(NULL, TRUE);
		$data_dasar = isset($array_input['dasar_tipe'])?  $array_input['dasar_tipe'] : "0";
		$data_waktu = isset($array_input['waktu'])?  $array_input['waktu'] : "0";

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'report_data_produksi';
		$data['submit_form'] = "reporting/report_produksi";
		$bulan = array('Januari', 'Februari', 'Maret', 'April','Mei','Juni','Juli', 'Agustus','September','Oktober','November','Desember');
		if (isset($array_input['propinsi'])==false) 
		{
			$data['chart'] = false;
		}
		/*else if($data_waktu==='1')
		{
			$data['data_dasar'] = $data_dasar;
			$data['propinsi'] = $array_input['propinsi'];
			$data['waktu'] = 1;
			$data['chart'] = true;
			$data['chart_title'] = "Data Perkembangan Produksi Kapal Per Trip";
			$data['chart_isi'] = $this->mdl_report_produksi->jumlah_produksi($array_input['propinsi'], $data_dasar);
		}*/
		else if($data_waktu==='2')
		{
			$data['data_dasar'] = $data_dasar;
			$data['propinsi'] = $array_input['propinsi'];
			$data['waktu'] = 2;
			$data['chart'] = true;
			$data['chart_title'] = "Data Perkembangan Produksi Kapal Per Bulan";
			$data['chart_isi'] = $this->mdl_report_produksi->jumlah_produksi_bulan($array_input['propinsi'], $data_dasar);
			$data['data'] = array();
			$head = null;
			$array_child = array();
			$jumlah_data = count($data['chart_isi']);
			$i = 0;
			$array_default = array(
								array(
									'label'	=> 'Januari',
									'y'		=> 0
									),
								array(
									'label'	=> 'Februari',
									'y'		=> 0
									),
								array(
									'label'	=> 'Maret',
									'y'		=> 0
									),
								array(
									'label'	=> 'April',
									'y'		=> 0
									),
								array(
									'label'	=> 'Mei',
									'y'		=> 0
									),
								array(
									'label'	=> 'Juni',
									'y'		=> 0
									),
								array(
									'label'	=> 'Juli',
									'y'		=> 0
									),
								array(
									'label'	=> 'Agustus',
									'y'		=> 0
									),
								array(
									'label'	=> 'September',
									'y'		=> 0
									),
								array(
									'label'	=> 'Oktober',
									'y'		=> 0
									),
								array(
									'label'	=> 'November',
									'y'		=> 0
									),
								array(
									'label'	=> 'Desember',
									'y'		=> 0
									)
							);
			foreach ($data['chart_isi'] as $key => $value) {
				# code...
				if($head != $value->id_kapal){
					if(!is_null($head))
					{
						$result = array_replace($array_default, $array_child);
						array_push($data['data'], array('type'=> "line", "dataPoints" => $result, 'legendText' => $value->nama_kapal, "showInLegend" => true));
						$array_child = array();
						$array_default = array(
								array(
									'label'	=> 'Januari',
									'y'		=> 0
									),
								array(
									'label'	=> 'Februari',
									'y'		=> 0
									),
								array(
									'label'	=> 'Maret',
									'y'		=> 0
									),
								array(
									'label'	=> 'April',
									'y'		=> 0
									),
								array(
									'label'	=> 'Mei',
									'y'		=> 0
									),
								array(
									'label'	=> 'Juni',
									'y'		=> 0
									),
								array(
									'label'	=> 'Juli',
									'y'		=> 0
									),
								array(
									'label'	=> 'Agustus',
									'y'		=> 0
									),
								array(
									'label'	=> 'September',
									'y'		=> 0
									),
								array(
									'label'	=> 'Oktober',
									'y'		=> 0
									),
								array(
									'label'	=> 'November',
									'y'		=> 0
									),
								array(
									'label'	=> 'Desember',
									'y'		=> 0
									)
							);
					}
					$head = $value->id_kapal;
					// array_push($array_child, array('x'=> $value->bulan.$value->tahun,'y'=>$value->nilai));
				}
				$i++;
				$array_child[(int)$value->bulan-1] = array( 'label'=>$bulan[$value->bulan-1],'y'=> (int)$value->nilai);

				if($i == $jumlah_data)
				{
					$result = array_replace($array_default, $array_child);
					array_push($data['data'], array('type'=> "line", "dataPoints" => $result, 'legendText' => $value->nama_kapal, "showInLegend" => true));
				}
			}
			$data['data'] = json_encode($data['data']);
			// echo "<pre>";
			// print_r(json_encode($data['data']));
			// echo "</pre>";

		}else if($data_waktu==='3')
		{
			$data['data_dasar'] = $data_dasar;
			$data['propinsi'] = $array_input['propinsi'];
			$data['waktu'] = 3;
			$data['chart'] = true;
			$data['chart_title'] = "Data Perkembangan Produksi Kapal Per Triwulan";
			$data['chart_isi'] = $this->mdl_report_produksi->jumlah_produksi_triwulan($array_input['propinsi'], $data_dasar);
			$data['data'] = array();
			$head = null;
			$array_child = array();
			$jumlah_data = count($data['chart_isi']);
			$i = 0;
			$array_default = array(
								array(
									'label'	=> 'Semester 1',
									'y'		=> 0
									),
								array(
									'label'	=> 'Semester 2',
									'y'		=> 0
									),
								array(
									'label'	=> 'Semester 3',
									'y'		=> 0
									),
								array(
									'label'	=> 'Semester 4',
									'y'		=> 0
									)
							);
			foreach ($data['chart_isi'] as $key => $value) {
				# code...
				if($head != $value->id_kapal){
					if(!is_null($head))
					{
						$result = array_replace($array_default, $array_child);
						array_push($data['data'], array('type'=> "line", "dataPoints" => $result, 'legendText' => $value->nama_kapal, "showInLegend" => true));
						$array_child = array();
						$array_default = array(
								array(
									'label'	=> 'Semester 1',
									'y'		=> 0
									),
								array(
									'label'	=> 'Semester 2',
									'y'		=> 0
									),
								array(
									'label'	=> 'Semester 3',
									'y'		=> 0
									),
								array(
									'label'	=> 'Semester 4',
									'y'		=> 0
									)
							);
					}
					$head = $value->id_kapal;
					// array_push($array_child, array('x'=> $value->bulan.$value->tahun,'y'=>$value->nilai));
				}
				$i++;
				$array_child[(int)$value->quarter-1] = array( 'label'=>(int)$value->quarter,'y'=> (int)$value->nilai);

				if($i == $jumlah_data)
				{
					$result = array_replace($array_default, $array_child);
					array_push($data['data'], array('type'=> "line", "dataPoints" => $result, 'legendText' => $value->nama_kapal, "showInLegend" => true));
				}
			}
			$data['data'] = json_encode($data['data']);

		}else if($data_waktu==='4')
		{
			$data['data_dasar'] = $data_dasar;
			$data['propinsi'] = $array_input['propinsi'];
			$data['waktu'] = 4;
			$data['chart'] = true;
			$data['chart_title'] = "Data Perkembangan Produksi Kapal Per Tahun";
			$data['chart_isi'] = $this->mdl_report_produksi->jumlah_produksi_tahun($array_input['propinsi'], $data_dasar);
			$data['data'] = array();
			$head = null;
			$array_child = array();
			$jumlah_data = count($data['chart_isi']);
			$i = 0;
			foreach ($data['chart_isi'] as $key => $value) {
				# code...
				if($head != $value->id_kapal){
					if(!is_null($head))
					{
						array_push($data['data'], array('type'=> "line", "dataPoints" => $array_child, 'legendText' => $value->nama_kapal, "showInLegend" => true));
						$array_child = array();
					}
					$head = $value->id_kapal;
					// array_push($array_child, array('x'=> $value->bulan.$value->tahun,'y'=>$value->nilai));
				}
				$i++;
				array_push($array_child, array('label'=> $value->tahun,'y'=> (int)$value->nilai));

				if($i == $jumlah_data)
				{
					array_push($data['data'], array('type'=> "line", "dataPoints" => $array_child, 'legendText' => $value->nama_kapal, "showInLegend" => true));
				}
			}
			$data['data'] = json_encode($data['data']);
		}

		echo Modules::run('templates/page/v_form', 'reporting', 'kapal_chart_produksi', $labels, $add_js, $add_css, $data);

	}

}
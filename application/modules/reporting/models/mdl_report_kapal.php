<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_report_kapal extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->load->database();
    }

	public function jumlah_kapal_gt()
    {
        $query = 'SELECT ">30" AS "value",
                        SUM(CASE WHEN dmk.gt > 30 THEN 1 ELSE 0 END) AS "jml_kapal"
                    FROM db_monev_kapi.mst_inka_mina AS dmk
                    WHERE aktif="Ya"
                     UNION all
                     SELECT "<=30" AS "value",
                        SUM(CASE WHEN dmk.gt <= 30 THEN 1 ELSE 0 END) AS "jml_kapal"
                    FROM db_monev_kapi.mst_inka_mina AS dmk
                    WHERE aktif="Ya"
            ';
    	$run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_kapal_propinsi()
    {
        $query = 'SELECT dsp.nama_propinsi AS "value", 
                        COUNT(*) AS "jml_kapal"
                    FROM db_monev_kapi.mst_inka_mina AS dmk
                    LEFT JOIN db_master.mst_kabupaten_kota AS dskk ON dskk.id_kabupaten_kota = dmk.id_kab_kota
                    LEFT JOIN db_master.mst_propinsi AS dsp ON dskk.id_propinsi = dsp.id_propinsi
                    WHERE dmk.aktif="Ya" AND dmk.id_kab_kota <> 0
                    GROUP BY dsp.id_propinsi
            ';
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_kapal_bahan_kapal()
    {
        $query = 'SELECT UPPER(dbk.nama_bahan_kapal) AS "value", 
                        COUNT(*) AS "jml_kapal"
                    FROM db_monev_kapi.mst_inka_mina AS dmk
                    RIGHT JOIN db_master.mst_bahan_kapal AS dbk ON dbk.id_bahan_kapal = dmk.id_bahan_kapal
                    WHERE dmk.aktif="Ya" 
                    GROUP BY dmk.id_bahan_kapal
            ';
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_kapal_alat_tangkap()
    {
        $query = 'SELECT dmk.id_alat_tangkap as value, 
                        CASE WHEN dmk.id_alat_tangkap = 0 THEN "Data Tidak Komplit" ELSE dat.nama_alat_tangkap END AS "nama", 
                        COUNT(*) AS "jml_kapal"
                    FROM db_monev_kapi.mst_inka_mina AS dmk
                    JOIN db_master.mst_alat_tangkap AS dat ON dat.id_alat_tangkap = dmk.id_alat_tangkap
                    WHERE dmk.aktif="Ya" 
                    GROUP BY dmk.id_alat_tangkap
            ';
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    
}
SETUP AWAL
=========

Aplikasi pendaftaran kapal KAPI.

### SETUP APLIKASI CODEIGNITER:
  - Clone via source tree ke folder dev_kapi_v2 di htdocs
  - Buat folder uploads/document di root folder dev_kapi_v2
  - buat folder assets/kapi/cs, assets/kapi/images/, assets/kapi/js
  - buat folder assets/third_party/
  - Extract semua zip file di folder misc/bahan_awal/
  - file assets_awal.zip mengandung tiga folder (css, js, images), Copy/paste tiga folder ini ke dalam folder assets/third_party/
  - extract file zip system.zip dan paste di root folder dev_kapi_v2
  - extract file zip config_awal.zip (config.php, database.php), Copy/Paste ke folder application/config/
  - buka application/config/config.php edit line 227 jadi:
    ```
    $config['encryption_key'] = 'djptittaskforce2012';
    ```
### SETUP DATABASE MYSQL:
  - Masuk ke phpmyadmin
  - Buat database kapi_pendaftaran
  - Buat database db_pendaftaran_kapal
  - Download file database dump : [kapi_pendaftaran_2013.sql.zip](https://www.dropbox.com/s/okule5pljq0ptzl/kapi_pendaftaran_2013.sql.zip)
  - Import file database dump kapi_pendaftaran_2013.sql.zip ke database kapi_pendaftaran dan db_pendaftaran_kapal yang baru dibuat
  - buka file application/config/database.php
  - Ubah sesuaikan konfigurasi database:
    ```
    $db['default']['username'] = '';
    $db['default']['password'] = '';
    $db['default']['database'] = 'db_pendaftaran_kapal';
    ```
  - sesuaikan dengan konfigurasi masing-masing
  
    > Untuk development akan digunakan database db_pendaftaran_kapal, dimana isinya masih sama dengan database lama, tapi bebas dimodifikasi oleh kita. Database kapi_pendaftaran bisa digunakan sebagai referensi dan tidak boleh diubah.


  

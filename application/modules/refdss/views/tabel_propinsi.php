<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_propinsi' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_propinsi){
		foreach ($list_propinsi as $item) {

			// $image_properties['src'] = 'uploads/'.$item->foto_ikan;
			// $image_properties['height'] = "150";
			// img($image_properties);

			$this->table->add_row(
								$counter.'.',
								$item->nama_propinsi,
								$item->jumlah_kabupaten,
								$item->jumlah_pelabuhan
								);
			$counter++;
		}
	}

	$table_propinsi = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_propinsi;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_propinsi').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                       
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>
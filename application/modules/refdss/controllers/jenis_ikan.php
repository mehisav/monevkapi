<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_ikan extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_jenis_ikan');
		}

	public function index()
	{
		$this->jenis_ikan();

	}

	public function jenis_ikan( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_jenis_ikan';
		$data['list_ikan'] = $this->mdl_jenis_ikan->list_jenis_ikan();
		
		echo Modules::run('templates/page/v_form', 'refdss', 'tabel_jenis_ikan', $labels, $add_js, $add_css, $data);

	}

	public function list_jenis_ikan_array()
    {

        $list_opsi = $this->mdl_jenis_ikan->list_opsi();

        return $list_opsi;
    }

}

<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='tabel_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	// echo "makannnn".$hasil;
	$counter = 1;
	if($data_kapal){
		foreach ($data_kapal as $item) {

			$link_edit = '<a class="btn btn-warning" style="width=120" href="'.base_url('profil_inka_mina/kapal/form_entry_kapal/'.$item->id_kapal).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" style="width=120" href="'.base_url('profil_inka_mina/kapal/delete_kapal_inka/'.$item->id_kapal).'">Hapus</a>';
			$link_view = '<a class="btn btn-primary" style="width=120" href="'.base_url('profil_inka_mina/kapal/detail_kapal_inka/'.$item->id_kapal).'">View</a>';

			$this->table->add_row(
								$item->nama_propinsi,
								$item->nama_kabupaten_kota,
								$item->nama_kapal,
								$item->gt,
								$item->tanda_selar,
								$item->siup,
								$item->sipi,
								$link_view
								);
			$counter++;
		}
	}

	$tabel_mapping_kapal = $this->table->generate();
?>
<div class="row">
	<div class="col-lg-3" style="margin-bottom:50px">
		<a class="btn btn-default"  href="<?php echo base_url('monitoring/oprasi/export_kapal/'.$filter); ?>">Export to Excel</a>
	</div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $tabel_mapping_kapal;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	document.getElementById('title').innerHTML = '<?PHP echo $title?>';
	$(document).ready( function () {
		$('#tabel_kapal').dataTable();
	} );
</script>
<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_dpi' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	// var_dump($list_dpi);die();
	if($list_dpi){
		foreach ($list_dpi as $item) {

			// $image_properties['src'] = 'uploads/'.$item->foto_ikan;
			// $image_properties['height'] = "150";
			// img($image_properties);

			$this->table->add_row(
								$counter.'.',
								$item->nama_wpp,
								$item->nama_dpi,
								$item->nama_dpi_inggris,
								$item->lintang,
								$item->bujur
								);
			$counter++;
		}
	}

	$table_dpi = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_dpi;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_dpi').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                       
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>
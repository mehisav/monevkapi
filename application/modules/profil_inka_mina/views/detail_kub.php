<style type="text/css">
    @media print {
        page_enter {page-break-after: always;}
    }
</style>
<script language="javascript">
    function printDiv(divName) {
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;

          document.body.innerHTML = printContents;

          window.print();

          document.body.innerHTML = originalContents;
    }

function myFunction() {
    document.getElementById("page_enter").style.pageBreakAfter = "always";
}
</script>
<FORM>
<INPUT TYPE="button" class="btn btn-default" value="Export to PDF" onClick="printDiv('muncul_print')">
</FORM>
<div id="muncul_print">
    <h3>Data KUB</h3>
    <!-- <div id="page_enter" style="page-break-after: always;"></div> -->
    <table class="tablelist" width="80%">
        <tr>
            <td  width="30%">ID KUB</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->id_kub))? $detail_kub->id_kub : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">ID Perusahaan  DSS</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->id_perusahaan))? $detail_kub->id_perusahaan : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Nama KUB</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nama_kub))? $detail_kub->nama_kub : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">No SIUP</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->no_siup))? $detail_kub->no_siup : ''; ?></td>
        </tr>
      <!--   <tr>
            <td  width="30%">Tanggal SIUP</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->tgl_siup))? $detail_kub->tgl_siup : ''; ?></td>
        </tr> -->
        <tr>
            <td  width="30%">Tahun Pembentukan</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->tahun_pembentukan))? $detail_kub->tahun_pembentukan : ''; ?></td>
        </tr>
       <!--  <tr>
            <td  width="30%">Tahun Pengukuhan</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->tahun_pengukuhan))? $detail_kub->tahun_pengukuhan : ''; ?></td>
        </tr> -->
        <tr>
            <td  width="30%">Sumber Anggaran</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->sumber_anggaran))? $detail_kub->sumber_anggaran : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Oleh</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->oleh))? $detail_kub->oleh : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">No Telepon </td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nomor))? $detail_kub->nomor : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Dana Simpanan</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->dana_simpan))? $detail_kub->dana_simpan : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Status Hukum Kelompok Nelayan</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->status_hukum))? $detail_kub->status_hukum : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Nomor Register</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nomor_register))? $detail_kub->nomor_register : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%"><b>ALAMAT</b></td>
            <td width="10px"></td>
            <td></td>
        </tr>
        <tr>
            <td  width="30%">Kabupaten/Kota</td>
            <td width="10px">:</td>
            <td><?php echo  (isset($detail_kub->id_kabupaten_kota))? $detail_kub->id_kabupaten_kota : '';?></td>
        </tr>
        <tr>
            <td  width="30%">Alamat Lengkap</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->alamat))? $detail_kub->alamat : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%"><b>PENGURUS</b></td>
            <td width="10px"></td>
            <td></td>
        </tr>
        <tr>
            <td  width="30%">Nama Ketua</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nama_ketua))? $detail_kub->nama_ketua : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Telepon Ketua</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->telp_ketua))? $detail_kub->telp_ketua : ''; ?></td>
        </tr>
       <!--  <tr>
            <td  width="30%">Nama Wakil</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nama_wakil))? $detail_kub->nama_wakil : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Telepon Wakil</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->telp_wakil))? $detail_kub->telp_wakil : ''; ?></td>
        </tr> -->
        <tr>
            <td  width="30%">Nama Sekretaris</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nama_sekretaris))? $detail_kub->nama_sekretaris : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Telepon Sekretaris</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->telp_sekretaris))? $detail_kub->telp_sekretaris : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Nama Bendahara</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->nama_bendahara))? $detail_kub->nama_bendahara : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%">Telepon Bendahara</td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->telp_bendahara))? $detail_kub->telp_bendahara : ''; ?></td>
        </tr>
        <tr>
            <td  width="30%"><b>Jumlah Anggota</b></td>
            <td width="10px">:</td>
            <td><?php echo (isset($detail_kub->anggota))? $detail_kub->anggota : ''; ?></td>
        </tr>
    </table>
</div>
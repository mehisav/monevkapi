<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelabuhan extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_pelabuhan_json()
	{
		$this->load->model('mdl_pelabuhan');

		$list_opsi = $this->mdl_pelabuhan->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_pelabuhan_array()
	{
		$this->load->model('mdl_pelabuhan');

		$list_opsi = $this->mdl_pelabuhan->list_opsi();

		return $list_opsi;
	}
	
}
?>
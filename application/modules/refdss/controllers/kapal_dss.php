<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kapal_dss extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_kapal_dss');
		}

	public function index()
	{
		$this->kapal();

	}

	public function kapal( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if(($filter === 'daerah')){
			$labels = 'label_view_kapal_daerah';
			$data['list_kapal'] = $this->mdl_kapal_dss->list_kapal_daerah();
		}else{
			$labels = 'label_view_kapal';
			$data['list_kapal'] = $this->mdl_kapal_dss->list_kapal();
		}
			echo Modules::run('templates/page/v_form', 'refdss', 'tabel_kapal_dss', $labels, $add_js, $add_css, $data);

	}

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alat_tangkap extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_alat_tangkap');
		}

	public function index()
	{
		$this->alat_tangkap();

	}

	public function alat_tangkap( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_alat_tangkap';
		$data['list_alat_tangkap'] = $this->mdl_alat_tangkap->list_alat_tangkap();
		
		echo Modules::run('templates/page/v_form', 'refdss', 'tabel_alat_tangkap', $labels, $add_js, $add_css, $data);

	}

	public function list_alat_tangkap()
	{
		$this->load->model('mdl_alat_tangkap');

		$list_opsi = $this->mdl_alat_tangkap->list_opsi();

		return $list_opsi;
	}

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dpi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_dpi');
		}

	public function index()
	{
		$this->dpi();

	}

	public function dpi( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		
		$labels = 'tabel_dpi';
		$data['list_dpi'] = $this->mdl_dpi->list_dpi();
		
		echo Modules::run('templates/page/v_form', 'refdss', 'tabel_dpi', $labels, $add_js, $add_css, $data);

	}

	public function list_dpi_array()
	{
		$this->load->model('mdl_dpi');

		$list_opsi = $this->mdl_dpi->list_opsi();

		return $list_opsi;
	}

}
<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_alat_tangkap' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_alat_tangkap){
		foreach ($list_alat_tangkap as $item) {

			// $image_properties['src'] = 'uploads/'.$item->foto_ikan;
			// $image_properties['height'] = "150";
			// img($image_properties);

			$this->table->add_row(
								$counter.'.',
								$item->kode_alat_tangkap,
								$item->nama_alat_tangkap,
								$item->nama_alat_tangkap_inggris,
								$item->jenis_alat_tangkap
								// '<img src="'.base_url().$item->foto_ikan.'" alt="Smiley face" height="42" width="42">'
								);
			$counter++;
		}
	}

	$table_alat_tangkap = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_alat_tangkap;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_alat_tangkap').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"}
                       
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>
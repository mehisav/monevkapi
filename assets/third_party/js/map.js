var geocoder;
var map;
var link = "";
function c_link(a){
	link = a;
}
function initialize() {
    map = new google.maps.Map(
    document.getElementById("map_canvas"), {
        center: new google.maps.LatLng(0.2,116.250487),
        zoom: 5,
		scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var url_locations = link;
	$.ajax({
		url:url_locations,
		dataType:'json',
		cache:'true',
		success:function(data_peta_sebaran){
			data_peta_sebaran.forEach(function(d){
				var lat = 0,
				lang = 0;
				label = "";
				if(d.provinsi == "Bali")
				{
					lat = -8.369843;
					lang =115.216706;
				}
				if(d.provinsi == "Banten")
				{
					lat = -6.504392;
					lang = 106.097324;
				}
				if(d.provinsi == "Bengkulu")
				{
					lat = -3.585785;
					lang = 102.299641;
				}
				if(d.provinsi == "DKI Jakarta")
				{
					lat = -6.176160;
					lang = 106.824711;
				}
				if(d.provinsi == "Gorontalo")
				{
					lat = 0.705327;
					lang = 122.450153;
				}
				if(d.provinsi == "Jambi")
				{
					lat = -1.503065;
					lang = 102.441456;
				}
				if(d.provinsi == "Jawa Barat")
				{
					lat = -6.974185;
					lang = 107.682042;
				}
				if(d.provinsi == "Jawa Tengah")
				{
					lat = -7.161380;
					lang = 110.082297;
				}
				if(d.provinsi == "Kalimantan Barat")
				{
					lat = -0.277660;
					lang = 111.418671;
				}
				if(d.provinsi == "Kalimantan Selatan")
				{
					lat = -3.087521;
					lang = 115.269636;
				}
				if(d.provinsi == "Kalimantan Tengah")
				{
					lat = -1.587494;
					lang = 113.224525;
				}
				if(d.provinsi == "Kalimantan Timur")
				{
					lat = 0.543516;
					lang = 116.540944;
				}
				if(d.provinsi == "Kep. Bangka Belitung")
				{
					lat = -2.687029;
					lang = 106.500303;
				}
				if(d.provinsi == "Kepulauan Riau")
				{
					lat = 3.944160;
					lang = 108.152776;
				}
				if(d.provinsi == "Lampung")
				{
					lat = -4.549303;
					lang = 105.337009;
				}
				if(d.provinsi == "Maluku")
				{
					lat = -3.237071;
					lang = 130.091854;
				}
				if(d.provinsi == "Maluku Utara")
				{
					lat = 1.559039;
					lang = 127.768428;
				}
				if(d.provinsi == "Nanggroe Aceh Darussalam")
				{
					lat = 4.545307;
					lang = 96.913163;
				}
				if(d.provinsi == "Nusa Tenggara Barat")
				{
					lat = -8.669664;
					lang = 117.350223;
				}
				if(d.provinsi == "Nusa Tenggara Timur")
				{
					lat = -8.675847;
					lang = 121.115020;
				}if(d.provinsi == "Papua")
				{
					lat = -4.227894;
					lang = 138.204577;
				}if(d.provinsi == "Papua Barat")
				{
					lat = -1.355249;
					lang = 132.960132;
				}if(d.provinsi == "Riau")
				{
					lat = 0.432178;
					lang = 101.655217;
				}if(d.provinsi == "Sulawesi Barat")
				{
					lat = -2.860517;
					lang = 119.188823;
				}if(d.provinsi == "Sulawesi Selatan")
				{
					lat = -3.682200;
					lang = 119.878815;
				}if(d.provinsi == "Sulawesi Tengah")
				{
					lat = -1.500740;
					lang = 121.463168;
				}if(d.provinsi == "Sulawesi Tenggara")
				{
					lat = -4.158595;
					lang = 122.229772;
				}if(d.provinsi == "Sulawesi Utara")
				{
					lat = 1.570457;
					lang = 124.885077;

				}if(d.provinsi == "Sumatera Barat")
				{
					lat = -0.769777;
					lang = 101.028097;

				}if(d.provinsi == "Sumatera Selatan")
				{
					lat = -3.328670;
					lang = 103.797880;
				}if(d.provinsi == "Sumatera Utara")
				{
					lat = 1.895118;
					lang = 98.859304;
				}if(d.provinsi == "Yogyakarta")
				{
					lat = -7.799360;
					lang = 110.362055;
				}

				var marker = new MarkerWithLabel({
			        position: new google.maps.LatLng(lat, lang),
			        // icon: mapStyles.uavSymbolBlack,
			        labelContent: '<b>' + d.provinsi + '</b><div style="text-align: center;">'+d.jumlah_kapal+' Unit</div>',
			        // labelAnchor: new google.maps.Point(95, 20),
			        labelClass: "labels",
			        labelStyle: {
			            opacity: 0.75
			        },
			        zIndex: 999999,
			        map: map
			    });

			});
		    
		}
	});
}


//Gas
// var gas_markers = null;

/*function gas() {
    if (gas_markers === null) {
        document.getElementById('gas').style.backgroundColor = "rgb(175,175,175)";
        document.getElementById('gas').style.borderColor = "black";
        document.getElementById('gas').style.color = "rgb(75,75,75)";

        var request = {
            location: map.getCenter(),
            radius: 3500,
            keyword: ["gas"]
        };
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, callback);


        function callback(results, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (gas_markers === null) gas_markers = [];
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }
            }
        }

        function createMarker(place) {
            var placeLoc = place.geometry.location;
            var gas_marker = new MarkerWithLabel({
                position: place.geometry.location,
                draggable: false,
                raiseOnDrag: false,
                map: map,
                // icon: "images/gas1.png",
                labelContent: "",
                labelAnchor: new google.maps.Point(10, 0),
                labelClass: "pin", // the CSS class for the label
                labelStyle: {
                    opacity: 0.95
                }
            });
            gas_markers.push(gas_marker);
            var infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(gas_markers, 'click', function () {
                infowindow.setContent('Promo Code: < br > Gas');
                infowindow.open(map, this);
            });
        }
        // gas_markers = 'one';

    } else {

        clearMarkers();
        document.getElementById('gas').style.backgroundColor = "rgb(75,75,75)";
        document.getElementById('gas').style.borderColor = "gray";
        document.getElementById('gas').style.color = "rgb(175,175,175)";

        gas_markers = null;

    }

    function clearMarkers() {

        for (var i = 0; i < gas_markers.length; i++) {
            gas_markers[i].setMap(null);
        }
        gas_markers = [];
    }

}*/
//Gas - end
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kapal extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        parent::__construct();
        // $this->load->database();
    }

     public function list_opsi()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = 'SELECT id_kapal AS id, nama_kapal as `text` FROM mst_inka_mina WHERE aktif = "Ya"';

        $run_query = $this->db_monev->query($query);                   
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {  
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->insert('mst_inka_mina', $data);
        if( $this->db_monev->affected_rows() > 0)
        {
            $result = true;
        }else
        {
            $result = false;
        }
        return $result;
    }

   public function kapal_dss($id)
    {
        $this->load->database();
        $query = "SELECT mk.*, 
                        mi.no_sipi, 
                        mi.id_dpi, 
                        mi.id_pelabuhan_pangkalan, 
                        mi.tanggal_sipi AS 'tgl_sipi', 
                        mi.tanggal_akhir_sipi AS 'tgl_akhir_sipi', 
                        mkub.id_kub
                FROM db_master.mst_kapal AS mk
                LEFT JOIN db_master.mst_izin as mi ON mi.id_kapal = mk.id_kapal AND mi.aktif = 'Ya'
                LEFT JOIN db_monev_kapi.mst_kub as mkub ON mkub.id_perusahaan = mk.id_perusahaan AND mkub.aktif = 'Ya'
                WHERE mk.id_kapal = $id AND mk.aktif = 'Ya'";
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input_dss($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $data_dss = $this->kapal_dss($id);
        // vdump($kapal_dss);die();
        $data = array();
        $data['nama_kapal'] = $data_dss->nama_kapal;
        $id_dpi = str_replace(' ', '', $data_dss->id_dpi);
        $data['id_dpi'] = $id_dpi;
        $data['id_kub'] = (isset($data_dss->id_kub))? $data_dss->id_kub : '';
        $data['id_transmitter_vms'] = (isset($data_dss->id_transmitter_vms))? $data_dss->id_transmitter_vms : '';
        $data['tanda_selar'] = $data_dss->tanda_selar;
        $data['gross_akte'] = $data_dss->nomor_gross_akte;
        $data['sipi'] = $data_dss->no_sipi;
        $data['tanggal_sipi'] = $data_dss->tgl_sipi ;
        $data['tanggal_akhir_sipi'] = $data_dss->tgl_akhir_sipi;
        $id_pelabuhan_pangkalan = str_replace(' ', '', $data_dss->id_pelabuhan_pangkalan);
        $data['id_pelabuhan_pangkalan'] = $id_pelabuhan_pangkalan;
        $data['id_alat_tangkap'] = $data_dss->id_alat_tangkap;
        $data['panjang_kapal'] = $data_dss->panjang_kapal;
        $data['lebar_kapal'] = $data_dss->lebar_kapal;
        $data['dalam_kapal'] = $data_dss->dalam_kapal;
        $data['daya'] = $data_dss->daya_kapal;
        $data['gt'] = $data_dss->gt_kapal;
        $data['mesin'] = $data_dss->merek_mesin;
        $data['no_mesin'] = $data_dss->no_mesin;
        $data['lokasi_pembangunan'] = $data_dss->tempat_pembangunan;
        $data['id_bahan_kapal'] = $data_dss->id_bahan_kapal;
        $data['id_dss'] = $data_dss->id_kapal;

        $this->db_monev->insert('mst_inka_mina',$data);                        
        if( $this->db_monev->affected_rows() > 0)
        {
            $result = true;
        }else
        {
            $result = false;
        }
        return $result;
    }

	public function data_kapal_dss()
    {
        $this->load->database();
        $query = 'SELECT  dmm.*,
                            mkub.nama_kub,
                            mdpi.nama_dpi,
                            mi.id_dpi, 
                            mi.no_sipi, 
                            mi.tanggal_sipi AS "tgl_sipi", 
                            mi.tanggal_akhir_sipi AS "tgl_akhir_sipi",
                            dmk.id_kapal as "kapal_inka",
                            mp.nama_pelabuhan,
                            mat.nama_alat_tangkap,
                            mbk.nama_bahan_kapal
                FROM    db_master.mst_kapal as dmm
                LEFT JOIN db_monev_kapi.mst_inka_mina as dmk ON dmk.id_dss=dmm.id_kapal AND dmk.aktif = "Ya"
                LEFT JOIN db_master.mst_izin as mi ON mi.id_kapal = dmm.id_kapal AND mi.aktif = "Ya"
                LEFT JOIN (SELECT mk.id_kapal, 
                                    group_concat(mdpi.nama_dpi separator ", ") AS nama_dpi
                            FROM db_master.mst_kapal as mk
                            LEFT JOIN db_master.mst_izin as mi ON mi.id_kapal = mk.id_kapal AND mi.aktif = "Ya"
                            LEFT JOIN db_master.mst_dpi as mdpi 
                                ON find_in_set(mdpi.id_dpi, mi.id_dpi)
                            GROUP BY mk.id_kapal
                         ) mdpi ON mdpi.id_kapal = dmm.id_kapal
                LEFT JOIN (SELECT mk.id_kapal, 
                                    group_concat(mdpi.nama_pelabuhan separator ", ") AS nama_pelabuhan
                            FROM db_master.mst_kapal as mk
                            LEFT JOIN db_master.mst_izin as mi ON mi.id_kapal = mk.id_kapal AND mi.aktif = "Ya"
                            LEFT JOIN db_master.mst_pelabuhan as mdpi 
                                ON find_in_set(mdpi.id_pelabuhan, mi.id_pelabuhan_pangkalan)
                            GROUP BY mk.id_kapal
                         ) mp ON mp.id_kapal = dmm.id_kapal
                LEFT JOIN db_monev_kapi.mst_kub as mkub ON mkub.id_perusahaan = dmm.id_perusahaan AND mkub.aktif = "Ya"
                LEFT JOIN db_master.mst_alat_tangkap as mat ON mat.id_alat_tangkap = dmm.id_alat_tangkap
                LEFT JOIN db_master.mst_bahan_kapal as mbk ON mbk.id_bahan_kapal = dmm.id_bahan_kapal
                WHERE dmm.aktif = "YA" AND dmk.id_dss IS NULL
                    AND ((dmm.nama_kapal LIKE "%INKA%" AND dmm.nama_kapal LIKE "%MINA%")
                    OR (dmm.nama_kapal LIKE "%MINA%" AND dmm.nama_kapal LIKE "%MARITIM%"))
                ';
    	$run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_kapal_inka($id_propinsi)
    {
        $this->load->database();
        $a = ($id_propinsi == 0 )? '' : 'AND mp.id_propinsi = "'.$id_propinsi.'"';
        $query = 'SELECT    dmm.id_kapal as "id_kapal",
                            dmm.nama_kapal as "nama_kapal",
                            dmm.gt as "gt",
                            dmm.tanda_selar as "tanda_selar",
                            dmm.sipi,
                            dmm.siup,
                            mkk.nama_kabupaten_kota,
                            mp.nama_propinsi
                FROM db_monev_kapi.mst_inka_mina as dmm
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON dmm.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                WHERE dmm.aktif = "YA" '.$a.'
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function daftar_kapal($id_propinsi)
    {
        $this->load->database();
        $a = ($id_propinsi == 0 )? '' : 'AND mp.id_propinsi = "'.$id_propinsi.'"';
        $query = 'SELECT mp.nama_propinsi,
                            mkk.nama_kabupaten_kota,
                            mim.nama_kapal as "nama_kapal",
                            mim.tanda_selar,
                            mim.gross_akte,
                            mim.sipi,
                            mim.tanggal_akhir_sipi,
                            mpb.nama_pelabuhan,
                            mat.nama_alat_tangkap,
                            mim.gt,
                            CONCat("P=", mim.panjang_kapal, ", L=", mim.lebar_kapal, ", D=" ,mim.dalam_kapal ),
                            mim.daya,
                            mim.mesin,
                            mim.no_mesin,
                            mim.kontraktor_pembangunan,
                            mim.lokasi_pembangunan,
                            mim.tahun_pembuatan,
                            mbk.nama_bahan_kapal,
                            kmp.masalah,
                            mk.nama_kub,
                            mk.no_siup,
                            mk.tahun_pembentukan,
                            mk.sumber_anggaran,
                            mk.oleh,
                            mk.no_telp,
                            mk.alamat,
                            mk.nama_ketua,
                            mk.telp_ketua,
                            mk.nama_sekretaris,
                            mk.nama_bendahara,
                            mk.anggota,
                            tp.jml_produksi,
                            tp.jml_opreasi,
                            tp.avg_hari_operasi,
                            tp.avg_produksi,
                            tp.avg_bbm,
                            tp.avg_operasional,
                            tp.avg_pendapatan,
                            mim.keterangan
                FROM db_monev_kapi.mst_inka_mina as mim
                LEFT JOIN db_monev_kapi.mst_kub as mk ON mk.id_kub = mim.id_kub
                LEFT JOIN db_master.mst_kabupaten_kota as mkk ON mim.id_kab_kota = mkk.id_kabupaten_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                LEFT JOIN db_monev_kapi.mst_permasalahan as kmp ON kmp.id = mim.id_permasalahan
                LEFT JOIN db_master.mst_alat_tangkap as mat ON mat.id_alat_tangkap = mim.id_alat_tangkap
                LEFT JOIN db_master.mst_bahan_kapal as mbk ON mbk.id_bahan_kapal = mim.id_bahan_kapal
                LEFT JOIN (SELECT id_kapal, SUM(jml_ikan) AS jml_produksi, 
                                    COUNT(*) AS jml_opreasi,  
                                    AVG(datediff(tgl_keluar,tgl_masuk)) AS avg_hari_operasi,
                                    AVG(jml_ikan) AS avg_produksi,
                                    AVG(kebutuhan_bbm) AS avg_bbm,
                                    AVG(biaya_operasional) AS avg_operasional,
                                    AVG(nilai_pendapatan-biaya_operasional) AS avg_pendapatan
                                FROM trs_produksi 
                                GROUP BY id_kapal) tp ON tp.id_kapal = mim.id_kapal
                LEFT JOIN (SELECT mim.id_kapal, 
                                    group_concat(nama_pelabuhan separator ", ") AS nama_pelabuhan
                            FROM db_monev_kapi.mst_inka_mina as mim
                            LEFT JOIN db_master.mst_pelabuhan as mpb 
                                ON find_in_set(mpb.id_pelabuhan, mim.id_pelabuhan_pangkalan)
                            GROUP BY mim.id_kapal
                         ) mpb ON mpb.id_kapal = mim.id_kapal
                WHERE mim.aktif = "YA" '.$a.'
                ';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $sql = " UPDATE mst_inka_mina SET aktif='Tidak' WHERE id_kapal=$id ";
        
        $query = $this->db_monev->query($sql);
    }

    public function detail_kapal_inka($id)
    {
        $this->load->database();
        $query = "SELECT mim.id_kapal,
                        mim.id_kub,
                        mim.id_dss,
                        mim.id_permasalahan,
                        mim.id_dpi,
                        mdpi.nama_dpi,
                        mim.id_transmitter_vms,
                        kmp.masalah,
                        mim.keterangan,
                        mim.nama_kapal,
                        mim.tahun_pembuatan,
                        mim.tanda_selar,
                        mim.gt,
                        mim.daya,
                        mim.panjang_kapal,
                        mim.lebar_kapal,
                        mim.dalam_kapal,
                        mim.no_mesin,
                        mim.mesin,
                        mim.gross_akte,
                        mim.siup,
                        mim.sipi,
                        mim.tanggal_sipi,
                        mim.tanggal_akhir_sipi,
                        mim.kontraktor_pembangunan,
                        mim.lokasi_pembangunan,
                        mim.id_kab_kota as kab_kota,
                        mim.id_bahan_kapal as bahan_kapal,
                        mim.id_alat_tangkap as jenis_alat_tangkap,
                        mim.id_pelabuhan_pangkalan,
                        mim.pelabuhan_pangkalan,
                        mim.foto_oprasional,
                        mim.foto_sea_trial,
                        mim.foto_pembuatan,
                        mim.foto_siup1,
                        mim.foto_siup2,
                        mim.foto_bast1,
                        mim.foto_bast2,
                        mkab.nama_kabupaten_kota,
                        mp.nama_propinsi,
                        mat.nama_alat_tangkap,
                        mbk.nama_bahan_kapal,
                        mpb.nama_pelabuhan
                FROM db_monev_kapi.mst_inka_mina as mim
                LEFT JOIN db_monev_kapi.mst_permasalahan as kmp ON kmp.id = mim.id_permasalahan
                LEFT JOIN db_master.mst_kabupaten_kota as mkab ON mkab.id_kabupaten_kota = mim.id_kab_kota
                LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkab.id_propinsi
                LEFT JOIN db_master.mst_alat_tangkap as mat ON mat.id_alat_tangkap = mim.id_alat_tangkap
                LEFT JOIN db_master.mst_bahan_kapal as mbk ON mbk.id_bahan_kapal = mim.id_bahan_kapal
                LEFT JOIN (SELECT mim.id_kapal, 
                                    group_concat(nama_pelabuhan separator ', ') AS nama_pelabuhan
                            FROM db_monev_kapi.mst_inka_mina as mim
                            LEFT JOIN db_master.mst_pelabuhan as mpb 
                                ON find_in_set(mpb.id_pelabuhan, mim.id_pelabuhan_pangkalan)
                            GROUP BY mim.id_kapal
                         ) mpb ON mpb.id_kapal = mim.id_kapal
                LEFT JOIN (SELECT mim.id_kapal, 
                                    group_concat(nama_dpi separator ', ') AS nama_dpi
                            FROM db_monev_kapi.mst_inka_mina as mim
                            LEFT JOIN db_master.mst_dpi as mdpi 
                                ON find_in_set(mdpi.id_dpi, mim.id_dpi)
                            GROUP BY mim.id_kapal
                         ) mdpi ON mdpi.id_kapal = mim.id_kapal
                WHERE mim.id_kapal = $id";
        $run_query = $this->db->query($query);                           

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_wpp_kapal($id_dpi)
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $query = 'SELECT mw.nama_wpp
                    FROM mst_dpi as md
                    LEFT JOIN mst_wpp as mw ON mw.id_wpp = md.id_wpp
                    WHERE md.id_dpi IN ('.$id_dpi.')
                ';
        $run_query = $this->db_dss->query($query);     

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update($data)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $this->db_monev->where('id_kapal', $data['id_kapal']);
        $query = $this->db_monev->update('mst_inka_mina',$data);

        if($this->db_monev->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_kub($id)
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT mk.*
                FROM mst_kub AS mk
                WHERE mk.id_kub = $id";
        $run_query = $this->db_monev->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_produksi($id)
    {
        $this->load->database();
        $query = 'SELECT tp.id_produksi,
                            mim.nama_kapal,
                            mw.nama_wpp,
                            md.nama_dpi,
                            mim.gt,
                            tp.tgl_keluar,
                            tp.tgl_masuk,
                            mpbk.nama_pelabuhan as nama_pelabuhan_keluar,
                            mpbm.nama_pelabuhan as nama_pelabuhan_masuk,
                            tp.jml_ikan,
                            tp.nilai_pendapatan,
                            mji.id_jenis_ikan,
                            tp.kebutuhan_bbm,
                            tp.biaya_operasional,
                            tp.nama_nahkoda,
                            tp.jml_abk
                    FROM db_monev_kapi.trs_produksi as tp
                    INNER JOIN db_monev_kapi.mst_inka_mina as mim ON mim.id_kapal = tp.id_kapal
                    LEFT JOIN db_master.mst_pelabuhan as mpbk ON mpbk.id_pelabuhan = tp.id_pelabuhan_keluar
                    LEFT JOIN db_master.mst_pelabuhan as mpbm ON mpbm.id_pelabuhan = tp.id_pelabuhan_masuk
                    LEFT JOIN db_master.mst_dpi as md ON md.id_dpi = tp.id_dpi
                    LEFT JOIN db_master.mst_wpp as mw ON mw.id_wpp = md.id_wpp
                    LEFT JOIN (SELECT tp.id_produksi, 
                                        group_concat(nama_jenis_ikan separator ", ") AS id_jenis_ikan
                                FROM db_monev_kapi.trs_produksi as tp
                                LEFT JOIN db_master.mst_jenis_ikan as mji 
                                    ON find_in_set(mji.id_jenis_ikan, tp.id_jenis_ikan)
                                GROUP BY tp.id_produksi) mji
                            ON mji.id_produksi = tp.id_produksi
                    WHERE tp.aktif = "Ya" AND tp.id_kapal = "'.$id.'"
                    ORDER BY tgl_masuk DESC';
        $run_query = $this->db->query($query);                           
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_permasalahan()
    {
        $this->db_monev = $this->load->database('default', TRUE);
        $query = "SELECT id AS id, masalah as text FROM mst_permasalahan ORDER BY urut";

        $run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}
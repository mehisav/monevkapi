<?php
  echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>
<div class="row">
  <div class="col-md-6">
    <?php
      echo Modules::run('pengguna_kapi/remote/input_form');


      $attr_hidden_id = array( 
                              'name'    =>'id_pengguna_kapi',
                              'label'   => 'id_pengguna_kapi',
                              'value'   => kos($detail_pengguna_kapi['id_pengguna_kapi'])
                          );
      echo $this->mkform->input_hidden($attr_hidden_id);

      if($submit_form === 'pengguna_kapi/main/update' ){

        $attr_hidden_id = array( 
                                'name'    =>'id_pengguna',
                                'label'   => 'id_pengguna',
                                'value'   => kos($detail_pengguna_kapi['id_pengguna'])
                          );
        echo $this->mkform->input_hidden($attr_hidden_id);

        // vdump($detail_pengguna_kapi, true);
      }

      $attr_kategori_pengguna = array( 
                      'name'    => $form['kategori_pengguna']['name'],
                      'label'   => $form['kategori_pengguna']['label'],
                      'opsi'    => array(
                          'PUSAT' => 'PUSAT', 
                          'PROPINSI' => 'PROPINSI',
                          'KAB/KOTA' => 'KAB/KOTA',),
                      'value'   => kos($detail_pengguna_kapi['kategori_pengguna']),
                      'OnChange' => 'cek_kategori(this.value)'
                          );
       echo $this->mkform->input_select($attr_kategori_pengguna);

    ?>
      
  </div>

  <div class="col-md-6"> 
          <?php

          echo "<div id='area_pusat' style='margin-bottom:15px; display:block;' >";

          echo "</div>";

          //AREA FORM KABKOTA
          echo "<div id='area_kab_kota' style='margin-bottom:15px; display:none;' >";
          $attr_prop_kota  = array( 
                                'input_id' => 'id_propinsi',
                                'input_id_kota' => 'id_kabupaten_kota',
                                'input_name' => 'id_propinsi',
                                'input_name_kota' => 'id_kabupaten_kota',
                                'label_text' => 'Propinsi',
                                'label_text_kota' => 'Kabupaten/Kota',
                                'array_opsi' => '', 
                                'opsi_selected' => kos($detail_pengguna_kapi["id_propinsi"]),  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-3 manual_input control-label'
                          );
          echo $this->mkform->dropdown_propkota_kode($attr_prop_kota);
          echo "</div>";

          ?>
        </div>
</div>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>

<script>
  
    function cek_kategori(jenis_kategori)
    {
      if(jenis_kategori==='PUSAT')
      {
        document.getElementById("area_pusat").style.display = "block";
        // document.getElementById("area_propinsi").style.display = "none";
        document.getElementById("area_kab_kota").style.display = "none";

      }
      else if(jenis_kategori==='PROPINSI')
      {
        document.getElementById("area_kab_kota").style.display = "block";
        document.getElementById("area_pusat").style.display = "none";
        // document.getElementById("area_propinsi").style.display = "none";
      }
      else if(jenis_kategori==='KAB/KOTA')
      {
        document.getElementById("area_kab_kota").style.display = "block";
        document.getElementById("area_pusat").style.display = "none";
        // document.getElementById("area_propinsi").style.display = "none";
      }
    }

</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dev_progress extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
	}
	
	public function index()
	{
		$this->load->model('mdl_dev');
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'buku_kapal';
		$views = 'view_table_progres';
		$labels = 'label_view_table_index';

		// $data['list_progress'] = $this->mdl_dev->get_list_progress();

		//echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
		echo Modules::run($template, //tipe template
							'dev', //nama module
							$views, //nama file view
							$labels, //dari labels.php
							'', //plugin javascript khusus untuk page yang akan di load
							'', //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function counter()
	{
		$this->load->model('mdl_dev_counter');

		$table = $this->mdl_dev_counter->get_table();

		$items = array();

		foreach ($table as $key => $row) {
			$item = array('id_lokasi'=> $row->id_kabupaten_kota,
						  'tipe_lokasi' => '3',
						  'nama_lokasi' => $row->nama_kabupaten_kota,
						  'counter' => 0);
			array_push($items, $item);
		}

		$this->mdl_dev_counter->insert_into($items);

		echo "ok";


	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
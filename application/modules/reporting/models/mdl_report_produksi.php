<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_report_produksi extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->load->database();
    }

	public function jumlah_produksi($id_kapal, $tipe)
    {
        $s_tipe = ($tipe==1)? 'jml_ikan':'pendapatan_bersih';
        $query = 'SELECT  SUM(dtp.jml_ikan) AS "nilai", MONTH(dtp.tgl_keluar) as bulan, YEAR(dtp.tgl_keluar) as tahun
                    FROM db_monev_kapi.trs_produksi AS dtp
                    LEFT JOIN db_monev_kapi.mst_inka_mina as mim ON mim.id_kapal = dtp.id_kapal
                    LEFT JOIN db_master.mst_kabupaten_kota as mkk ON mkk.id_kabupaten_kota = mim.id_kab_kota
                    LEFT JOIN db_master.mst_propinsi as mp ON mp.id_propinsi = mkk.id_propinsi
                    WHERE dtp.aktif="Ya" AND mp.id_propinsi = "'.$id_kapal.'"
                    GROUP BY MONTH(dtp.tgl_keluar), YEAR(dtp.tgl_keluar), mp.id_propinsi
                    ORDER BY dtp.tgl_keluar
            ';
    	$run_query = $this->db->query($query);                          
        // echo $this->db->last_query();
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_produksi_triwulan($id_propinsi, $tipe)
    {
        $s_tipe = ($tipe=1)? 'jml_ikan':'pendapatan_bersih';
        $query = "SELECT 
                        SUM(dtp.jml_ikan) AS nilai,
                        dtp.id_kapal,
                        mim.nama_kapal,
                        QUARTER(dtp.tgl_keluar) as quarter,
                        YEAR(dtp.tgl_keluar) AS tahun
                    FROM
                        db_monev_kapi.trs_produksi AS dtp
                            LEFT JOIN
                        db_monev_kapi.mst_inka_mina AS mim ON mim.id_kapal = dtp.id_kapal
                            LEFT JOIN
                        db_master.mst_kabupaten_kota AS mkk ON mkk.id_kabupaten_kota = mim.id_kab_kota
                            LEFT JOIN
                        db_master.mst_propinsi AS mp ON mp.id_propinsi = mkk.id_propinsi
                    WHERE
                        dtp.aktif = 'Ya'
                            AND mp.id_propinsi = ".$id_propinsi."
                    GROUP BY  YEAR(dtp.tgl_keluar) , QUARTER(dtp.tgl_keluar),dtp.id_kapal
                    ORDER BY dtp.id_kapal, YEAR(dtp.tgl_keluar), QUARTER(dtp.tgl_keluar)
            ";
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_produksi_bulan($id_propinsi, $tipe)
    {
        $s_tipe = ($tipe=1)? 'jml_ikan':'pendapatan_bersih';
        $query = "SELECT 
                        SUM(dtp.jml_ikan) AS nilai,
                        dtp.id_kapal,
                        mim.nama_kapal,
                        MONTH(dtp.tgl_keluar) AS bulan,
                        YEAR(dtp.tgl_keluar) AS tahun
                    FROM
                        db_monev_kapi.trs_produksi AS dtp
                            LEFT JOIN
                        db_monev_kapi.mst_inka_mina AS mim ON mim.id_kapal = dtp.id_kapal
                            LEFT JOIN
                        db_master.mst_kabupaten_kota AS mkk ON mkk.id_kabupaten_kota = mim.id_kab_kota
                            LEFT JOIN
                        db_master.mst_propinsi AS mp ON mp.id_propinsi = mkk.id_propinsi
                    WHERE
                        dtp.aktif = 'Ya'
                            AND mp.id_propinsi = ".$id_propinsi."
                    GROUP BY MONTH(dtp.tgl_keluar) , YEAR(dtp.tgl_keluar) , dtp.id_kapal
                    ORDER BY dtp.id_kapal, dtp.tgl_keluar
            ";
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function jumlah_produksi_tahun($id_propinsi, $tipe)
    {
        $s_tipe = ($tipe=1)? 'jml_ikan':'pendapatan_bersih';
        $query = "SELECT 
                        SUM(dtp.jml_ikan) AS nilai,
                        dtp.id_kapal,
                        mim.nama_kapal,
                        YEAR(dtp.tgl_keluar) AS tahun
                    FROM
                        db_monev_kapi.trs_produksi AS dtp
                            LEFT JOIN
                        db_monev_kapi.mst_inka_mina AS mim ON mim.id_kapal = dtp.id_kapal
                            LEFT JOIN
                        db_master.mst_kabupaten_kota AS mkk ON mkk.id_kabupaten_kota = mim.id_kab_kota
                            LEFT JOIN
                        db_master.mst_propinsi AS mp ON mp.id_propinsi = mkk.id_propinsi
                    WHERE
                        dtp.aktif = 'Ya'
                            AND mp.id_propinsi = ".$id_propinsi."
                    GROUP BY YEAR(dtp.tgl_keluar) , dtp.id_kapal
                    ORDER BY dtp.id_kapal, dtp.tgl_keluar
            ";
        $run_query = $this->db->query($query);                          
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    public function detail_list_produksi($id)
    {
        $query = "SELECT `mst_dpi`.`id_wpp`, `nama_wpp`, 
                        `gt`, `nama_kapal`, 
                        `tgl_keluar` as tgl_berangkat, DATEDIFF(`tgl_masuk`, `tgl_keluar`) as jml_hari_operasi, 
                        `jml_ikan`, `nilai_pendapatan`, 
                        `nama_jenis_ikan`, `kebutuhan_bbm`, 
                        `biaya_operasional`, nilai_pendapatan-biaya_operasional as pendapatan_bersih,
                        `jml_abk` as jumlah_abk
                    FROM db_monev_kapi.trs_produksi as trs_produksi
                    LEFT JOIN db_monev_kapi.mst_inka_mina as mst_inka_mina 
                        ON `mst_inka_mina`.`id_kapal` = `trs_produksi`.`id_kapal` 
                    LEFT JOIN (SELECT id_produksi, group_concat(nama_jenis_ikan separator ', ') AS nama_jenis_ikan 
                                FROM db_monev_kapi.trs_produksi as trs_produksi 
                                LEFT JOIN db_master.mst_jenis_ikan as mst_jenis_ikan
                                    ON find_in_set(mst_jenis_ikan.id_jenis_ikan, trs_produksi.id_jenis_ikan) 
                                GROUP BY id_produksi) jenis_ikan 
                        ON `jenis_ikan`.`id_produksi` = `trs_produksi`.`id_produksi` 
                    LEFT JOIN db_master.mst_dpi as mst_dpi 
                        ON `mst_dpi`.`id_dpi` = `trs_produksi`.`id_dpi` 
                    LEFT JOIN db_master.mst_wpp as mst_wpp
                        ON `mst_wpp`.`id_wpp` = `mst_dpi`.`id_wpp` 
                    WHERE `trs_produksi`.`id_kapal` = '".$id."'
                ";
        $run_query = $this->db->query($query);
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = FALSE;
        }
        return $result;
    }
}
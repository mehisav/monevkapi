<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produksi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_produksi');
		}

	public function index()
	{
		$this->produksi();

	}

	public function produksi( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if($filter != '' ) $data['filter'] = $filter;
		$data['list_produksi'] = $this->mdl_produksi->list_produksi();
		$data['last_update'] = $this->mdl_produksi->last_pipp();
		$template = 'templates/page/v_form';
		$modules = 'profil_inka_mina';
		$views = 'tabel_produksi';
		$labels = 'tabel_produksi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function ambil_data( $filter = '' )
	{
		if( $this->mdl_produksi->update_pipp() ){
			$url = base_url('profil_inka_mina/produksi');
			redirect($url);
		}else{
			$url = base_url('profil_inka_mina/produksi');
			redirect($url);
		}
	}

	public function form_entry_produksi($filter = '')
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['submit_form'] = 'profil_inka_mina/produksi/input';
		if($filter != '' )
		{
			$id_kapal = $filter;
			$data['detail_produksi'] = $this->mdl_produksi->detail_produksi($id_kapal);
			$data['submit_form'] = 'profil_inka_mina/produksi/input/update';
		}
		$data['data_jenis_ikan'] = json_encode($this->mdl_produksi->select2());
		// $data['data_kapal_dss'] = $this->mdl_produksi->data_kapal_dss();
		$template = 'templates/page/v_form';
		$modules = 'profil_inka_mina';
		$views = 'form_entry_produksi';
		$labels = 'form_produksi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input($filter = '')
	{
		// $this->load->model('mdl_inka_mina');

		$array_input = $this->input->post(NULL, TRUE);
		$array_produksi_ikan['id_jenis_ikan'] = $array_input['id_jenis_ikan'];
		$array_produksi_ikan['jml_ikan'] = $array_input['jml_ikan'];
		$array_produksi_ikan['harga_jual'] = $array_input['harga_jual'];
		unset($array_input['id_jenis_ikan'],$array_input['jml_ikan'],$array_input['harga_jual']);
		// $array_input['id_jenis_ikan'] = implode(',' ,  $array_input['id_jenis_ikan']);

		// $detail_kapal = $this->mdl_inka_mina->detil_kapal($array_input['id_kapal']);
		// echo $array_input['produktivitas_kapal'];
		unset($array_input['gt']);
		/*echo "<pre>";
		print_r($array_produksi_ikan);
		print_r($array_input);
		echo "</pre>";
		

		echo "<pre>";
		print_r($insert_produksi);
		echo "</pre>";*/
		if($filter === ''){
			$id_produksi = $this->mdl_produksi->input($array_input);
			if( $id_produksi !== null ){

				$insert_produksi = array();
				foreach ($array_produksi_ikan['id_jenis_ikan'] as $key => $value) {
					# code...
					array_push($insert_produksi, array(
													'id_produksi' => (int)$id_produksi,
													'id_jenis_ikan' => (int)$value,
													'jml_ikan' => (int)$array_produksi_ikan['jml_ikan'][$key],
													'harga_jual' => (int)$array_produksi_ikan['harga_jual'][$key]
													)
								);
				}
				
				foreach($insert_produksi as $key => $value)
				{
					$result = $this->mdl_produksi->input_produksi_ikan($value);

				}
				
				$url = base_url('profil_inka_mina/produksi');
				redirect($url);
			}else{
				$url = base_url('profil_inka_mina/produksi');
				redirect($url);
			}
		}else
		{
			if( $this->mdl_produksi->update($array_input) ){
				$url = base_url('profil_inka_mina/produksi');
				redirect($url);
			}else{
				$url = base_url('profil_inka_mina/produksi');
				redirect($url);
			}
		}
	}
	
	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$this->load->model('mdl_inka_mina');

		$array_input = $this->input->post(NULL, TRUE);
		$array_input['id_jenis_ikan'] = implode(',' ,  $array_input['id_jenis_ikan']);
		// $array_input['pendapatan_bersih'] = $array_input['nilai_pendapatan'] - $array_input['biaya_operasional'];
		
		// $array_input['pendapatan_abk'] = ($array_input['pendapatan_bersih'] - $array_input['dana_simpanan_kub']) / $array_input['jumlah_abk'];
		
		$detail_kapal = $this->mdl_inka_mina->detil_kapal($array_input['id_kapal']);
		// $array_input['produktivitas_kapal'] = ($array_input['jml_ikan']/1000)/$detail_kapal->gt;

		$array_to_edit = $array_input;
		if( $this->mdl_produksi->update($array_to_edit) ){
			$url = base_url('produksi/produksi');
			redirect($url);

		}else{
			$url = base_url('produksi/produksi');
			redirect($url);
		}
	}

	public function delete($id)
    {
        $this->mdl_produksi->delete($id);

        $url = base_url('profil_inka_mina/produksi');
        redirect($url);
    }


}
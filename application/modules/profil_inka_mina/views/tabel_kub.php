<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_kub' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$heading = Array(
					'0' => Array('data' => 'No', 'width' => '20'),
					'1' => 'Nama KUB',
					'2' => Array('data' => 'No SIUP', 'width' => '40'),
					// '3' => Array('data' => 'Tanggal SIUP', 'width' => '40'),
					'4' => Array('data' => 'Kabupaten/Kota', 'width' => '80'),
					'5' => Array('data' => 'No Telp', 'width' => '80'),
					'6' => Array('data' => 'Nama Ketua', 'width' => '80'),
					'7' => Array('data' => 'Jumlah Anggota', 'width' => '40'),
					'8' => Array('data' => 'Aksi', 'width' => '200')
					
				);
	$this->table->set_heading($heading);
	$counter = 1;
	if($list_kub){
		foreach ($list_kub as $item) {

			// $image_properties['src'] = 'uploads/'.$item->foto_ikan;
			// $image_properties['height'] = "150";
			// img($image_properties);
			$link_edit = '<a class="btn btn-warning" style="width=120" href="'.base_url('profil_inka_mina/kub/entry_kub/'.$item->id_kub).'">&nbsp;&nbsp;Edit&nbsp;&nbsp;</a>';
			$link_delete = '<a class="btn btn-danger" style="width=120" href="'.base_url('profil_inka_mina/kub/delete_kub/'.$item->id_kub).'">Hapus</a>';
			$link_view = '<a class="btn btn-primary" style="width=120" href="'.base_url('profil_inka_mina/kub/detail_kub/'.$item->id_kub).'">View</a>';
			$this->table->add_row(
								$counter.'.',
								$item->nama_kub,
								$item->no_siup,
								// $item->tgl_siup,
								$item->kabupaten,
								$item->no_telp,
								$item->nama_ketua,
								$item->anggota,
								$link_view." ".$link_edit." ".$link_delete
								);
			$counter++;
		}
	}

	$table_kub = $this->table->generate();
?>
<div class="row">
	<div class="col-lg-3" style="margin-bottom:50px">
		<a class="btn btn-default"  href="<?php echo base_url('profil_inka_mina/kub/export_kub') ?>">Export to Excel</a>
	</div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $table_kub;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_kub').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        // {"sClass": "text-left"},
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                       
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>
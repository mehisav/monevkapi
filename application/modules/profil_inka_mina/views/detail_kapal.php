<?php
    $this->load->helper('html');
    $temp_pelabuhan = ($detail_inka->id_pelabuhan_pangkalan==0)?$detail_inka->pelabuhan_pangkalan:$detail_inka->nama_pelabuhan;
?>
<style type="text/css">
    @media print {
        page_enter {page-break-after: always;}
    }
</style>
<script language="javascript">
    function printDiv() {
          var printContents = document.getElementById("print1").innerHTML + document.getElementById("print2").innerHTML;
          var originalContents = document.body.innerHTML;

          document.body.innerHTML = printContents;

          window.print();

          document.body.innerHTML = originalContents;
    }

    function myFunction() {
        document.getElementById("page_enter").style.pageBreakAfter = "always";
    }
</script>



<body>
<div class="container">
    <div class="page-header">
        <h3>NAMA KAPAL : <?php echo $detail_inka->nama_kapal; ?></h3>
    </div>
	<div class="col-md-6">
        
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1default" data-toggle="tab">Kapal</a></li>
                        <li><a href="#tab2default" data-toggle="tab">KUB</a></li>
                        <li><a href="#tab3default" data-toggle="tab">Produksi</a></li>
                    </ul>
            </div>
            <div class="panel-body" >
                <div class="tab-content" >
                    <div class="tab-pane fade in active" id="tab1default">
                        <FORM>
                        <INPUT TYPE="button" class="btn btn-default" value="Export to PDF" onClick="printDiv('panel-body')">
                    </FORM>
                    <div id="print1">
                        <table class="tablelist" width="80%">
                            <tr>
                                <td  width="30%">ID Kapal Bantuan</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->id_kapal; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">ID Kapal DSS</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->id_dss; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Nama Kapal</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->nama_kapal; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Kabupaten/Kota</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->nama_kabupaten_kota; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Propinsi</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->nama_propinsi; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">DPI</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->nama_dpi; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">WPP</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_wpp; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">ID Transmitter VMS</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->id_transmitter_vms; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Tanda Selar</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->tanda_selar; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Gross Akte</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->gross_akte; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">No. SIPI/</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->sipi; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Masa Berlaku SIPI</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->tanggal_sipi." - ".$detail_inka->tanggal_akhir_sipi; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Pelabuhan Pangkalan</td>
                                <td width="10px">:</td>
                                <td><?php echo  $temp_pelabuhan;?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Alat Tangkap</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->nama_alat_tangkap; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Dimensi Kapal</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->panjang_kapal." X ".$detail_inka->lebar_kapal." X ".$detail_inka->dalam_kapal; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Daya Kapal</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->daya; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">GT</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->gt; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Mesin</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->mesin; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">No Mesin</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->no_mesin; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Kontraktor Pembangunan</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->kontraktor_pembangunan; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Lokasi Pembangunan</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->lokasi_pembangunan; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Tahun Pembuatan</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->tahun_pembuatan; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Bahan Kapal</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->nama_bahan_kapal; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Permasalahan Kapal</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->masalah; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Keterangan</td>
                                <td width="10px">:</td>
                                <td><?php echo $detail_inka->keterangan; ?></td>
                            </tr>
                        </table>
                        </br>
                        <div id="page_enter" style="page-break-after: always;"></div>
                        </br>
                        <table class='tbl_gambar' width=100%>
                            <tr>
                                <td colspan=3 class='td-center'><h4>FOTO-FOTO KEGIATAN</h4><td>
                            </tr>
                            <tr>
                                <td class='td-center'>
                                    <?php 
                                        $file = (isset($detail_inka->foto_pembuatan))? $detail_inka->foto_pembuatan: '0.jpg';
                                        $image_properties = array(
                                                  'src' => 'uploads/'.$file,
                                                  'class' => 'post_images',
                                                  'width' => '250',
                                                  'height' => '150'
                                        );
                                        echo img($image_properties);

                                    ?>
                                </td>
                                <td class='td-center'>
                                    <?php 
                                        $file = (isset($detail_inka->foto_sea_trial))? $detail_inka->foto_sea_trial: '0.jpg';
                                        $image_properties['src'] = 'uploads/'.$file;
                                        echo img($image_properties);

                                    ?>
                                </td>
                                <td class='td-center'>
                                    <?php 
                                        $file = (isset($detail_inka->foto_oprasional))? $detail_inka->foto_oprasional: '0.jpg';
                                        $image_properties['src'] = 'uploads/'.$file;
                                        echo img($image_properties);

                                    ?>
                                </td>
                            <tr>
                            <tr>
                                <td class='td-center'>Proses Pembuatan</td>
                                <td class='td-center'>Proses Sea Trial</td>
                                <td class='td-center'>Operasional</td>
                            <tr>
                        </table>
                        </br>
                        </br>
                        <table class='tbl_gambar' width=100%>
                        <tr>
                            <td colspan="2" class='td-center' style="font-weight:bold;">Foto Surat Ijin</td>
                            <td colspan="2" class='td-center' style="font-weight:bold;">Bas ke KuB</td>
                            
                        <tr>
                        <tr>
                            <td class='td-center'>
                                <?php 
                                    $file = (isset($detail_inka->foto_siup1))? $detail_inka->foto_siup1: '0.jpg';
                                    $image_properties = array(
                                                  'src' => 'uploads/'.$file,
                                                  'class' => 'post_images',
                                                  'width' => '150',
                                                  'height' => '250'
                                        );
                                    echo img($image_properties);

                                ?>
                            </td>
                            <td class='td-center'>
                                <?php 
                                    $file = (isset($detail_inka->foto_siup2))? $detail_inka->foto_siup2: '0.jpg';
                                    $image_properties['src'] = 'uploads/'.$file;
                                    echo img($image_properties);

                                ?>
                            </td>
                            <td class='td-center'>
                                <?php 
                                    $file = (isset($detail_inka->foto_bast1))? $detail_inka->foto_bast1: '0.jpg';
                                    $image_properties['src'] = 'uploads/'.$file;
                                    echo img($image_properties);

                                ?>
                            </td>
                            <td class='td-center'>
                                <?php 
                                    $file = (isset($detail_inka->foto_bast2))? $detail_inka->foto_bast2: '0.jpg';
                                    $image_properties['src'] = 'uploads/'.$file;
                                    echo img($image_properties);

                                ?>
                            </td>
                        <tr>
                        <tr>
                            <td class='td-center'>SIPI</td>
                            <td class='td-center'>SIUP</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    </div>
                    </div>
                    <div class="tab-pane fade" id="tab2default">
                    <FORM>
                        <INPUT TYPE="button" class="btn btn-default" value="Export to PDF" onClick="printDiv('panel-body')">
                    </FORM>
                    <div id="print2">
                        <div id="page_enter" style="page-break-after: always;"></div>
                       <table class="tablelist" width="80%">
                            <tr>
                                <td  width="30%">ID KUB</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->id_kub))? $detail_kub->id_kub : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">ID Perusahaan  DSS</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->id_perusahaan))? $detail_kub->id_perusahaan : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Nama KUB</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->nama_kub))? $detail_kub->nama_kub : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">No SIUP</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->no_siup))? $detail_kub->no_siup : ''; ?></td>
                            </tr>
                          <!--   <tr>
                                <td  width="30%">Tanggal SIUP</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->tgl_siup))? $detail_kub->tgl_siup : ''; ?></td>
                            </tr> -->
                            <tr>
                                <td  width="30%">Tahun Pembentukan</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->tahun_pembentukan))? $detail_kub->tahun_pembentukan : ''; ?></td>
                            </tr>
                           <!--  <tr>
                                <td  width="30%">Tahun Pengukuhan</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->tahun_pengukuhan))? $detail_kub->tahun_pengukuhan : ''; ?></td>
                            </tr> -->
                            <tr>
                                <td  width="30%">Sumber Anggaran</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->sumber_anggaran))? $detail_kub->sumber_anggaran : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Oleh</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->oleh))? $detail_kub->oleh : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">No Telepon </td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->nomor))? $detail_kub->nomor : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%"><b>ALAMAT</b></td>
                                <td width="10px"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td  width="30%">Kabupaten/Kota</td>
                                <td width="10px">:</td>
                                <td><?php echo  (isset($detail_kub->id_kabupaten_kota))? $detail_kub->id_kabupaten_kota : '';?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Alamat Lengkap</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->alamat))? $detail_kub->alamat : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%"><b>PENGURUS</b></td>
                                <td width="10px"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td  width="30%">Nama Ketua</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->nama_ketua))? $detail_kub->nama_ketua : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Telepon Ketua</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->telp_ketua))? $detail_kub->telp_ketua : ''; ?></td>
                            </tr>
                           <!--  <tr>
                                <td  width="30%">Nama Wakil</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->nama_wakil))? $detail_kub->nama_wakil : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Telepon Wakil</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->telp_wakil))? $detail_kub->telp_wakil : ''; ?></td>
                            </tr> -->
                            <tr>
                                <td  width="30%">Nama Sekretaris</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->nama_sekretaris))? $detail_kub->nama_sekretaris : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Telepon Sekretaris</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->telp_sekretaris))? $detail_kub->telp_sekretaris : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Nama Bendahara</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->nama_bendahara))? $detail_kub->nama_bendahara : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%">Telepon Bendahara</td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->telp_bendahara))? $detail_kub->telp_bendahara : ''; ?></td>
                            </tr>
                            <tr>
                                <td  width="30%"><b>Jumlah Anggota</b></td>
                                <td width="10px">:</td>
                                <td><?php echo (isset($detail_kub->anggota))? $detail_kub->anggota : ''; ?></td>
                            </tr>
                        </table>
                    </div>
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                        <?php
                            //OLAH DATA TAMPIL
                            $template = array( "table_open" => "<table id='table_daftar_produksi' class='table table-hover table-bordered'>");
                            $this->table->set_template($template);
                            $this->table->set_heading($constants['th_table']);
                            $counter = 1;
                            if($detail_produksi){
                                foreach ($detail_produksi as $item) {
                                    $link_edit = '<a class="btn btn-warning" style="font-size:10px" href="'.base_url('profil_inka_mina/produksi/form_entry_produksi/'.$item->id_produksi).'">&nbsp;&nbsp;Edit&nbsp;&nbsp;</a>';
                                    $link_delete = '<a class="btn btn-danger" style="font-size:10px" href="'.base_url('profil_inka_mina/produksi/delete/'.$item->id_produksi).'">Hapus</a>';
                                    $this->table->add_row(
                                                        $counter.'.',
                                                        $item->nama_kapal,
                                                        $item->nama_wpp,
                                                        $item->nama_dpi,
                                                        tgl($item->tgl_keluar),
                                                        tgl($item->tgl_masuk),
                                                        $item->nama_pelabuhan_keluar,
                                                        $item->nama_pelabuhan_masuk,
                                                        $item->nama_nahkoda,
                                                        $item->jml_abk,
                                                        $item->id_jenis_ikan,
                                                        $item->jml_ikan,
                                                        "RP ".number_format($item->nilai_pendapatan,2),
                                                        $item->kebutuhan_bbm,
                                                        "RP ".number_format($item->biaya_operasional,2),
                                                        "RP ".number_format((($item->nilai_pendapatan - $item->biaya_operasional) - ($item->kebutuhan_bbm*7400)),2),
                                                        ''
                                                        );
                                    $counter++;
                                }
                            }

                            $table_detail_produksi = $this->table->generate();
                        ?>
                        <div style="width:100%;overflow:auto;">
                            <?php
                                echo $table_detail_produksi;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<script>
    $(document).ready( function () {
        $('#table_daftar_produksi').dataTable({

            "aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-left"},
                        {"sClass": "text-center"},
                        {"sClass": "text-left"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-right"},
                        {"sClass": "text-center"}
                      ],
        });
    } );
</script>
</body>
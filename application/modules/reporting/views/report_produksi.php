<!-- filter propinsi -->
<?php
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	// print_r(Modules::run('laporan/mst_filter/list_propinsi_array'));
	$attr_nama_kapal = array( 'name' => $form['id_kapal']['name'],
                          'label' => $form['nama_kapal']['label'],
                          'value' => (isset($id_kapal)? $id_kapal : 0),
                          'opsi' => Modules::run('profil_inka_mina/kapal/list_inka_mina_array')
                          // 'opsi' => Modules::run('reporting/report_kapal/list_inka_mina_array')
          );          
         echo $this->mkform->input_select2($attr_nama_kapal);
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Filter</button>
              <button type="submit" class="btn btn-primary" name="export">Export</button>
            </div>
          </div>
  </div>
</div>


<!-- tabel laporan  -->
<?php
	$template = array( "table_open" => "<table id='table_daftar_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading(	'NO.',
								'WPP',
								'Daerah Penangkalan',
								'GT Kapal',
								'Tgl Berangkat',
								'Jumlah Hari Operasi',
								'Volume Produksi/Trip (Kg)',
								'Nilai Produksi/Trip (Rp)',
								'Jenis Ikan Hasil Tangkapan Dominan',
								'Kebutuhan BBM/TRIP (Ton)',
								'Biaya Operasional/Trip (RP)',
								'Jumlah ABK (orang)',
								'Pendapatan Bersih/Trip (Rp)'
							);
	if($list_produksi){
		$index = 1;
		foreach ($list_produksi as $item) {
			$this->table->add_row(
				$index,
				$item->id_wpp,
				$item->nama_wpp,
				$item->gt,
				tgl($item->tgl_berangkat),
				$item->jml_hari_operasi,
				$item->jml_ikan,
				$item->nilai_pendapatan,
				$item->nama_jenis_ikan,
				$item->kebutuhan_bbm,
				$item->biaya_operasional,
				$item->jumlah_abk,
				$item->pendapatan_bersih
				);
			$index++;	
		}
	}
	$table_list_kapal = $this->table->generate();
?>
<!-- TAMPIL DATA -->
	<div style="width:100%;border:1px solid #ccc;overflow:auto;">
	<!-- <div style="height:800px;width:100%;"> -->
		<?php
			echo $table_list_kapal;
		?>
	</div>

<script>
	$(document).ready( function () {
		$('#table_daftar_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"bFilter": true,
	        "bAutoWidth": true
		} );
	} );
</script>
<?php
	$this->load->helper('html');
	echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
	// print_r(Modules::run('laporan/mst_filter/list_propinsi_array'));
	 $attr_propinsi = array( 'name' => $form['propinsi']['name'],
                          'label' => $form['propinsi']['label'],
                          'value' => (isset($propinsi)? $propinsi : 0),
                          'opsi' => Modules::run('refdss/propinsi/list_propinsi')
          );          
         echo $this->mkform->input_select2($attr_propinsi);
	$attr_dasar_tipe = array( 'name' => $form['dasar_tipe']['name'],
                                         'label' => $form['dasar_tipe']['label'],
                                         'opsi' => array(
                                                          '1' => 'Produksi', 
                                                          '2' => 'Pendapatan',
                                                        ),
                                         'value' => (isset($dasar_tipe)? $dasar_tipe : 0)
                    );
          echo $this->mkform->input_select($attr_dasar_tipe);
    $attr_dasar_kapal = array( 'name' => $form['waktu']['name'],
                                         'label' => $form['waktu']['label'],
                                         'opsi' => array(
                                                          // '1' => 'Pertrip', 
                                                          '2' => 'Perbulan',
                                                          '3' => 'Triwulan',
                                                          '4' => 'Tahun'
                                                        ),
                                         'value' => (isset($waktu)? $waktu : 0)
                    );
          echo $this->mkform->input_select($attr_dasar_kapal);
    $bulan = array('Januari', 'Februari', 'Maret', 'April','Mei','Juni','Juli', 'Agustus','September','Oktober','November','Desember');
    // $tanggal = (isset($list_produksi->tgl_berangkat)? $list_produksi->tgl_berangkat : '');
    //       $attr_tanggal_berangkat = array(  'mindate' => array('time' => '10 year'),
    //                                       'default_date' => $tanggal,
    //                                       'placeholder' => '', 
    //                                       'name' => $form['tgl_berangkat']['name'],
    //                                     'label' => $form['tgl_berangkat']['label']
    //                 );
    //       echo $this->mkform->input_date($attr_tanggal_berangkat); 
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Filter</button>
              <!-- <button type="submit" class="btn btn-primary" name="export">Export</button> -->
            </div>
          </div>
  </div>
</div>
<?php 
if($chart==true)
{
?>
	<script type="text/javascript">
	  window.onload = function () {
	    var chart = new CanvasJS.Chart("chartContainer",
	    {
	      title:{
	        text: "<?php echo $chart_title?>"
	      },
	      animationEnabled: true,
	      axisY: {
	        title: ""
	      },
	      legend: {
	        verticalAlign: "bottom",
	        horizontalAlign: "center"
	      },
	      theme: "theme2",
	      data: <?=$data?>
	    });

	    chart.render();
	  }
	  </script>
	<div id="chartContainer" style="height: 300px; width: 100%;">
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Nama Kapal</th>
				<th>Waktu</th>
				<th>Jumlah Produksi</th>
			</tr>
		</thead>
		<tbody>
			
			<?php
				$i = 1;
				foreach ($chart_isi as $data_table) {
					echo "<tr>";
					echo "<th>".$data_table->nama_kapal."</th>";
					echo "<td align='center' >";
					if(isset($data_table->bulan)){
						echo $bulan[$data_table->bulan-1].' - ';
					}

					if(isset($data_table->quarter)){
						echo "Semester ".$data_table->quarter.' - ';
					}
					echo $data_table->tahun."</td>";
					echo "<td align='center' >".$data_table->nilai."</td>";
					echo "</tr>";
					$i++;
				}
			?>
			
		</tbody>
	</table>
<?php
}
?>
<div class="row">
  <div class="col-lg-12">
          <?php

          //echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
          //var_dump($detail_produksi);
          //$input_hidden  = array('gt' => $detail_produksi['gt']);

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
          if(isset($detail_produksi->id_produksi)){
            $input_hidden  = array('id_produksi' => $detail_produksi->id_produksi );

            echo form_hidden($input_hidden);
          }
          ?>


         <?php

          // $attr_jenis_ikan = array( 'name' => $form['jenis_ikan']['name'],
          //                         'label' => $form['jenis_ikan']['label'],
          //                         'opsi' => Modules::run('produksi/mst_jenis_ikan/list_jenis_ikan_array')
          //           );
          // echo $this->mkform->input_select2($attr_jenis_ikan); 
         $attr_nama_kapal = array( 'name' => $form['id_kapal']['name'],
                          'label' => $form['nama_kapal']['label'],
                          'value' => (isset($detail_produksi->id_kapal)? $detail_produksi->id_kapal : 0),
                          'opsi' => Modules::run('profil_inka_mina/kapal/list_inka_mina_array')
          );          
         echo $this->mkform->input_select2($attr_nama_kapal);

          $attr_daerah_penangkapan = array('name' => $form['id_dpi']['name'],
                                 'label' => $form['nama_dpi']['label'],
                                 'value' => (isset($detail_produksi->id_dpi)? $detail_produksi->id_dpi : 0),
                                 'opsi' => Modules::run('refdss/dpi/list_dpi_array'),
                                 'placeholder' => ''
                    );
          echo $this->mkform->input_select2($attr_daerah_penangkapan);           

          $tanggal = (isset($detail_produksi->tgl_keluar)? $detail_produksi->tgl_keluar : '');
          $attr_tgl_keluar = array(  'mindate' => array('time' => '1 year'),
                                          'default_date' => '',
                                          'placeholder' => '', 
                                          'name' => $form['tgl_keluar']['name'],
                                        'label' => $form['tgl_keluar']['label']
                    );
          echo $this->mkform->input_date($attr_tgl_keluar); 
          
          $tanggal = (isset($detail_produksi->tgl_masuk)? $detail_produksi->tgl_masuk : '');
          $attr_tgl_masuk = array(  'mindate' => array('time' => '1 year'),
                                          'default_date' => $tanggal,
                                          'placeholder' => '', 
                                          'name' => $form['tgl_masuk']['name'],
                                        'label' => $form['tgl_masuk']['label']
                    );
          echo $this->mkform->input_date($attr_tgl_masuk);  

           $attr_id_pelabuhan_keluar = array('name' => $form['id_pelabuhan_keluar']['name'],
                                 'label' => $form['id_pelabuhan_keluar']['label'],
                                 'value' => (isset($detail_produksi->id_pelabuhan_keluar)? $detail_produksi->id_pelabuhan_keluar : 0),
                                 'opsi' => Modules::run('refdss/pelabuhan/list_pelabuhan_array'),
                                 'placeholder' => ''
                    );
          echo $this->mkform->input_select2($attr_id_pelabuhan_keluar);

           $attr_id_pelabuhan_masuk = array('name' => $form['id_pelabuhan_masuk']['name'],
                                 'label' => $form['id_pelabuhan_masuk']['label'],
                                 'value' => (isset($detail_produksi->id_pelabuhan_masuk)? $detail_produksi->id_pelabuhan_masuk : 0),
                                 'opsi' => Modules::run('refdss/pelabuhan/list_pelabuhan_array'),
                                 'placeholder' => ''
                    );
          echo $this->mkform->input_select2($attr_id_pelabuhan_masuk);

          $attr_nama_nahkoda = array( 'name' => $form['nama_nahkoda']['name'],
                                        'label' => $form['nama_nahkoda']['label'],
                                        'value' => (isset($detail_produksi->nama_nahkoda)? $detail_produksi-> nama_nahkoda : '')
                    );          
          echo $this->mkform->input_text($attr_nama_nahkoda);  

          $attr_jumlah_abk = array( 'name' => $form['jumlah_abk']['name'],
                                        'label' => $form['jumlah_abk']['label'],
                                        'value' => (isset($detail_produksi->jml_abk)? $detail_produksi->jml_abk : '')
                    );
          echo $this->mkform->input_text($attr_jumlah_abk); 



          $attr_jenis_ikan = array( 'name' => $form['id_jenis_ikan']['name'].'[]',
                                  'label' => $form['jenis_ikan']['label'],
                                  'opsi' => Modules::run('refdss/jenis_ikan/list_jenis_ikan_array'),
                                  'is_multipel' => FALSE,
                                  'value_m' => (isset($detail_produksi->id_jenis_ikan)? $detail_produksi->id_jenis_ikan : '')
                    );
          echo $this->mkform->input_select2($attr_jenis_ikan);

          $attr_jml_ikan = array( 'name' => $form['jml_ikan']['name'].'[]',
                                        'label' => $form['jml_ikan']['label'],
                                        'value' => (isset($detail_produksi->jml_ikan)? $detail_produksi->jml_ikan : '')
                    );
          echo $this->mkform->input_text($attr_jml_ikan); 

          $attr_jml_ikan = array( 'name' => $form['harga_jual']['name'].'[]',
                                        'label' => $form['harga_jual']['label'],
                                        'value' => (isset($detail_produksi->harga_jual)? $detail_produksi->harga_jual : '')
                    );
          echo $this->mkform->input_text($attr_jml_ikan); 

          echo "<div id='row' ></div>";

          echo '<div class="form-group mkform-select">
                  <label for="" class="col-sm-3 control-label"></label>
                  <div class="col-sm-8">
                    <a href="#row" id="tambah_row" class="btn btn-default" >[+]Tambah</a>
                  </div>
                </div>';


          ?>

          <script type="text/javascript">
            var name = "<?=$form['id_jenis_ikan']['name']?>";
            var name2 = "<?=$form['jml_ikan']['name']?>";
            var name3 = "<?=$form['harga_jual']['name']?>";
            var list_jenis_ikan_select2 = <?=$data_jenis_ikan?>;

            var init_select2_jenis_ikan = function(){
                        $('.jenis_ikan_selector').select2({
                                    data: list_jenis_ikan_select2,
                                    placeholder: 'Nama Jenis Ikan'
                                    // templateSelection: formatJenisIkanSelection
                                });
            }

            var tombol = function(){

                  $('#tambah_row').on("click",function(){
                      var div = '<div class="form-group mkform-select">'+
                                    '<label for="'+name+'[]" class="col-sm-3 control-label">Jenis Ikan Hasil Tangkapan Dominan</label>'+
                                    '<div class="col-sm-8">'+
                                      '<select name="'+name+'[]" class="select2ikan form-control" style="width:80%!important" >'+
                                        '<option value="0"></option>';
                                        $.each(list_jenis_ikan_select2, function(index, value){
                                          
                                          div +='<option value="'+value.id+'" >'+value.text+'</option>';

                                        });
                          div +=      '</select><a href="#row" id="hapus_row" class="btn btn-default hapus_row" >[-]Hapus</a>'+
                                    '</div>'+
                                  '</div>';
                          div += '<div class="form-group mkform-text">'+
                                    '<label for="'+name2+'[]" class="col-sm-3 control-label">Volume (Kg)</label>'+
                                    '<div class="col-sm-8">'+
                                        '<input id="" '+
                                          'name="'+name2+'[]"'+
                                          'type="text"'+
                                          'class="form-control"'+
                                          'placeholder=""'+
                                          'value="" >'+
                                    '</div>'+
                                  '</div>';
                          div += '<div class="form-group mkform-text">'+
                                    '<label for="'+name3+'[]" class="col-sm-3 control-label">Harga Jual</label>'+
                                    '<div class="col-sm-8">'+
                                        '<input id="" '+
                                          'name="'+name3+'[]"'+
                                          'type="text"'+
                                          'class="form-control"'+
                                          'placeholder=""'+
                                          'value="" >'+
                                    '</div>'+
                                  '</div>';
                      $('#row').after(div); 

                      $('select.select2ikan').select2();

                      $('.hapus_row').click(function(){
                          $(this).parent().parent().next().remove();
                          $(this).parent().parent().next().remove();
                          $(this).parent().parent().remove();
                      });

                  });
            }

            var tombol_hapus = function(){
                      
            }

            s_func.push(init_select2_jenis_ikan);
            s_func.push(tombol);
            s_func.push(tombol_hapus);

          </script>

          <?php

          $attr_nilai_pendapatan = array( 'name' => $form['nilai_pendapatan']['name'],
                                        'label' => $form['nilai_pendapatan']['label'],
                                        'value' => (isset($detail_produksi->nilai_pendapatan)? $detail_produksi->nilai_pendapatan : '')
                    );
          echo $this->mkform->input_text($attr_nilai_pendapatan); 
 
          $attr_kebutuhan_bbm = array( 'name' => $form['kebutuhan_bbm']['name'],
                                        'label' => $form['kebutuhan_bbm']['label'],
                                        'value' => (isset($detail_produksi->kebutuhan_bbm)? $detail_produksi->kebutuhan_bbm : '')
                    );
          echo $this->mkform->input_text($attr_kebutuhan_bbm); 

          $attr_biaya_operasional = array( 'name' => $form['biaya_operasional']['name'],
                                        'label' => $form['biaya_operasional']['label'],
                                        'value' => (isset($detail_produksi->biaya_operasional)? $detail_produksi->biaya_operasional : '')
                    );
          echo $this->mkform->input_text($attr_biaya_operasional); 

         ?>
  </div>
</div>

         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>